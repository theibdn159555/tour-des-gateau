using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using WebUI.Controllers.Carts;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.Models;
using Xunit;

namespace XUnitTest_2
{
    public class UnitTest1
    {
        [Fact]
        public async void GetExchangeRateTest()
        {
            var result = await OrderController.GetExchangeRate();
            Assert.Equal((decimal)23074.995517, result);
        }
        [Fact]
        public async void AddProductItemToCartTest()
        {
            var item = new CartDetail()
            {
                AccountId = 9,
                ProductId = 2,
                Quantity = 2,
                UnitPrice = 75,
                DiscountValue = 0.0
            };
            WebUI.Data.db_a7c7da_phunghung123Context context = new WebUI.Data.db_a7c7da_phunghung123Context();
            var repo = new SqlDetailCartRepo(context);
            var result = await repo.CreateNew(item);
            Assert.Equal(19, result);
        }
        [Fact]
        public async void PayPalCheckoutTest()
        {
            var writeDto = new OrderWriteDTO()
            {
                AccountId = 9,
                PaymentMethod = 3,
                ReceiverFirstName = "Phung",
                ReceiverLastName = "Hung",
                Phone = "0963357143",
                ShipAddress = "5/1 14th St.",
                ShipWard = "25th W.",
                ShipDistrict = "Binh Tan Dist."
            };
            var mockRepo = new Mock<IWeakRepo<WebUI.Models.Order>>();
            var mockMapper = new Mock<IMapper>();
            var mockConfig = new Mock<IConfiguration>();
            var controller = new OrderController(mockRepo.Object, mockMapper.Object, mockConfig.Object);
            var actionresult = await controller.PaypalCheckout(writeDto);
            Assert.Equal(302, 302);
        }
    }
}
