﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Discount_Events")]
    [Index(nameof(EventName), Name = "idx_event")]
    public partial class DiscountEvent
    {
        public DiscountEvent()
        {
            Products = new HashSet<Product>();
        }

        [Key]
        [Column("event_id")]
        public int EventId { get; set; }
        [Column("event_name")]
        [StringLength(255)]
        public string EventName { get; set; }
        [Column("date_start", TypeName = "date")]
        public DateTime? DateStart { get; set; }
        [Column("date_end", TypeName = "date")]
        public DateTime? DateEnd { get; set; }
        [Column("discount_value")]
        public double? DiscountValue { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty(nameof(Product.Event))]
        public virtual ICollection<Product> Products { get; set; }
    }
}
