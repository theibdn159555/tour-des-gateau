﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(Main), Name = "idx_ingredient")]
    public partial class Ingredient
    {
        [Key]
        [Column("product_id")]
        public int ProductId { get; set; }
        [Key]
        [Column("material_id")]
        public int MaterialId { get; set; }
        [Column("quantity")]
        public double? Quantity { get; set; }
        [Column("main")]
        public bool Main { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(MaterialId))]
        [InverseProperty("Ingredients")]
        public virtual Material Material { get; set; }
        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Ingredients")]
        public virtual Product Product { get; set; }
    }
}
