﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Rating")]
    [Index(nameof(StarRating), Name = "idx_sta")]
    public partial class Rating
    {
        [Key]
        [Column("product_id")]
        public int ProductId { get; set; }
        [Key]
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("star_rating")]
        public int StarRating { get; set; }
        [Column("comment")]
        [StringLength(1000)]
        public string Comment { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Ratings")]
        public virtual Product Product { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(Account.Ratings))]
        public virtual Account User { get; set; }
    }
}
