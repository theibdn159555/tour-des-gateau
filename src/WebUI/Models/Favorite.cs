﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    public partial class Favorite
    {
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Key]
        [Column("product_id")]
        public int ProductId { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }

        [ForeignKey(nameof(AccountId))]
        [InverseProperty("Favorites")]
        public virtual Account Account { get; set; }
        [ForeignKey(nameof(ProductId))]
        [InverseProperty("Favorites")]
        public virtual Product Product { get; set; }
    }
}
