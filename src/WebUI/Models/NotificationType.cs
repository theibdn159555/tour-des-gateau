﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Notification_Types")]
    public partial class NotificationType
    {
        public NotificationType()
        {
            NotificationTemplates = new HashSet<NotificationTemplate>();
        }

        [Key]
        [Column("notice_type_id")]
        public int NoticeTypeId { get; set; }
        [Column("type_name")]
        [StringLength(255)]
        public string TypeName { get; set; }

        [InverseProperty(nameof(NotificationTemplate.Type))]
        public virtual ICollection<NotificationTemplate> NotificationTemplates { get; set; }
    }
}
