﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(PermissionName), Name = "UNIQUE_PER_NAME", IsUnique = true)]
    public partial class Permission
    {
        public Permission()
        {
            Accounts = new HashSet<Account>();
        }

        [Key]
        [Column("permission_id")]
        public int PermissionId { get; set; }
        [Required]
        [Column("permission_name")]
        [StringLength(50)]
        public string PermissionName { get; set; }
        [Column("description")]
        [StringLength(500)]
        public string Description { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [InverseProperty(nameof(Account.Permission))]
        public virtual ICollection<Account> Accounts { get; set; }
    }
}
