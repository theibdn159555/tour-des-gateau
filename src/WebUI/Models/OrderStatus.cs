﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Order_Status")]
    [Index(nameof(StatusName), Name = "UNIQUE_STT_NAME", IsUnique = true)]
    public partial class OrderStatus
    {
        public OrderStatus()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        [Column("status_id")]
        public int StatusId { get; set; }
        [Required]
        [Column("status_name")]
        [StringLength(100)]
        public string StatusName { get; set; }
        [Column("description")]
        [StringLength(255)]
        public string Description { get; set; }

        [InverseProperty(nameof(Order.Status))]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
