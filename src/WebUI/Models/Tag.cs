﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(NameTag), Name = "idx_name_tag", IsUnique = true)]
    public partial class Tag
    {
        public Tag()
        {
            Products = new HashSet<Product>();
        }

        [Key]
        [Column("tag_id")]
        public int TagId { get; set; }
        [Required]
        [Column("name_tag")]
        [StringLength(255)]
        public string NameTag { get; set; }

        [InverseProperty(nameof(Product.Tag))]
        public virtual ICollection<Product> Products { get; set; }
    }
}
