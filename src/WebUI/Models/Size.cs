﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(SizeName), Name = "UNIQUE_SIZ_NAME", IsUnique = true)]
    public partial class Size
    {
        public Size()
        {
            Products = new HashSet<Product>();
        }

        [Key]
        [Column("size_id")]
        public int SizeId { get; set; }
        [Required]
        [Column("size_name")]
        [StringLength(40)]
        public string SizeName { get; set; }
        [Column("description")]
        [StringLength(255)]
        public string Description { get; set; }

        [InverseProperty(nameof(Product.Size))]
        public virtual ICollection<Product> Products { get; set; }
    }
}
