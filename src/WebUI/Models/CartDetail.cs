﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Cart_Details")]
    public partial class CartDetail
    {
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Key]
        [Column("cart_id")]
        public int CartId { get; set; }
        [Key]
        [Column("product_id")]
        public int ProductId { get; set; }
        [Column("quantity")]
        public int Quantity { get; set; }
        [Column("discount_value")]
        public double DiscountValue { get; set; }
        [Column("unit_price", TypeName = "decimal(18, 0)")]
        public decimal UnitPrice { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey("CartId,AccountId")]
        [InverseProperty("CartDetails")]
        public virtual Cart Cart { get; set; }
        [ForeignKey(nameof(ProductId))]
        [InverseProperty("CartDetails")]
        public virtual Product Product { get; set; }
    }
}
