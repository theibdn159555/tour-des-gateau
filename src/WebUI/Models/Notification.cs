﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    public partial class Notification
    {
        [Key]
        [Column("notification_template_id")]
        public int NotificationTemplateId { get; set; }
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Key]
        [Column("notification_id")]
        public int NotificationId { get; set; }
        [Column("content")]
        [StringLength(255)]
        public string Content { get; set; }
        [Column("is_read")]
        public bool? IsRead { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(AccountId))]
        [InverseProperty("Notifications")]
        public virtual Account Account { get; set; }
        [ForeignKey(nameof(NotificationTemplateId))]
        [InverseProperty("Notifications")]
        public virtual NotificationTemplate NotificationTemplate { get; set; }
    }
}
