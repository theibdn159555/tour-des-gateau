﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(ProductName), Name = "UNIQUE_PROD_NAME", IsUnique = true)]
    [Index(nameof(ProductName), Name = "idx_prd")]
    public partial class Product
    {
        public Product()
        {
            CartDetails = new HashSet<CartDetail>();
            Favorites = new HashSet<Favorite>();
            Ingredients = new HashSet<Ingredient>();
            Ratings = new HashSet<Rating>();
        }

        [Key]
        [Column("product_id")]
        public int ProductId { get; set; }
        [Column("category_id")]
        public int CategoryId { get; set; }
        [Column("size_id")]
        public int SizeId { get; set; }
        [Column("tag_id")]
        public int TagId { get; set; }
        [Column("event_id")]
        public int? EventId { get; set; }
        [Required]
        [Column("product_name")]
        [StringLength(255)]
        public string ProductName { get; set; }
        [Column("unit_price", TypeName = "decimal(18, 0)")]
        public decimal UnitPrice { get; set; }
        [Column("image")]
        [StringLength(500)]
        public string Image { get; set; }
        [Column("description")]
        [StringLength(500)]
        public string Description { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        [Column("product_illustration", TypeName = "image")]
        public byte[] ProductIllustration { get; set; }

        [ForeignKey(nameof(CategoryId))]
        [InverseProperty("Products")]
        public virtual Category Category { get; set; }
        [ForeignKey(nameof(EventId))]
        [InverseProperty(nameof(DiscountEvent.Products))]
        public virtual DiscountEvent Event { get; set; }
        [ForeignKey(nameof(SizeId))]
        [InverseProperty("Products")]
        public virtual Size Size { get; set; }
        [ForeignKey(nameof(TagId))]
        [InverseProperty("Products")]
        public virtual Tag Tag { get; set; }
        [InverseProperty(nameof(CartDetail.Product))]
        public virtual ICollection<CartDetail> CartDetails { get; set; }
        [InverseProperty(nameof(Favorite.Product))]
        public virtual ICollection<Favorite> Favorites { get; set; }
        [InverseProperty(nameof(Ingredient.Product))]
        public virtual ICollection<Ingredient> Ingredients { get; set; }
        [InverseProperty(nameof(Rating.Product))]
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
