﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(Username), nameof(Email), Name = "idx_name_email", IsUnique = true)]
    [Index(nameof(Username), Name = "unique_account", IsUnique = true)]
    [Index(nameof(Email), Name = "unique_email", IsUnique = true)]
    public partial class Account
    {
        public Account()
        {
            Carts = new HashSet<Cart>();
            Favorites = new HashSet<Favorite>();
            Notifications = new HashSet<Notification>();
            Ratings = new HashSet<Rating>();
            Violations = new HashSet<Violation>();
        }

        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Column("permission_id")]
        public int PermissionId { get; set; }
        [Required]
        [Column("username")]
        [StringLength(50)]
        public string Username { get; set; }
        [Required]
        [Column("password_hash")]
        [StringLength(100)]
        public string PasswordHash { get; set; }
        [Column("first_name")]
        [StringLength(30)]
        public string FirstName { get; set; }
        [Column("last_name")]
        [StringLength(30)]
        public string LastName { get; set; }
        [Required]
        [Column("email")]
        [StringLength(100)]
        public string Email { get; set; }
        [Column("avatar")]
        [StringLength(500)]
        public string Avatar { get; set; }
        [Column("dob", TypeName = "date")]
        public DateTime? Dob { get; set; }
        [Column("phone")]
        [StringLength(20)]
        public string Phone { get; set; }
        [Column("gender")]
        public int? Gender { get; set; }
        [Column("membership")]
        public double? Membership { get; set; }
        [Column("facebook_id")]
        [StringLength(255)]
        public string FacebookId { get; set; }
        [Column("google _id")]
        [StringLength(255)]
        public string GoogleId { get; set; }
        [Column("ip_address")]
        [StringLength(255)]
        public string IpAddress { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        [Column("unlocked_member_at", TypeName = "datetime")]
        public DateTime? UnlockedMemberAt { get; set; }
        [Column("account_illustration", TypeName = "image")]
        public byte[] AccountIllustration { get; set; }

        [ForeignKey(nameof(PermissionId))]
        [InverseProperty("Accounts")]
        public virtual Permission Permission { get; set; }
        [InverseProperty(nameof(Cart.Account))]
        public virtual ICollection<Cart> Carts { get; set; }
        [InverseProperty(nameof(Favorite.Account))]
        public virtual ICollection<Favorite> Favorites { get; set; }
        [InverseProperty(nameof(Notification.Account))]
        public virtual ICollection<Notification> Notifications { get; set; }
        [InverseProperty(nameof(Rating.User))]
        public virtual ICollection<Rating> Ratings { get; set; }
        [InverseProperty(nameof(Violation.Account))]
        public virtual ICollection<Violation> Violations { get; set; }
    }
}
