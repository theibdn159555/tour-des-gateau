﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(AccountId), Name = "idx_acc")]
    public partial class Cart
    {
        public Cart()
        {
            CartDetails = new HashSet<CartDetail>();
        }

        [Key]
        [Column("cart_id")]
        public int CartId { get; set; }
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Column("is_open")]
        public bool? IsOpen { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(AccountId))]
        [InverseProperty("Carts")]
        public virtual Account Account { get; set; }
        [InverseProperty("Cart")]
        public virtual Order Order { get; set; }
        [InverseProperty(nameof(CartDetail.Cart))]
        public virtual ICollection<CartDetail> CartDetails { get; set; }
    }
}
