﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Index(nameof(CartId), Name = "idx_crt")]
    public partial class Order
    {
        [Key]
        [Column("cart_id")]
        public int CartId { get; set; }
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Column("status_id")]
        public int StatusId { get; set; }
        [Column("payment_method")]
        public int PaymentMethod { get; set; }
        [Column("bank_name")]
        [StringLength(255)]
        public string BankName { get; set; }
        [Column("ship_fee")]
        public double? ShipFee { get; set; }
        [Column("note")]
        [StringLength(255)]
        public string Note { get; set; }
        [Required]
        [Column("ship_address")]
        [StringLength(255)]
        public string ShipAddress { get; set; }
        [Required]
        [Column("ship_ward")]
        [StringLength(100)]
        public string ShipWard { get; set; }
        [Required]
        [Column("ship_district")]
        [StringLength(100)]
        public string ShipDistrict { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        [Column("phone")]
        [StringLength(20)]
        public string Phone { get; set; }
        [Column("receiver_first_name")]
        [StringLength(60)]
        public string ReceiverFirstName { get; set; }
        [Column("receiver_last_name")]
        [StringLength(60)]
        public string ReceiverLastName { get; set; }

        [ForeignKey("CartId,AccountId")]
        [InverseProperty("Order")]
        public virtual Cart Cart { get; set; }
        [ForeignKey(nameof(StatusId))]
        [InverseProperty(nameof(OrderStatus.Orders))]
        public virtual OrderStatus Status { get; set; }
    }
}
