﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    [Table("Notification_Templates")]
    public partial class NotificationTemplate
    {
        public NotificationTemplate()
        {
            Notifications = new HashSet<Notification>();
        }

        [Key]
        [Column("template_id")]
        public int TemplateId { get; set; }
        [Column("content_template")]
        [StringLength(255)]
        public string ContentTemplate { get; set; }
        [Column("type_id")]
        public int? TypeId { get; set; }

        [ForeignKey(nameof(TypeId))]
        [InverseProperty(nameof(NotificationType.NotificationTemplates))]
        public virtual NotificationType Type { get; set; }
        [InverseProperty(nameof(Notification.NotificationTemplate))]
        public virtual ICollection<Notification> Notifications { get; set; }
    }
}
