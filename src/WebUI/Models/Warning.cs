﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    public partial class Warning
    {
        public Warning()
        {
            Violations = new HashSet<Violation>();
        }

        [Key]
        [Column("warning_id")]
        public int WarningId { get; set; }
        [Required]
        [Column("content")]
        [StringLength(255)]
        public string Content { get; set; }
        [Column("description")]
        [StringLength(500)]
        public string Description { get; set; }

        [InverseProperty(nameof(Violation.Warning))]
        public virtual ICollection<Violation> Violations { get; set; }
    }
}
