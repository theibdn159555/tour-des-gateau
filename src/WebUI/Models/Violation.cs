﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace WebUI.Models
{
    public partial class Violation
    {
        [Key]
        [Column("account_id")]
        public int AccountId { get; set; }
        [Key]
        [Column("warning_id")]
        public int WarningId { get; set; }
        [Column("reason")]
        [StringLength(500)]
        public string Reason { get; set; }
        [Column("created_at", TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column("updated_at", TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }

        [ForeignKey(nameof(AccountId))]
        [InverseProperty("Violations")]
        public virtual Account Account { get; set; }
        [ForeignKey(nameof(WarningId))]
        [InverseProperty("Violations")]
        public virtual Warning Warning { get; set; }
    }
}
