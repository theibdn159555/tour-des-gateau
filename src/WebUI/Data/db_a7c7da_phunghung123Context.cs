﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebUI.Models;

#nullable disable

namespace WebUI.Data
{
    public partial class db_a7c7da_phunghung123Context : DbContext
    {
        public db_a7c7da_phunghung123Context()
        {
        }

        public db_a7c7da_phunghung123Context(DbContextOptions<db_a7c7da_phunghung123Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<CartDetail> CartDetails { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<DiscountEvent> DiscountEvents { get; set; }
        public virtual DbSet<Favorite> Favorites { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<NotificationTemplate> NotificationTemplates { get; set; }
        public virtual DbSet<NotificationType> NotificationTypes { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }
        public virtual DbSet<Size> Sizes { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<Violation> Violations { get; set; }
        public virtual DbSet<Warning> Warnings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
// #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=SQL5097.site4now.net;Initial Catalog=db_a7c7da_phunghung123;User Id=db_a7c7da_phunghung123_admin;Password=01679751807Ph;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasIndex(e => e.Phone, "idx_phone")
                    .IsUnique()
                    .HasFilter("([phone] IS NOT NULL)");

                entity.Property(e => e.Avatar).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.FacebookId).IsUnicode(false);

                entity.Property(e => e.IpAddress).IsUnicode(false);

                entity.Property(e => e.Membership).HasDefaultValueSql("((0))");

                entity.Property(e => e.PasswordHash).IsUnicode(false);

                entity.Property(e => e.PermissionId).HasDefaultValueSql("((4))");

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.Username).IsUnicode(false);

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ACC_PER");
            });

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.HasKey(e => new { e.CartId, e.AccountId })
                    .HasName("PK_CRT");

                entity.Property(e => e.IsOpen).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CRT_ACC");
            });

            modelBuilder.Entity<CartDetail>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.CartId, e.ProductId })
                    .HasName("PK_DCT");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CartDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DCT_PRD");

                entity.HasOne(d => d.Cart)
                    .WithMany(p => p.CartDetails)
                    .HasForeignKey(d => new { d.CartId, d.AccountId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DCT_CRT");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CategoryName).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<DiscountEvent>(entity =>
            {
                entity.HasKey(e => e.EventId)
                    .HasName("PK_EVT");
            });

            modelBuilder.Entity<Favorite>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.ProductId })
                    .HasName("PK_FAV");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Favorites)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FAV_ACC");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Favorites)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FAV_PRD");
            });

            modelBuilder.Entity<Ingredient>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.MaterialId })
                    .HasName("PK_ING");

                entity.HasOne(d => d.Material)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.MaterialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IGD_MAT");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Ingredients)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IGD_PRD");
            });

            modelBuilder.Entity<Material>(entity =>
            {
                entity.Property(e => e.MaterialName).IsUnicode(false);
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.HasKey(e => new { e.NotificationTemplateId, e.AccountId, e.NotificationId })
                    .HasName("PK_NOT");

                entity.Property(e => e.NotificationId).ValueGeneratedOnAdd();

                entity.Property(e => e.Content).IsUnicode(false);

                entity.Property(e => e.IsRead).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NEV_ACC");

                entity.HasOne(d => d.NotificationTemplate)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.NotificationTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NEV_NTE");
            });

            modelBuilder.Entity<NotificationTemplate>(entity =>
            {
                entity.HasKey(e => e.TemplateId)
                    .HasName("PK_NTP");

                entity.Property(e => e.ContentTemplate).IsUnicode(false);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.NotificationTemplates)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_NTP_NTY");
            });

            modelBuilder.Entity<NotificationType>(entity =>
            {
                entity.HasKey(e => e.NoticeTypeId)
                    .HasName("PK_NTY");

                entity.Property(e => e.TypeName).IsUnicode(false);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => new { e.CartId, e.AccountId })
                    .HasName("PK_ORD");

                entity.Property(e => e.BankName).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.ShipAddress).IsUnicode(false);

                entity.Property(e => e.ShipDistrict).IsUnicode(false);

                entity.Property(e => e.ShipFee).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShipWard).IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORD_STT");

                entity.HasOne(d => d.Cart)
                    .WithOne(p => p.Order)
                    .HasForeignKey<Order>(d => new { d.CartId, d.AccountId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ORD_CRT");
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK_STT");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.StatusName).IsUnicode(false);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.PermissionName).IsUnicode(false);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.EventId).HasDefaultValueSql("((1))");

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.ProductName).IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRO_CAT");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_PRD_EVT");

                entity.HasOne(d => d.Size)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.SizeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRD_SIZ");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.TagId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PRD_TAG");
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.UserId })
                    .HasName("PK_RAT");

                entity.Property(e => e.Comment).IsUnicode(true);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Ratings)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RAT_PRD");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Ratings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RAT_ACC");
            });

            modelBuilder.Entity<Size>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.SizeName).IsUnicode(false);
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.Property(e => e.NameTag).IsUnicode(false);
            });

            modelBuilder.Entity<Violation>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.WarningId })
                    .HasName("PK_VIO");

                entity.Property(e => e.Reason).IsUnicode(false);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Violations)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VIO_ACC");

                entity.HasOne(d => d.Warning)
                    .WithMany(p => p.Violations)
                    .HasForeignKey(d => d.WarningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VIO_WAR");
            });

            modelBuilder.Entity<Warning>(entity =>
            {
                entity.Property(e => e.Content).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
