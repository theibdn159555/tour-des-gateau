using AutoMapper;
using WebUI.DTO.System;
using WebUI.Models;

namespace WebUI.Profiles.System
{
    public class LoginProfile : Profile
    {
        public LoginProfile()
        {
            // source => target 
            CreateMap<Account, LoginDTO>();
        }
    }
}