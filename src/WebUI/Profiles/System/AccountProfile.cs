using AutoMapper;
using WebUI.DTO.System;
using WebUI.Models;

namespace WebUI.Profiles.System
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            // source => target 
            CreateMap<Account, AccountDTO>();
        }
    }
}