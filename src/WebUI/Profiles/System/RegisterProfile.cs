using AutoMapper;
using WebUI.DTO.System;
using WebUI.Models;

namespace WebUI.Profiles.System
{
    public class RegisterProfile : Profile
    {
        public RegisterProfile()
        {
            // source => target 
            CreateMap<RegisterDTO, Account>();
        }
    }
}