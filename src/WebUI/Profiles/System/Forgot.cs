using AutoMapper;
using WebUI.DTO.System;
using WebUI.Models;

namespace WebUI.Profiles.System
{
    public class ForgotProfile : Profile
    {
        public ForgotProfile()
        {
            // source => target 
            CreateMap<ForgotDTO, Account>();
        }
    }
}