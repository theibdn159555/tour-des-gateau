using AutoMapper;
using WebUI.DTO.System;
using WebUI.Models;
using WebUI.DTO.Accounts;

namespace WebUI.Profiles.User
{
    public class Update : Profile
    {
        public Update()
        {
            // source => target 
            CreateMap<UpdateDTO, Account>();
        }
    }
}