using System.ComponentModel.DataAnnotations;

namespace WebUI.DTO.System
{
    public class RegisterDTO
    {
        [Required]
        public bool IsAgree { get; set; }

        [Required, StringLength(50 , ErrorMessage = "Maximum username is 50 characters")]
        public string Username { get; set; }

        [Required, StringLength(100 , ErrorMessage = "Maximum password is 50 characters")]
        public string Password { get; set; }

        [Required, StringLength(100 , ErrorMessage = "Maximum confirmpassword is 50 characters")]
        public string ConfirmPassword { get; set; }

        [Required, StringLength(100 , ErrorMessage = "Maximum email is 50 characters")]
        public string Email { get; set; }

        private int permissionId = 4;
        public int PermissionId
        {
            get
            {
                if (permissionId >= 1 && permissionId <= 4) return permissionId;
                return 4;
            }
            set { permissionId = value; }
        }
    }
}

// [StringLength(50, ErrorMessage = "Maximum is 50 characters")]