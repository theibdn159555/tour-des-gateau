using System.ComponentModel.DataAnnotations;
using System;

namespace WebUI.DTO.System
{
    public class AccountDTO
    {
        public int AccountId { get; set; }
        public int PermissionId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public DateTime? Dob { get; set; }
        public string Phone { get; set; }
        public int? Gender { get; set; }
        public double? Membership { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
