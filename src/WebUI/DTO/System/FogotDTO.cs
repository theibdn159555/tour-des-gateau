using System.ComponentModel.DataAnnotations;

namespace WebUI.DTO.System
{
    public class ForgotDTO
    {
        [Required, StringLength(50)]
        public string Username { get; set; }

        [Required, StringLength(100)]
        public string Password { get; set; }

        [Required, StringLength(100)]
        public string ConfirmPassword { get; set; }

        [Required, StringLength(100)]
        public string Email { get; set; }

    }
}
