using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebUI.Data;


using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes;
using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Threading.Tasks;
using WebUI.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;
using WebUI.CreateByHung.SqlQueryRepoes;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.CreateByHung.SqlQueryRepoes.WeakRepoes;
using WebUI.CreateByHung.SqlQueryRepoes.AdminRepoes;

namespace Tour_des_Gateau
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // for get appsettings from anywhere
            services.AddSingleton(Configuration);

            SetupJWTServices(services);

            services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.AddDbContext<db_a7c7da_phunghung123Context>(
                opt => opt.UseSqlServer( Configuration.GetConnectionString("ConnectionOnline"))
                // opt => opt.UseSqlServer( Configuration.GetConnectionString("HungConnection"))
            );
            services.AddControllers().AddNewtonsoftJson(
                s =>
                {
                    s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                }
            );

            services.AddScoped<IRepo<Product>, SqlProductRepo>();
            services.AddScoped<IRepo<Category>, SqlCategoryRepo>();            
            services.AddScoped<IRepo<Size>, SqlSizeRepo>();
            services.AddScoped<IRepo<DiscountEvent>, SqlDiscountEventRepo>();
            services.AddScoped<IRepo<Tag>, SqlTagRepo>();
            services.AddScoped<IRepo<Material>, SqlMaterialRepo>();
            services.AddScoped<IRelationshipRepo<Ingredient>, SqlIngredientRepo>();
            services.AddScoped<IRelationshipRepo<Favorite>, SqlFavoriteRepo>();
            services.AddScoped<IRelationshipRepo<Rating>, SqlRatingRepo>();
            services.AddScoped<IWeakRepo<Cart>, SqlCartRepo>();
            services.AddScoped<IWeakRepo<Order>, SqlOrderRepo>();
            services.AddScoped<IRelationshipRepo<CartDetail>, SqlDetailCartRepo>();
            services.AddScoped<INotificationRepo<Notification>, SqlNotificationRepo>();
            services.AddScoped<IImageRepo, ImageInteractRepo>();
            services.AddScoped<IAdminRepo, SqlAdminRepo>();
            services.AddScoped<IViolationRepo, SqlViolationRepo>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        private void SetupJWTServices(IServiceCollection services)
        {
            string key = Configuration["Key"]; //this should be same which is used while creating token      
            var issuer = Configuration["Issuer"];  //this should be same which is used while creating token  

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = issuer,
                    ValidAudience = issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key))
                };

                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            //receive raw req
            app.Use((context, next) =>
            {
                context.Request.EnableBuffering(); // calls EnableRewind() `https://github.com/dotnet/aspnetcore/blob/4ef204e13b88c0734e0e94a1cc4c0ef05f40849e/src/Http/Http/src/Extensions/HttpRequestRewindExtensions.cs#L23`
                return next();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
