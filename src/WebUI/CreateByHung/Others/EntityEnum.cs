namespace WebUI.CreateByHung.Others
{
    public enum EntityEnum
    {
        Category,
        Product,
        Size,
        Tag,
        DiscountEvent,
        Material,
        Account
    }
}