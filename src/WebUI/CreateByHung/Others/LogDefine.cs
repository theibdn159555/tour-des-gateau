using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WebUI.CreateByHung.Others
{
    public static class LogDefine
    {
        public static void LogOut(object obj){
            System.Console.WriteLine(JsonConvert.SerializeObject(obj));
        }
        public static void LogOutList<TSource>(this IEnumerable<TSource> source){
            foreach (var item in source)
            {
                System.Console.WriteLine(JsonConvert.SerializeObject(item));
            }
        }
        public static void LogOut<TObject>(this TObject obj)
        {
            System.Console.WriteLine(JsonConvert.SerializeObject(obj));
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
        (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static IEnumerable<T> DistinctBy2<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }
    }
}