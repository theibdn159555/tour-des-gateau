namespace WebUI.CreateByHung.Others
{
    public static class ValidationInputData
    {
        public static bool IsDatetTimeValid(int year, int month)
        {
            if(year > 1999 && year < 2051)
            {
                if(month > 0 && month < 13)
                {
                    return true;
                }
            }
            return false;
        }
        public static void IsTargetNotNull(object obj)
        {
            if(obj.Equals(null))
                throw new System.Exception($"Object {nameof(obj)} is null");
        }
    }
}