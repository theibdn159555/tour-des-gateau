using Newtonsoft.Json;
using WebUI.Models;

namespace WebUI.CreateByHung.Others
{
    public static class PowerTools
    {
        public static T CloneObject<T>(this T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }
        public static Product MappingToProductModel(Product item)
        {
            return new Product()
            {
                ProductId = item.ProductId,
                ProductName = item.ProductName,
                UnitPrice = item.UnitPrice,
                CategoryId = item.CategoryId,
                SizeId = item.SizeId,
                TagId = item.TagId,
                EventId = item.EventId,
                Image = item.Image,
                Description = item.Description,
                CreatedAt = item.CreatedAt,
                UpdatedAt = item.UpdatedAt
                        
            };
        }
        public static Account MappingToAccountModel(Account account)
        {
            return new Account()
            {
                Username = account.Username,
                Email = account.Email,
                PermissionId = account.PermissionId,
                Membership = account.Membership
            };
        }
    }
}