namespace WebUI.CreateByHung.Others
{
    public class YearMonthQuery
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
    }
}