using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.IngredientDTOs
{
    public class IngredientUpdateDTO
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int MaterialId { get; set; }
        public double? Quantity { get; set; }
        public bool Main { get; set; }
    }
}