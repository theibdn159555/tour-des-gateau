using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.IngredientDTOs
{
    public class IngredientReadDTO
    {
        public int ProductId { get; set; }
        public int MaterialId { get; set; }
        public double? Quantity { get; set; }
        public bool Main { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        private Material material;
        public Material Material
        {
            get {
                if(material == null)
                {
                    return null;
                }
                return new Material()
                {
                    MaterialId = material.MaterialId,
                    MaterialName = material.MaterialName
                }; 
            }
            set { material = value; }
        }
        private Product product;
        public Product Product
        {
            get { 
                if(product == null)
                {
                    return null;
                }
                return MappingRelationship.MappingToProductModel(product); 
            }
            set { product = value; }
        }
        
    }
}