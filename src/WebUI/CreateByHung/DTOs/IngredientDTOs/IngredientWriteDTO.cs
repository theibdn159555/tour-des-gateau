using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.IngredientDTOs
{
    public class IngredientWriteDTO
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int MaterialId { get; set; }
        [Required]
        public double? Quantity { get; set; }
        [Required]
        public bool Main { get; set; }
    }
}