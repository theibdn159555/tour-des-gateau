using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.AdminDTOs
{
    public class AdminUpdateDTO
    {
        
        [Required]
        public int AccountId { get; set; }
        [Required]
        public int newPermission { get; set; }
        
    }
}