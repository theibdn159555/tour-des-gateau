using System;

namespace WebUI.CreateByHung.DTOs.ViolationDTOs
{
    public class ViolationReadDTO
    {
        
        public int AccountId { get; set; }
        public int WarningId { get; set; }
        public string Reason { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}