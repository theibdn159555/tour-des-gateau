using System;

namespace WebUI.CreateByHung.DTOs.ViolationDTOs
{
    public class ViolationWriteDTO
    {
        public int AccountId { get; set; }
        public int WarningId { get; set; }
        public string Reason { get; set; }
    }
}