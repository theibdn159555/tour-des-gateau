using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.SizeDTOs
{
    public class SizeWriteDTO{
        
        [Required]
        [StringLength(40)]
        public string SizeName { get; set; }

        [Required]        
        [StringLength(255)]
        public string Description { get; set; }
    }
}