using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.RatingDTOs
{
    public class RatingSummaryDTO
    {
        public int ProductId { get; set; }
        public int FiveStar { get; set; }
        public int FourStar { get; set; }
        public int ThreeStar { get; set; }
        public int TwoStar { get; set; }
        public int OneStar { get; set; }

        private int sumAll;
        public int SumAll
        {
            get {
                if(sumAll == 0)
                {
                    sumAll = FiveStar + FourStar + ThreeStar + TwoStar + OneStar;
                }
                return sumAll; 
            }
            set { sumAll = value; }
        }        
        public double AverageStar { get; set; }
        private Product product;
        public Product Product
        {
            get { 
                if(product == null)
                {
                    return null;
                }
                return MappingRelationship.MappingToProductModel(product); 
            }
            set { product = value; }
        }
    }
}