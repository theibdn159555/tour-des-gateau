using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.RatingDTOs
{
    public class RatingUpdateDTO
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int StarRating { get; set; }
        public string Comment { get; set; }
    }
}