using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.RatingDTOs
{
    public class RatingReadDTO
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public int StarRating { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        private Account user;
        public Account User
        {
            get {
                if(user == null)
                {
                    return null;
                }
                return MappingRelationship.MappingToAccountModel(user);
            }
            set { user = value; }
        }
        // private Product product;
        // public Product Product
        // {
        //     get { 
        //         if(product == null)
        //         {
        //             return null;
        //         }
        //         return MappingRelationship.MappingToProductModel(product); 
        //     }
        //     set { product = value; }
        // }
    }
}