using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.NotificationDTOs
{
    public class NotificationReadDTO
    {
        public int NotificationTemplateId { get; set; }
        public int AccountId { get; set; }
        public int NotificationId { get; set; }
        public string Content { get; set; }
        public bool? IsRead { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Account Account { get; set; }

        private NotificationTemplate notificationTemplate;
        public NotificationTemplate NotificationTemplate
        {
            get {
                if(notificationTemplate.Notifications != null)
                {
                    notificationTemplate.Notifications = null;
                }
                if(notificationTemplate.Type.NotificationTemplates != null)
                {
                    notificationTemplate.Type.NotificationTemplates = null;
                }
                return notificationTemplate; 
            }
            set { notificationTemplate = value; }
        }
    }
}