using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.NotificationDTOs
{
    public class NotificationUpdateDTO
    {
        public int AccountId { get; set; }
        public int NotificationId { get; set; }
        public bool? IsRead { get; set; }
    }
}