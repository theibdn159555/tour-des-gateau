using System.Collections.Generic;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.MaterialDTOs
{
    public class MaterialReadDTO
    {
        public int MaterialId { get; set; }
        public string MaterialName { get; set; }
        private ICollection<Ingredient> ingredients;
        public ICollection<Ingredient> Ingredients
        {
            get {
                if(ingredients == null)
                    return null;
                
                ICollection<Ingredient> includeProducts = new List<Ingredient>();
                foreach (var item in ingredients)
                {
                    // var tempProduct = MappingRelationship.MappingToProductModel(item.Product);
                    Ingredient temp = new Ingredient()
                    {
                        // Product = tempProduct != null ? tempProduct : null,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity,
                        Main = item.Main
                    };
                    includeProducts.Add(temp);
                }
                return ingredients; 
            }
            set { ingredients = value; }
        }
        
    }
}