using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.MaterialDTOs
{
    public class MaterialUpdateDTO
    {
        [Required(ErrorMessage = "The field need to fill")]
        [StringLength(255, ErrorMessage = "Tag name is too long")]
        public string MaterialName { get; set; }
    }
}