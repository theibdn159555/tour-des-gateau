using System;
using WebUI.CreateByHung.DTOs.CartDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.Orders
{
    public class OrderReadDTO
    {
        public int CartId { get; set; }
        public int AccountId { get; set; }
        public int StatusId { get; set; }
        public string Phone { get; set; }
        public string ReceiverFirstName { get; set; }
        public string ReceiverLastName { get; set; }
        public int PaymentMethod { get; set; }
        public string BankName { get; set; }
        public double? ShipFee { get; set; }
        public string Note { get; set; }
        public string ShipAddress { get; set; }
        public string ShipWard { get; set; }
        public string ShipDistrict { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        private double finalTotalPrice;
        public double FinalTotalPrice
        {
            get { 
                if(finalTotalPrice == 0)
                {
                    if(CartDto == null) return 0;
                    finalTotalPrice = CartDto.TotalPrice + (double)ShipFee;
                }
                return finalTotalPrice; 
            }
            set { finalTotalPrice = value; }
        }
        private Cart cart;
        public Cart Cart
        {
            get { 
                if(cart == null) return null;
                return cart; 
            }
            set { cart = value; }
        }
        
        private OrderStatus orderStatus;
        public OrderStatus Status
        {
            get {
                if(orderStatus == null) return null;
                orderStatus.Orders = null;
                return orderStatus;
            }
            set { orderStatus = value; }
        }
        private CartReadDTO cartDto;
        public CartReadDTO CartDto
        {
            get { 
                if(cartDto == null)
                {
                    if(Cart == null) return null;
                    cartDto = new CartReadDTO()
                    {
                        Account = Cart.Account,
                        CartDetails = Cart.CartDetails
                    };
                }
                return cartDto;
            }
            set { cartDto = value; }
        }
    }
}