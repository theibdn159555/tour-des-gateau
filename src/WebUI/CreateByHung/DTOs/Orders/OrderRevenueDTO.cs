namespace WebUI.CreateByHung.DTOs.Orders
{
    public class OrderRevenueDTO
    {
        public decimal TotalRevenue { get; set; }
        public bool? IsGrow { get; set; }
        public string Error { get; set; }
    }
}