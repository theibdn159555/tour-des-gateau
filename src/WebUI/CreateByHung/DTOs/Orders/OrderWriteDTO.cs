using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.Orders
{
    public class OrderWriteDTO
    {
        [Required]
        public int AccountId { get; set; }
        [Required]
        public int PaymentMethod { get; set; }
        public string BankName { get; set; }
        public string ReceiverFirstName { get; set; }
        public string ReceiverLastName { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        [Required]
        public string ShipAddress { get; set; }
        [Required]
        public string ShipWard { get; set; }
        [Required]
        public string ShipDistrict { get; set; }
    }
}