using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.Orders
{
    public class OrderUpdateDTO
    {
        public int CartId { get; set; }
        public int AccountId { get; set; }
        public int StatusId { get; set; }
        public string Note { get; set; }
    }
}