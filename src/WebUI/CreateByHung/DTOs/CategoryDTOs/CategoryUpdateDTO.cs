using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebUI.CreateByHung.DTOs.CategoryDTOs
{
    public class CategoryUpdateDTO
    {
        [Required]
        [StringLength(255)]
        public string CategoryName { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
    }
}
