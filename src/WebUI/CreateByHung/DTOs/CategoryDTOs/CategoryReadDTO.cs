using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Others;
using WebUI.Models;

// #nullable disable

namespace WebUI.CreateByHung.DTOs.CategoryDTOs
{
    public class CategoryReadDTO
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }

        private ICollection<Product> products;
        public ICollection<Product> Products
        {
            get { 
                if(products == null){
                    return null;
                }
                ICollection<Product> includeProducts = new List<Product>();
                foreach (var item in products)
                {
                    Product temp = MappingRelationship.MappingToProductModel(item);
                    includeProducts.Add(temp);
                }
                return includeProducts;

            }
            set { products = value; }
        }
        
    }
}
