using System;
using System.Collections.Generic;
using WebUI.CreateByHung.DTOs.CartDetailDTOs;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.CartDTOs
{
    public class CartReadDTO
    {
        public int CartId { get; set; }
        public int AccountId { get; set; }
        public bool? IsOpen { get; set; }
        private Account account;
        public Account Account
        {
            get { return account == null ? null : MappingRelationship.MappingToAccountModel(account); }
            set { account = value; }
        }        
        public ICollection<CartDetail> CartDetails { get; set; }
        public Order Orders { get; set; }
        private double totalPrice = 0;
        public double TotalPrice
        {
            get {
                if(totalPrice == 0)
                {
                    foreach (var item in DetailDto)
                    {
                        totalPrice += item.FinalPrice;
                    }
                }
                return totalPrice; 
            }
            set { totalPrice = value; }
        }
        
        private ICollection<DetailReadDTO> detailDto;
        public ICollection<DetailReadDTO> DetailDto
        {
            get {
                if(detailDto == null)
                {
                    detailDto = new List<DetailReadDTO>();
                    foreach (var item in CartDetails)
                    {
                        double member = GetMemberValue(account, item);
                        double finalPrice = (double)item.Quantity * (double)item.UnitPrice * (1 -  item.DiscountValue - member);
                        DetailReadDTO temp = new DetailReadDTO(){
                            CartId = item.CartId,
                            AccountId = item.AccountId,
                            ProductId = item.ProductId,
                            Quantity = item.Quantity,
                            DiscountValue = item.DiscountValue,
                            UnitPrice = item.UnitPrice,
                            CreatedAt = item.CreatedAt,
                            UpdatedAt = item.UpdatedAt,
                            Product = item.Product,
                            FinalPrice = finalPrice,
                            MemberValue = member
                        };
                        detailDto.Add(temp);
                    }
                }
                
                return detailDto; 
            }
            set { detailDto = value; }
        }
        public double GetMemberValue(Account account, CartDetail cartItem)
        {
            // System.Console.WriteLine("account " + account.UnlockedMemberAt);
            // System.Console.WriteLine("cartItem: " + cartItem.CreatedAt);
            if(account.UnlockedMemberAt == null) return 0;
            if(
                ((DateTime)account.UnlockedMemberAt).Date
                    .CompareTo(((DateTime)cartItem.CreatedAt).Date) <= 0
            )
            {
                return (double)account.Membership;
            }
            return 0;
        }
    }
}