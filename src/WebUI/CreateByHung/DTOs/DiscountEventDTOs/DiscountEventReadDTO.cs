using System;
using System.Collections.Generic;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.DiscountEventDTOs
{
    public class DiscountEventReadDTO
    {
        private ICollection<Product> products;

        public int EventId { get; set; }
        public string EventName { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public double? DiscountValue { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ICollection<Product> Products
        {
            get { 
                if(products == null){
                    return null;
                }
                ICollection<Product> includeProducts = new List<Product>();
                foreach (var item in products)
                {
                    Product temp = MappingRelationship.MappingToProductModel(item);
                    includeProducts.Add(temp);
                }
                return includeProducts;

            }
            set { products = value; }
        }
    }
}