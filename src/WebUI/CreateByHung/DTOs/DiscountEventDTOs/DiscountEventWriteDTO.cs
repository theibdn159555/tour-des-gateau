using System;
using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs
{
    public class DiscountEventWriteDTO
    {
        [Required]
        [StringLength(255)]
        public string EventName { get; set; }
        [Required]
        public DateTime? DateStart { get; set; }
        [Required]
        public DateTime? DateEnd { get; set; }
        [Required]
        public double? DiscountValue { get; set; }
    }
}