using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.TagDTOs
{
    public class TagWriteDTO
    {
        [Required(ErrorMessage = "The field need to fill")]
        [StringLength(255, ErrorMessage = "Tag name is too long")]
        public string NameTag { get; set; }
    }
}