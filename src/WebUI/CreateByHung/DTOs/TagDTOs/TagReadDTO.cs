using System.Collections.Generic;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.TagDTOs
{
    public class TagReadDTO
    {
        public int TagId { get; set; }
        public string NameTag { get; set; }
        private ICollection<Product> products;
        public ICollection<Product> Products
        {
            get { 
                if(products == null){
                    return null;
                }
                ICollection<Product> includeProducts = new List<Product>();
                foreach (var item in products)
                {
                    Product temp = MappingRelationship.MappingToProductModel(item);
                    includeProducts.Add(temp);
                }
                return includeProducts;

            }
            set { products = value; }
        }
    }
}