using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.CartDetailDTOs
{
    public class DetailWriteDTO
    {
        [Required]
        public int AccountId { get; set; }
        public int CartId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}