using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.CartDetailDTOs
{
    public class DetailReadDTO
    {
        public int AccountId { get; set; }
        public int CartId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double DiscountValue { get; set; }
        public decimal? UnitPrice { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        private Product product;
        public Product Product
        {
            get { 
                if(product == null)
                    return null;
                Product temp = MappingRelationship.MappingToProductModel(product);
                if(temp.Size != null ) temp.Size.Products = null;
                product = temp;
                return product;
            }
            set { product = value; }
        }
        public Cart Cart { get; set; }
        public double FinalPrice { get; set; }
        public double MemberValue { get; set; }
    }
}