using Microsoft.AspNetCore.Http;
using WebUI.CreateByHung.Others;

namespace WebUI.CreateByHung.DTOs.ImageDTOs
{
    public class ImageUploadDTO
    {
        public int ID { get; set; }
        public string OwnerName { get; set; }
        public IFormFile Image { get; set; }
        public EntityEnum EntityType { get; set; }
    }
}