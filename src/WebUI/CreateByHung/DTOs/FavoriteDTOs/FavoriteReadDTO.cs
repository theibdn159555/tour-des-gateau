using System;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.FavoriteDTOs
{
    public class FavoriteReadDTO
    {
        public int AccountId { get; set; }
        public int ProductId { get; set; }
        public DateTime CreatedAt { get; set; }
        private Account account;
        public Account Account
        {
            get {
                if(account == null)
                {
                    return null;
                }
                return MappingRelationship.MappingToAccountModel(account);
            }
            set { account = value; }
        }
        private Product product;
        public Product Product
        {
            get { 
                if(product == null)
                {
                    return null;
                }
                return MappingRelationship.MappingToProductModel(product); 
            }
            set { product = value; }
        }
    }
}