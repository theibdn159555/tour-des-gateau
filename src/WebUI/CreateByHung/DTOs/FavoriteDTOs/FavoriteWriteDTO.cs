using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.FavoriteDTOs
{
    public class FavoriteWriteDTO
    {
        [Required]
        public int AccountId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}