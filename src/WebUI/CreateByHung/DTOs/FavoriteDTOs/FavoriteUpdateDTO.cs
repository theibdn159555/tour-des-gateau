using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.FavoriteDTOs
{
    public class FavoriteUpdateDTO
    {
        [Required]
        public int AccountId { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}