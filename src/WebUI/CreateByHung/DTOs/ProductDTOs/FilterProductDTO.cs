using System.Collections.Generic;
using WebUI.CreateByHung.DTOs.CategoryDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.ProductDTOs
{
    public class FilterProductDTO
    {
        public IEnumerable<int> Categories { get; set; }
        public int? SizeId { get; set; }
        public int? PriceRange { get; set; }
        public bool? OrderByName { get; set; }
    }
}