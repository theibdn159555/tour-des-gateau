using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.CategoryDTOs;
using WebUI.CreateByHung.DTOs.DiscountEventDTOs;
using WebUI.CreateByHung.DTOs.SizeDTOs;
using WebUI.CreateByHung.DTOs.TagDTOs;
using WebUI.Models;

// #nullable disable

namespace WebUI.CreateByHung.DTOs.ProductDTOs
{
    public class ProductReadDTO
    {
        public int ProductId { get; set;}
        public int CategoryId { get; set; }
        public int SizeId { get; set; }
        public int TagId { get; set; }
        public int? EventId { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        private Category category;
        public Category Category
        {
            get {
                if(category == null){
                    return null;
                }
                CategoryReadDTO readDTO = new CategoryReadDTO(){
                    CategoryId = category.CategoryId,
                    CategoryName = category.CategoryName,
                    Description = category.Description
                };
                return new Category(){
                    CategoryId = readDTO.CategoryId,
                    CategoryName = readDTO.CategoryName,
                    Description = readDTO.Description
                }
                ;
            }
            set { category = value; }
        }
        
        private Size size;
        public Size Size
        {
            get {
                if(size == null){
                    return null;
                }
                SizeReadDTO readDTO = new SizeReadDTO(){
                    SizeId = size.SizeId,
                    SizeName = size.SizeName,
                    Description = size.Description
                };
                return new Size(){
                    SizeId = readDTO.SizeId,
                    SizeName = readDTO.SizeName,
                    Description = readDTO.Description
                }                ;
            }
            set { size = value; }
        }
        
        private DiscountEvent discountEvent;
        public DiscountEvent Event
        {
            get {
                if(discountEvent == null){
                    return null;
                }
                DiscountEventReadDTO readDTO = new DiscountEventReadDTO(){
                    EventId = discountEvent.EventId,
                    EventName = discountEvent.EventName,
                    DateStart = discountEvent.DateStart,
                    DateEnd = discountEvent.DateEnd,
                    DiscountValue = discountEvent.DiscountValue
                };
                return new DiscountEvent(){
                    EventId = readDTO.EventId,
                    EventName = readDTO.EventName,
                    DateStart = readDTO.DateStart,
                    DateEnd = readDTO.DateEnd,
                    DiscountValue = readDTO.DiscountValue
                };
            }
            set { discountEvent = value; }
        }
        
        private Tag tag;
        public Tag Tag
        {
            get {
                if(tag == null){
                    return null;
                }
                TagReadDTO readDTO = new TagReadDTO(){
                    TagId = tag.TagId,
                    NameTag = tag.NameTag
                };
                return new Tag(){
                    TagId = tag.TagId,
                    NameTag = tag.NameTag
                };
            }
            set { tag = value; }
        }
        
    }
}