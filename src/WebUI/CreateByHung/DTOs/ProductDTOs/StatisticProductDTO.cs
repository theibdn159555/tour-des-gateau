using System.Collections.Generic;
using WebUI.Models;

namespace WebUI.CreateByHung.DTOs.ProductDTOs
{
    public class StatisticProductDTO
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public decimal ProductPrice { get; set; }
        public string Size { get; set;}
        public Product Product { get; set; }
        public List<PerMonth> QuantityPerMonth { get; set; }
        public int Year { get; set; }

        public class PerMonth
        {
            public int Quantity { get; set; }
            public int Month { get; set; }
        }
        public int SoldQuantites { get; set;}
        public int TotalFollowers { get; set; }
        public double AverageStar { get; set; }

    }
}