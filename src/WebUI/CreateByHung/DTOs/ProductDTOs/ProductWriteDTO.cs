﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.CreateByHung.DTOs.ProductDTOs
{
    public class ProductWriteDTO
    {
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int SizeId { get; set; }
        [Required]
        public int TagId { get; set; }
        [Required]
        public int EventId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        public string Image { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
