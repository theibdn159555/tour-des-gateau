using AutoMapper;
using WebUI.CreateByHung.DTOs.TagDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<Tag, TagReadDTO>();
            CreateMap<TagWriteDTO, Tag>();
            CreateMap<TagUpdateDTO, Tag>();
            CreateMap<Tag, TagUpdateDTO>();
        }
    }
}