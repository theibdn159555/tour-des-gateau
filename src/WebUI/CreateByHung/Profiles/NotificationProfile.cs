using AutoMapper;
using WebUI.CreateByHung.DTOs.NotificationDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class NotificationProfile : Profile
    {
        public NotificationProfile()
        {
            CreateMap<Notification, NotificationReadDTO>();
            CreateMap<NotificationUpdateDTO, Notification>();
            CreateMap<Notification, NotificationUpdateDTO>();
        }
    }
}