using AutoMapper;
using WebUI.CreateByHung.DTOs.SizeDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class SizeProfile : Profile
    {
        public SizeProfile()
        {
            //Source -> Target
            CreateMap<Size, SizeReadDTO>();
            CreateMap<SizeWriteDTO, Size>();
            CreateMap<SizeUpdateDTO, Size>();
            CreateMap<Size, SizeUpdateDTO>();
        }
    }
}