using AutoMapper;
using WebUI.CreateByHung.DTOs.FavoriteDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class FavoriteProfile : Profile
    {
        public FavoriteProfile()
        {
            CreateMap<Favorite, FavoriteReadDTO>();
            CreateMap<FavoriteWriteDTO, Favorite>();
            CreateMap<FavoriteUpdateDTO, Favorite>();
            CreateMap<Favorite, FavoriteUpdateDTO>();
        }
    }
}