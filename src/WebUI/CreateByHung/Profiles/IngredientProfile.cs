using AutoMapper;
using WebUI.CreateByHung.DTOs.IngredientDTOs;
using WebUI.CreateByHung.IngredientDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class IngredientProfile : Profile
    {
        public IngredientProfile()
        {
            CreateMap<Ingredient, IngredientReadDTO>();
            CreateMap<IngredientWriteDTO, Ingredient>();
            CreateMap<IngredientUpdateDTO, Ingredient>();
            CreateMap<Ingredient, IngredientUpdateDTO>();
        }
    }
}