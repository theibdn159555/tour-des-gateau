using AutoMapper;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductReadDTO>();
            CreateMap<ProductWriteDTO, Product>();
            CreateMap<ProductUpdateDTO, Product>();
            CreateMap<Product, ProductUpdateDTO>();
        }
    }
}