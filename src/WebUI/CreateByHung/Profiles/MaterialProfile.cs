using AutoMapper;
using WebUI.CreateByHung.DTOs.MaterialDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class MaterialProfile : Profile
    {
        public MaterialProfile()
        {
            CreateMap<Material, MaterialReadDTO>();
            CreateMap<MaterialWriteDTO, Material>();
            CreateMap<MaterialUpdateDTO, Material>();
            CreateMap<Material, MaterialUpdateDTO>();
        }
    }
}