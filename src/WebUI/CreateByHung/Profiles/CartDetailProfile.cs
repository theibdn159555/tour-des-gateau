using AutoMapper;
using WebUI.CreateByHung.DTOs.CartDetailDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class CartDetailProfile : Profile
    {
        public CartDetailProfile()
        {
            CreateMap<CartDetail, DetailReadDTO>();
            CreateMap<DetailWriteDTO, CartDetail>();
            CreateMap<DetailUpdateDTO, CartDetail>();
            CreateMap<CartDetail, DetailUpdateDTO>();
        }
    }
}