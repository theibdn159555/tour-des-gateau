using AutoMapper;
using WebUI.CreateByHung.DTOs.CartDTOs;
using WebUI.Models;

namespace WebUI.Profiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<Cart, CartReadDTO>();
        }
    }
}