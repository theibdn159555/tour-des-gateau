using AutoMapper;
using WebUI.Models;
using WebUI.CreateByHung.DTOs.ViolationDTOs;

namespace WebUI.CreateByHung.Profiles
{
    public class ViolationProfile : Profile
    {
        public ViolationProfile()
        {
            CreateMap<Violation, ViolationReadDTO>();
            CreateMap<ViolationWriteDTO, Violation>();
            // ViolationReadDTO
            // ViolationWriteDTO
        }
    }
}