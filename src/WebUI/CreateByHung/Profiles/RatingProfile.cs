using AutoMapper;
using WebUI.CreateByHung.DTOs.RatingDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class RatingProfile : Profile
    {
        public RatingProfile()
        {
            CreateMap<Rating, RatingReadDTO>();
            CreateMap<RatingWriteDTO, Rating>();
            CreateMap<RatingUpdateDTO, Rating>();
            CreateMap<Rating, RatingUpdateDTO>();
        }
    }
}