using AutoMapper;
using WebUI.CreateByHung.DTOs.CategoryDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            //Source -> Target
            CreateMap<Category, CategoryReadDTO>();
            CreateMap<CategoryWriteDTO, Category>();
            CreateMap<CategoryUpdateDTO, Category>();
            CreateMap<Category, CategoryUpdateDTO>();
        }
    }
}