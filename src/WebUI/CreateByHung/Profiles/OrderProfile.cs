using AutoMapper;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderReadDTO>();
            CreateMap<OrderWriteDTO, Order>();
            CreateMap<OrderUpdateDTO, Order>();
            CreateMap<Order, OrderUpdateDTO>();
        }
    }
}