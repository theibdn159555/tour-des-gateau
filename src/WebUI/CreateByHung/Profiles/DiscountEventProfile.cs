using AutoMapper;
using WebUI.CreateByHung.DTOs;
using WebUI.CreateByHung.DTOs.DiscountEventDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Profiles
{
    public class DiscountEventProfile : Profile
    {
        public DiscountEventProfile()
        {
            //Source -> Target
            CreateMap<DiscountEvent, DiscountEventReadDTO>();
            CreateMap<DiscountEventWriteDTO, DiscountEvent>();
            CreateMap<DiscountEventUpdateDTO, DiscountEvent>();
            CreateMap<DiscountEvent, DiscountEventUpdateDTO>();
        }
    }
}