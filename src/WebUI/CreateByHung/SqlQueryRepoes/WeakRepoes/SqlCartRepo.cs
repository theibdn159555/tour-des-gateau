using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.WeakRepoes
{
    public class SqlCartRepo : IWeakRepo<Cart>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlCartRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public Task<int> CreateNew(Cart target)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteOld(Cart target)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<Cart>> GetAll()
        {
            IEnumerable<Cart> carts = _context.Carts.Include(c => c.CartDetails).ToList();
            return Task.FromResult(carts);
        }

        public Task<Cart> GetExactlyByID(int ownerId, int target)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Cart> GetAbleCartByID(int ownerId)
        {
            int expired = await SqlViolationRepo.IsViolationExpired(ownerId);
            if(expired < 0)
            {
                throw new Exception("This account is banned");
            }
            var able = await _context.Carts
                .Where(
                    c => (bool)c.IsOpen && c.AccountId == ownerId)
                .Include(
                    c => c.Account)
                .Include(
                    c => c.CartDetails)
                    .ThenInclude(
                        p => p.Product)
                        .ThenInclude(
                            s => s.Size)
                .Select(cart => new Cart{
                    CartId = cart.CartId,
                    AccountId = cart.AccountId,
                    IsOpen = cart.IsOpen,
                    CreatedAt = cart.CreatedAt,
                    UpdatedAt = cart.UpdatedAt,
                    CartDetails = cart.CartDetails.Select(
                        detail => new CartDetail{
                            AccountId = detail.AccountId,
                            CartId = detail.CartId,
                            ProductId = detail.ProductId,
                            UnitPrice = detail.UnitPrice,
                            Quantity = detail.Quantity,
                            DiscountValue = detail.DiscountValue,
                            CreatedAt = detail.CreatedAt,
                            UpdatedAt = detail.UpdatedAt,
                            Product = new Product{
                                ProductId = detail.ProductId,
                                ProductName = detail.Product.ProductName,
                                UnitPrice = detail.Product.UnitPrice,
                                EventId = detail.Product.EventId,
                                Event = detail.Product.Event,
                                Size = detail.Product.Size,
                                SizeId = detail.Product.SizeId,
                                Image = detail.Product.Image
                            }
                        }
                    ).ToList(),
                    Account = new Account{
                        AccountId = cart.Account.AccountId,
                        Membership = cart.Account.Membership,
                        UnlockedMemberAt = cart.Account.UnlockedMemberAt,
                        FirstName = cart.Account.FirstName,
                        LastName = cart.Account.LastName,
                        Email = cart.Account.Email
                    }
                })
                .FirstOrDefaultAsync();
            
            if(able.CartDetails.Any(
                item => item.DiscountValue != 0 && !((DateTime)item.CreatedAt).Date.Equals(DateTime.Now.Date)
            ))
            {
                var cartItems = _context.CartDetails.Where(
                    d => d.CartId == able.CartId && d.AccountId == able.AccountId
                ).FirstOrDefault();
                _context.CartDetails.Remove(cartItems);
                await SaveChanges();
            }
            able = await _context.Carts
                .Where(
                    c => (bool)c.IsOpen && c.AccountId == ownerId)
                .Include(
                    c => c.Account)
                .Include(
                    c => c.CartDetails)
                    .ThenInclude(
                        p => p.Product)
                        .ThenInclude(
                            s => s.Size)
                            .Select(cart => new Cart{
                    CartId = cart.CartId,
                    AccountId = cart.AccountId,
                    IsOpen = cart.IsOpen,
                    CreatedAt = cart.CreatedAt,
                    UpdatedAt = cart.UpdatedAt,
                    CartDetails = cart.CartDetails.Select(
                        detail => new CartDetail{
                            AccountId = detail.AccountId,
                            CartId = detail.CartId,
                            ProductId = detail.ProductId,
                            UnitPrice = detail.UnitPrice,
                            Quantity = detail.Quantity,
                            DiscountValue = detail.DiscountValue,
                            CreatedAt = detail.CreatedAt,
                            UpdatedAt = detail.UpdatedAt,
                            Product = new Product{
                                ProductId = detail.ProductId,
                                ProductName = detail.Product.ProductName,
                                UnitPrice = detail.Product.UnitPrice,
                                EventId = detail.Product.EventId,
                                Event = detail.Product.Event,
                                Size = detail.Product.Size,
                                SizeId = detail.Product.SizeId,
                                Image = detail.Product.Image
                            }
                        }
                    ).ToList(),
                    Account = new Account{
                        AccountId = cart.Account.AccountId,
                        Membership = cart.Account.Membership,
                        UnlockedMemberAt = cart.Account.UnlockedMemberAt,
                        FirstName = cart.Account.FirstName,
                        LastName = cart.Account.LastName,
                        Email = cart.Account.Email
                    }
                })
                .FirstOrDefaultAsync();
            return able;
        }

        public Task<IEnumerable<Cart>> GetListByID(int ownerId)
        {
            IEnumerable<Cart> carts = _context.Carts
                .Where(
                    c => c.AccountId == ownerId)
                .Include(
                    c => c.Account)
                .Include(
                    c => c.CartDetails)
                .ThenInclude(
                    d => d.Product).ToList();
            return Task.FromResult(carts);
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateOld(Cart target)
        {
            
        }
    }
}