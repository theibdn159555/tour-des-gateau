using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.CartDTOs;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Paginations;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.WeakRepoes
{
    public class SqlOrderRepo : IWeakRepo<Order>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlOrderRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<int> CreateNew(Order target)
        {
            var cart = _context.Carts
                .Include(c => c.CartDetails)
                .Include(c => c.Account)
                .Where(c => c.AccountId == target.AccountId && (bool)c.IsOpen)
                .FirstOrDefault();

            if (cart.CartDetails.Count() == 0)
            {
                throw new Exception("Can not make an Order without any items");
            }

            CartReadDTO cartReadDTO = new CartReadDTO()
            {
                CartDetails = cart.CartDetails,
                Account = cart.Account
            };
            target.CartId = cart.CartId;
            target.StatusId = 6; //"New" status

            double totalPrice = cartReadDTO.TotalPrice;

            int expired = await SqlViolationRepo.IsViolationExpired(target.AccountId);
            if (expired < 0)
            {
                throw new Exception("This account is banned");
            }
            switch (expired)
            {
                case -1:
                    throw new Exception("This account is banned");
                case 0:
                    break;
                default:
                    if (totalPrice > 100000)
                    {
                        throw new Exception("This account's warrant is being applied so that he/she can not make an Order greater than 100000 VND");
                    }
                    break;
            }

            int quantity = GetQuantityItems(cartReadDTO.CartDetails);
            target.ShipFee = GetShipFee(quantity) * totalPrice;

            if (target.Phone == null || target.Phone.Equals(null) || target.Phone == "")
            {
                target.Phone = cart.Account.Phone;
            }
            if (target.PaymentMethod == 1) target.BankName = "No Bank";
            target.Note = "No Note";

            await _context.Orders.AddAsync(target);
            await SaveChanges();

            return target.AccountId;
            //return Task.FromResult(target.AccountId);
        }

        public async Task<Cart> GetCartAble(int id)
        {
            SqlCartRepo cartRepo = new SqlCartRepo(_context);
            var cart = await cartRepo.GetAbleCartByID(id);
            return cart;
        }
        public static int GetQuantityItems(IEnumerable<CartDetail> cartDetails)
        {
            int quantity = 0;
            foreach (var item in cartDetails)
            {
                quantity += item.Quantity;
            }
            return quantity;
        }

        public static double GetShipFee(int quantity)
        {
            if (quantity > 5 && quantity <= 10)
            {
                return 0.05;
            }
            else if (quantity > 10 && quantity <= 20)
            {
                return 0.1;
            }
            else if (quantity > 20)
            {
                return 0.15;
            }
            else
            {
                return 0;
            }
        }

        public void DeleteOld(Order target)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<Order>> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Task<Order> GetExactlyByID(int ownerId, int target)
        {
            var order = _context.Orders
            .Where(
                order => order.AccountId == ownerId && order.CartId == target)
            .FirstOrDefault();
            return Task.FromResult(order);
        }

        public async Task<Order> GetDetailExactlyByID(int ownerId, int target)
        {
            var order = await _context.Orders
                .OrderByDescending(
                    order => order.CartId)
                .Where(
                    order => order.AccountId == ownerId && order.CartId == target)
                .Include(order => order.Status)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.CartDetails)
                        .ThenInclude(detail => detail.Product)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.Account)
                .Select(
                    o => new Order
                    {
                        CartId = o.CartId,
                        AccountId = o.AccountId,
                        StatusId = o.StatusId,
                        Phone = o.Phone,
                        ReceiverFirstName = o.ReceiverFirstName,
                        ReceiverLastName = o.ReceiverLastName,
                        PaymentMethod = o.PaymentMethod,
                        BankName = o.BankName,
                        ShipFee = o.ShipFee,
                        Note = o.Note,
                        ShipAddress = o.ShipAddress,
                        ShipWard = o.ShipWard,
                        ShipDistrict = o.ShipDistrict,
                        CreatedAt = o.CreatedAt,
                        UpdatedAt = o.UpdatedAt,
                        Cart = new Cart
                        {
                            CartId = o.CartId,
                            AccountId = o.AccountId,
                            Account = new Account
                            {
                                AccountId = o.Cart.Account.AccountId,
                                FirstName = o.Cart.Account.FirstName,
                                LastName = o.Cart.Account.LastName,
                                Email = o.Cart.Account.Email,
                                Membership = o.Cart.Account.Membership,
                                Avatar = o.Cart.Account.Avatar,
                                UnlockedMemberAt = o.Cart.Account.UnlockedMemberAt,
                                PermissionId = o.Cart.Account.PermissionId
                            },
                            CartDetails = o.Cart.CartDetails.Select(
                                cart => new CartDetail
                                {
                                    CartId = cart.CartId,
                                    AccountId = cart.AccountId,
                                    UnitPrice = cart.UnitPrice,
                                    DiscountValue = cart.DiscountValue,
                                    ProductId = cart.ProductId,
                                    CreatedAt = cart.CreatedAt,
                                    UpdatedAt = cart.UpdatedAt,
                                    Quantity = cart.Quantity,
                                    Product = new Product
                                    {
                                        ProductId = cart.Product.ProductId,
                                        Size = cart.Product.Size,
                                        Image = cart.Product.Image,
                                        EventId = cart.Product.EventId
                                    }
                                }
                            ).ToList()
                        },
                        Status = o.Status
                    }
                )
            .FirstOrDefaultAsync();
            return order;
        }

        public Task<IEnumerable<Order>> GetListByID(int ownerId)
        {
            IEnumerable<Order> orders = _context.Orders
                .OrderByDescending(
                    order => order.CartId)
                .Where(
                    order => order.AccountId == ownerId)
                .Include(order => order.Status)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.CartDetails)
                        .ThenInclude(detail => detail.Product)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.Account)
                .Select(
                    o => new Order
                    {
                        CartId = o.CartId,
                        AccountId = o.AccountId,
                        StatusId = o.StatusId,
                        Phone = o.Phone,
                        ReceiverFirstName = o.ReceiverFirstName,
                        ReceiverLastName = o.ReceiverLastName,
                        PaymentMethod = o.PaymentMethod,
                        BankName = o.BankName,
                        ShipFee = o.ShipFee,
                        Note = o.Note,
                        ShipAddress = o.ShipAddress,
                        ShipWard = o.ShipWard,
                        ShipDistrict = o.ShipDistrict,
                        CreatedAt = o.CreatedAt,
                        UpdatedAt = o.UpdatedAt,
                        Cart = new Cart
                        {
                            CartId = o.CartId,
                            AccountId = o.AccountId,
                            Account = new Account
                            {
                                AccountId = o.Cart.Account.AccountId,
                                FirstName = o.Cart.Account.FirstName,
                                LastName = o.Cart.Account.LastName,
                                Email = o.Cart.Account.Email,
                                Membership = o.Cart.Account.Membership,
                                Avatar = o.Cart.Account.Avatar,
                                UnlockedMemberAt = o.Cart.Account.UnlockedMemberAt,
                                PermissionId = o.Cart.Account.PermissionId
                            },
                            CartDetails = o.Cart.CartDetails.Select(
                                cart => new CartDetail
                                {
                                    CartId = cart.CartId,
                                    AccountId = cart.AccountId,
                                    UnitPrice = cart.UnitPrice,
                                    DiscountValue = cart.DiscountValue,
                                    ProductId = cart.ProductId,
                                    CreatedAt = cart.CreatedAt,
                                    UpdatedAt = cart.UpdatedAt,
                                    Quantity = cart.Quantity,
                                    Product = new Product
                                    {
                                        ProductId = cart.Product.ProductId,
                                        ProductName = cart.Product.ProductName,
                                        UnitPrice = cart.Product.UnitPrice,
                                        SizeId = cart.Product.SizeId,
                                        Size = cart.Product.Size,
                                        Image = cart.Product.Image,
                                        EventId = cart.Product.EventId
                                    }
                                }
                            ).ToList()
                        },
                        Status = o.Status
                    }
                )
                .ToList();
            return Task.FromResult(orders);
        }
        public Task<Order> GetLastOrder(int ownerId)
        {
            var lastOrder = _context.Orders
                .Where(
                    order => order.AccountId == ownerId)
                .OrderByDescending(
                    order => order.CartId)
                .FirstOrDefault();
            // if(lastOrder == null) throw new Exception("There is no order");
            return Task.FromResult(lastOrder);
        }
        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public void UpdateOld(Order target)
        {

        }
        public async Task<bool> UnlockMembership(int ownerId)
        {
            const double unlockMemberMilestone = 3000;

            IEnumerable<Order> orders = await GetListByID(ownerId);
            List<CartReadDTO> carts = new List<CartReadDTO>();
            foreach (var item in orders)
            {
                if (item.Cart.Account.Membership != 0)
                {
                    return false;
                }
                if (item.StatusId == 4)
                {
                    CartReadDTO tempDto = new CartReadDTO()
                    {
                        CartDetails = item.Cart.CartDetails,
                        Account = item.Cart.Account
                    };
                    // System.Console.WriteLine("Cart ID: " + tempDto.CartId);
                    // System.Console.WriteLine("Total Price: " + tempDto.TotalPrice);
                    carts.Add(tempDto);
                }
            }
            double totalCashUserPaid = 0;
            foreach (var item in carts)
            {
                // System.Console.WriteLine("Total Price (in loop totalPaid): " + item.TotalPrice);
                totalCashUserPaid += item.TotalPrice;
            }
            // System.Console.WriteLine("totalCashUserPaid: " + totalCashUserPaid);
            if (totalCashUserPaid >= unlockMemberMilestone)
            {
                var user = _context.Accounts.Where(acc => acc.AccountId == ownerId).FirstOrDefault();
                user.Membership = 0.05;
                await SaveChanges();
            }
            return true;
        }
        private async Task<OrderStatus> IsStatusAble(string status)
        {
            status = status.Trim();
            if (status.Contains("+")) status = status.Replace("+", " ");
            if (status.Equals(null))
            {
                throw new Exception("status name is not null");
            }
            Regex regex = new Regex(@"^\d$");
            if (regex.IsMatch(status))
            {
                throw new Exception("status name must be alphabet characters");
            }
            foreach (var item in _context.OrderStatuses.ToList())
            {
                // System.Console.WriteLine("item name to lower: " + item.StatusName.ToLower());
                if (item.StatusName.ToLower().Equals(status.ToLower()))
                {
                    System.Console.WriteLine("Found");
                    return item;
                }
            }
            var statusEntity = await _context.OrderStatuses
            .Where(s => s.StatusName.ToLower().Equals(status.ToLower())).FirstOrDefaultAsync();

            if (statusEntity is null)
            {
                throw new Exception("status name is not existed");
            }
            return statusEntity;
        }
        public async Task<IEnumerable<Order>> GetOrdersOfSpecifyUserByStatus(int ownerId, string status)
        {
            var target = await IsStatusAble(status);
            var orders = await GetListByID(ownerId);
            orders = orders.Where(order => order.StatusId == target.StatusId).ToList();

            return orders;
        }
        public async Task<IEnumerable<Order>> GetOrdersByStatus(string status)
        {
            var target = await IsStatusAble(status);
            var orders = _context.Orders
                .Where(
                    order => order.StatusId == target.StatusId)
                .Include(order => order.Status)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.CartDetails)
                .Include(order => order.Cart)
                    .ThenInclude(cart => cart.Account)
                .ToList();
            return orders;
        }

    }
}