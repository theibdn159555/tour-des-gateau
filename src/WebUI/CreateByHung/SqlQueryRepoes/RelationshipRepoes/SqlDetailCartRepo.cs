using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.CartDetailDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlDetailCartRepo : IRelationshipRepo<CartDetail>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlDetailCartRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(CartDetail target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(CartDetail));
            }
            int expired = await SqlViolationRepo.IsViolationExpired(target.AccountId);
            if(expired < 0)
            {
                throw new Exception("This account is banned");
            }
            // System.Console.WriteLine("accountId = " + target.AccountId);
            if(_context.Carts.Any(c => c.AccountId == target.AccountId) is false)
            {
                // System.Console.WriteLine("user existed");
                // var user = _context.Accounts.Where(a => a.AccountId == target.AccountId).FirstOrDefault();
                await _context.Carts.AddAsync(new Cart()
                {
                    AccountId = target.AccountId,
                    CartId = 1
                });
                await SaveChanges();
            }

            var openCart = GetOpenCart(target.AccountId);
            target.CartId = openCart.CartId;
            // System.Console.WriteLine("cartId = " + target.CartId);

            if(_context.CartDetails.Any(c => c.Equals(target)) is true)
            {
                // throw new ObjectExistedException("The item has been added to cart");
                var existItem = _context.CartDetails
                .Where(
                    item => item.CartId == target.CartId 
                    && item.ProductId == target.ProductId 
                    && item.AccountId == target.AccountId).FirstOrDefault();
                existItem.Quantity += target.Quantity;
                await SaveChanges();
                return existItem.CartId;
            }
            if(_context.Products.Select(p => p.ProductId).Any(p => p == target.ProductId) is false)
            {
                throw new ObjectNotFoundException("The item is not exist");
            }
            var product = _context.Products.Where(p => p.ProductId == target.ProductId).Include(e => e.Event).Select(
                p => new Product{
                    ProductId = p.ProductId,
                    ProductName = p.ProductName,
                    UnitPrice = p.UnitPrice,
                    Event = p.Event,
                    EventId = p.EventId
                }
            ).FirstOrDefault();

            target.UnitPrice = product.UnitPrice;
            target.DiscountValue = (double)product.Event.DiscountValue;

            await _context.CartDetails.AddAsync(target);
            await SaveChanges();

            return target.CartId;
        }

        public async void DeleteOld(CartDetail target)
        {
            if(target == null) throw new ArgumentNullException(nameof(CartDetail));

            if(_context.CartDetails.Any(c => c.Equals(target)) is false)
            {
                throw new ObjectNotFoundException("The item is not existed in cart");
            }
            _context.CartDetails.Remove(target);
            await SaveChanges();
        }
        public void DeleteOld(int userId, int productId)
        {
            var openCart = GetOpenCart(userId);
            
            var model = _context.CartDetails.Where(
                c => c.AccountId == userId 
                && c.ProductId == productId 
                && c.CartId == openCart.CartId
            ).FirstOrDefault();

            _context.CartDetails.Remove(model);
        }
        public void DeleteAll(int userId)
        {
            var openCart = GetOpenCart(userId);

            var models = _context.CartDetails.Where(
                c => c.AccountId == userId
                && c.CartId == openCart.CartId
            ).ToList();

            _context.CartDetails.RemoveRange(models);
        }
        public void DeleteList(int userId, IEnumerable<CartDetail> models)
        {
            var openCart = GetOpenCart(userId);
            foreach (var item in models)
            {
                if(item.CartId == 0)
                {
                    item.CartId = openCart.CartId;
                    if(!_context.CartDetails
                        .Any(
                            d => d.CartId == item.CartId 
                            && d.AccountId == item.AccountId 
                            && d.ProductId == item.ProductId
                    ))
                    {
                        throw new Exception("Can not delete because some items are not existed in your cart");
                    }
                }
            }
            _context.CartDetails.RemoveRange(models);
        }
        public Cart GetOpenCart(int userId)
        {
            return _context.Carts.Where(c => c.AccountId == userId && (bool)c.IsOpen).FirstOrDefault();
        }
        public async Task<IEnumerable<CartDetail>> GetByID(int id)
        {
            //id == UserID
            int expired = await SqlViolationRepo.IsViolationExpired(id);
            if(expired < 0)
            {
                throw new Exception("This account is banned");
            }
            var cart = GetOpenCart(id);
            IEnumerable<CartDetail> details = _context.CartDetails.Where(d => d.AccountId == cart.AccountId && d.CartId == cart.CartId).ToList();
            return details;
        }

        public Task<IEnumerable<CartDetail>> Filter(int id, bool? orderByCreatedAt, int? year, int? month, int? day, DateTime? from, DateTime? to)
        {
            throw new NotImplementedException();
        }

        public Task<bool> FindExist(CartDetail target)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CartDetail>> GetAll(int? limit)
        {
            throw new NotImplementedException();
        }


        public Task<CartDetail> GetByID(int firstId, int secondId)
        {
            throw new NotImplementedException();
        }

        public Task<CartDetail> GetByID(int id, ICollection<int> groupIds)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsAbleToInsert(CartDetail target)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsAvailable(int id, EntityEnum entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateOld(CartDetail target)
        {
            // _context.CartDetails.Update(target);            
        }
        public void UpdateOldRange(IEnumerable<CartDetail> targets)
        {
            _context.CartDetails.UpdateRange(targets);
        }

        public Task<PaginationRepo<CartDetail>> GetPagination(int id, PaginationModel paging)
        {
            throw new NotImplementedException();
        }
    }
}