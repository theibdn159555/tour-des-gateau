using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlNotificationRepo : INotificationRepo<Notification>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlNotificationRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<bool> CreateNew(int userId)
        {
            var time = DateTime.Now;
            // System.Console.WriteLine(time);
            IEnumerable<DiscountEvent> events = _context.DiscountEvents
                .Where(
                    e => e.DateStart.Value.Year == time.Year
                    && e.DateStart.Value.Month == time.Month)
                .ToList();

            string eventName = "";
            bool flag = false;

            double totalDaysLeft = 0;

            foreach (var item in events)
            {
                totalDaysLeft = ((item.DateStart.Value) - time).TotalDays;
                if (totalDaysLeft > 0 && totalDaysLeft <= 7)
                {
                    eventName = item.EventName;
                    flag = true;
                    break;
                }
            }
            if (!flag) return false;

            totalDaysLeft = Math.Floor(totalDaysLeft);

            string templateContent = _context.NotificationTemplates
                .Where(
                    t => t.TemplateId == 8)
                .FirstOrDefault()
                .ContentTemplate;
            Notification eventNotify = new Notification()
            {
                NotificationTemplateId = 8,
                AccountId = userId,
                Content = eventName + " " + templateContent + " " + totalDaysLeft.ToString()
            };
            await _context.Notifications.AddAsync(eventNotify);
            await SaveChanges();
            // await NotSave();
            return true;
        }

        public void DeleteOld(Notification target)
        {
            throw new NotImplementedException();
        }

        public Task<bool> FindExist(Notification target)
        {
            throw new NotImplementedException();
        }

        public Task<Notification> FindExist(int target)
        {
            var model = _context.Notifications.Where(n => n.NotificationId == target).FirstOrDefault();
            return Task.FromResult(model);
        }

        public Task<IEnumerable<Notification>> GetByID(int id)
        {
            IEnumerable<Notification> models = _context.Notifications
            .OrderByDescending(n => n.CreatedAt)
                .Where(
                    n => n.AccountId == id)
                .Include(
                    t => t.NotificationTemplate)
                    .ThenInclude(
                        type => type.Type)
                .ToList();
            return Task.FromResult(models);
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public Task<IEnumerable<Notification>> Search(string name)
        {
            throw new NotImplementedException();
        }

        public void UpdateOld(Notification target)
        {

        }
        private Task<bool> NotSave() => Task.FromResult(true);
    }
}