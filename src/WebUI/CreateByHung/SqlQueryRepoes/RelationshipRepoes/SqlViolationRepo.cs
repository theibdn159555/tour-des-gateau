using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlViolationRepo : IViolationRepo
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlViolationRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(Violation target)
        {
            bool isExisted = await FindExist(target);
            if(isExisted == true)
            {
                throw new Exception($"This one: {target.WarningId} is given for {target.AccountId}!");
            }
            if(target.Reason == null || target.Reason == "")
            {
                throw new Exception("Missing reason field!");
            }
            
            await _context.Violations.AddAsync(target);
            await SaveChanges();
            return target.WarningId;
        }
        private async Task<Violation> GetNearestViolationOfUser(int userId)
        {
            return await _context.Violations.Where(v => v.AccountId == userId).OrderByDescending(v => v.UpdatedAt).FirstOrDefaultAsync();
        }

        public async static Task<int> IsViolationExpired(int userId)
        {
            db_a7c7da_phunghung123Context context = new db_a7c7da_phunghung123Context();
            var nearestWarnLevel = await context.Violations.Where(v => v.AccountId == userId).OrderByDescending(v => v.UpdatedAt).FirstOrDefaultAsync();
            int expired = 0;
            if(nearestWarnLevel == null)
            {
                return expired;
            }
            switch (nearestWarnLevel.WarningId)
            {
                case 2:
                    expired = 100;
                    break;
                case 3:
                    expired = -1;
                    break;
                default:
                    break;
            }
            if(expired != 100)
            {
                return expired;
            }
            var boughtOrders = await context.Orders
            .Where(
                o => o.AccountId == userId 
                && o.StatusId == 4 
                && ((DateTime)o.UpdatedAt)
                    .CompareTo(
                        (DateTime)nearestWarnLevel.UpdatedAt) == 1)
            .CountAsync();
            int timeLeft = expired - boughtOrders;
            if(timeLeft < 0)
                expired = 0;
            return expired;
        }

        public void DeleteOld(Violation target)
        {
            // if(target == null) throw new ArgumentNullException(nameof(CartDetail));

            // if(await FindExist(target) is false)
            // {
            //     throw new Exception("The warrant is not existed in db");
            // }
            var model = _context.Violations.Where(v => v.AccountId == target.AccountId && v.WarningId == target.WarningId).FirstOrDefault();
            _context.Violations.Remove(model);
            // await SaveChanges();
            _context.SaveChanges();
        }

        // public async Task<IEnumerable<Violation>> Filter(
        //     int? warningId, 
        //     bool? orderByDate, 
        //     int? year, int? month, int? day, 
        //     DateTime? from, DateTime? to
        // )
        // {
        //     int takeRow = limit == null ? 6 : (int)limit;
        //     int gotoPage = page == null ? 1 : (int)page;
        //     List<Violation> results = new List<Violation>();
        //     bool searching = true;
        //     int curPage = 1;
        //     while (searching)
        //     {                
        //         var list = await GetAll(limit, curPage);//6, page = 1
        //         // list = _context.Violations.AsQueryable().Where(v => (!warningId.HasValue || v.WarningId == warningId.Value) && ())
        //         list = _context.Violations;
        //         if(list.Count() == 0)
        //         {
        //             System.Console.WriteLine("No data");
        //             break;
        //         }
        //         if(warningId != null)
        //         {
        //             list = list.Where(v => v.WarningId == (int)warningId);
        //         }
        //         if(orderByDate != null)
        //         {
        //             if((bool)orderByDate)
        //             {
        //                 list = list.OrderBy(v => v.UpdatedAt);
        //             }
        //             else
        //             {
        //                 list = list.OrderByDescending(v => v.UpdatedAt);
        //             }
        //         }
        //         if(from != null && to != null)
        //         {
        //             list = list.Where(item => item.CreatedAt >= from && item.CreatedAt <= to);
        //             results.AddRange(list);
        //             curPage++;
        //             continue;
        //         }
        //         if(year != null)
        //         {
        //             list = list.Where(item => ((DateTime)item.UpdatedAt).Year == (int)year);
        //             if(month != null)
        //             {
        //                 list = list.Where(item => ((DateTime)item.UpdatedAt).Month == (int)month);
        //                 if(day != null)
        //                 {
        //                     list = list.Where(item => ((DateTime)item.UpdatedAt).Day == (int)day);
        //                 }
        //             }
        //         }
        //         list = list.ToList();
        //         results.AddRange(list);
        //         curPage ++;
        //     }
        //     results = results.Skip((gotoPage - 1) * takeRow).Take(takeRow).ToList();
        //     return results;
        // }

        public async Task<IEnumerable<Violation>> Filter(
            int? warningId, 
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to
        )
        {
            var results = new List<Violation>();

            var list = _context.Violations.AsQueryable();
            if(list.Count() == 0 || list == null)
            {
                return null;
            }

            if(orderByDate != null)
                {
                    if((bool)orderByDate)
                    {
                        list = list.OrderBy(v => v.UpdatedAt);
                    }
                    else
                    {
                        list = list.OrderByDescending(v => v.UpdatedAt);
                    }
                }

            list = list.Where(v => 
                (!warningId.HasValue || v.WarningId == warningId.Value) &&
                (
                    (!year.HasValue && !month.HasValue && !day.HasValue) || 
                    (
                        v.UpdatedAt.Value.Year == year.Value && 
                        v.UpdatedAt.Value.Month == month.Value && 
                        v.UpdatedAt.Value.Day == day.Value
                    )
                )
            );
            list = list.Where(item => 
                (!from.HasValue && !to.HasValue) || 
                (item.UpdatedAt.Value >= from.Value && item.UpdatedAt.Value <= to.Value)
            );
            results = await list.Select(
                v => new Violation {
                    WarningId = v.WarningId,
                    AccountId = v.AccountId,
                    Account = new Account{
                        Username = v.Account.Username,
                        Email = v.Account.Email,
                        Phone = v.Account.Phone,
                    },
                    Reason = v.Reason,
                    CreatedAt = v.CreatedAt,
                    UpdatedAt = v.UpdatedAt
                }
            ).ToListAsync();
            return results;            
        }

        public async Task<bool> FindExist(Violation target)
        {
            var rowExisted = await _context.Violations.AnyAsync(v => v.AccountId == target.AccountId && v.WarningId == target.WarningId);
            return rowExisted;
        }

        // public Task<IEnumerable<ViolationReadDTO>> GetAll(int? limit, int page)
        // {
        //     var takeRow = limit == null ? 6 : (int)limit;
        //     var list = _context.Violations
        //     .Skip((page - 1) * takeRow)
        //     .Take(takeRow)
        //     .Include(w => w.Warning).Include(a => a.Account)
        //     .Select(
        //         v => new {
        //             WarningId = v.WarningId,
        //             AccountId = v.AccountId,
        //             Username = v.Account.Username,
        //             Email = v.Account.Email,
        //             Phone = v.Account.Phone,
        //             Reason = v.Reason,
        //             CreatedAt = v.CreatedAt,
        //             UpdatedAt = v.UpdatedAt
        //         }
        //     )
        //     .ToList();
        //     var mappedList = new List<Violation>();
        //     foreach (var item in list)
        //     {
        //         var temp = new Violation()
        //         {
                    
        //         };
        //     }
        //     return Task.FromResult(list);
        // }
        public async Task<IEnumerable<Violation>> GetAll()
        {
            var list = await _context.Violations
            .Include(w => w.Warning).Include(a => a.Account)
            .Select(
                v => new Violation{
                    WarningId = v.WarningId,
                    AccountId = v.AccountId,
                    Account = new Account{
                        Username = v.Account.Username,
                        Email = v.Account.Email,
                        Phone = v.Account.Phone,
                    },
                    Reason = v.Reason,
                    CreatedAt = v.CreatedAt,
                    UpdatedAt = v.UpdatedAt
                }
            )
            .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<Violation>> GetByID(int userId)
        {
            IEnumerable<Violation> list = await _context.Violations
            .Where(a => a.AccountId == userId)
            .Include(w => w.Warning)
            .Include(a => a.Account)
            .Select(
                v => new Violation{
                    WarningId = v.WarningId,
                    AccountId = v.AccountId,
                    Account = new Account{
                        Username = v.Account.Username,
                        Email = v.Account.Email,
                        Phone = v.Account.Phone,
                    },
                    Reason = v.Reason,
                    CreatedAt = v.CreatedAt,
                    UpdatedAt = v.UpdatedAt
                }
            )
            .ToListAsync();
            return list;
        }

        public async Task<Violation> GetByID(int userId, int warningId)
        {
            var violation = await _context.Violations
            .Where(v => v.AccountId == userId && v.WarningId == warningId)
            .Include(w => w.Warning)
            .Include(a => a.Account)
            .Select(
                v => new Violation{
                    WarningId = v.WarningId,
                    AccountId = v.AccountId,
                    Account = new Account{
                        Username = v.Account.Username,
                        Email = v.Account.Email,
                        Phone = v.Account.Phone,
                    },
                    Reason = v.Reason,
                    CreatedAt = v.CreatedAt,
                    UpdatedAt = v.UpdatedAt
                }
            )
            .FirstOrDefaultAsync();
            return violation;
        }

        public async Task<bool> IsUserExist(int userId)
        {
            var uesrExisted = await _context.Accounts.AnyAsync(a => a.AccountId == userId);
            return uesrExisted;
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateOld(Violation target)
        {
            
        }
    }
}