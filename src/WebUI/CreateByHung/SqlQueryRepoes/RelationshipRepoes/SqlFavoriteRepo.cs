using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using WebUI.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Paginations;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlFavoriteRepo : IRelationshipRepo<Favorite>
    {
        private readonly db_a7c7da_phunghung123Context _context;
        public SqlFavoriteRepo(db_a7c7da_phunghung123Context context)
        {
            this._context = context;
        }
        public async Task<int> CreateNew(Favorite target)
        {            
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Favorite));
            }

            int flag = 0;
            if(await IsAvailable(target.AccountId, EntityEnum.Account))
            {
                if(await IsAvailable(target.ProductId, EntityEnum.Product))
                {
                    flag = 1;
                }
            }

            if(flag == 0)
            {
                throw new ObjectNotFoundException("Account or Product is not found");
            }

            if(await FindExist(target))
            {
                throw new ObjectExistedException("Record is Existed");
            }
            int expired = await SqlViolationRepo.IsViolationExpired(target.AccountId);
            if(expired < 0)
            {
                throw new Exception("This account is banned");
            }

            await _context.Favorites.AddAsync(target);

            bool save = await SaveChanges();            
            return target.AccountId;
        }

        public void DeleteOld(Favorite target)
        {
            // System.Console.WriteLine("In DeleteRepo: " + target.AccountId + " & " + target.ProductId);
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Favorite));
            }
            _context.Favorites.Remove(target);
        }

        public async Task<IEnumerable<Favorite>> Filter(
            int id,
            bool? orderByCreatedAt, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to
        )
        {
            IEnumerable<Favorite> lists = await _context.Favorites
            .Where(item => item.AccountId == id)
            .Include(item => item.Account)
            .Include(item => item.Product).ToListAsync();
            
            if(orderByCreatedAt != null)
            {
                if((bool)orderByCreatedAt == true)
                    lists = lists.OrderBy(item => item.CreatedAt);
                else
                    lists = lists.OrderByDescending(item => item.CreatedAt);
            }

            if(from != null && to != null)
            {
                lists = lists.Where(item => item.CreatedAt >= from && item.CreatedAt <= to);
                return lists;
            }

            if(year != null)
            {
                lists = lists.Where(item => ((DateTime)item.CreatedAt).Year == (int)year);
                if(month != null)
                {
                    lists = lists.Where(item => ((DateTime)item.CreatedAt).Month == (int)month);
                    if(day != null)
                    {
                        lists = lists.Where(item => ((DateTime)item.CreatedAt).Day == (int)day);
                    }
                }
            }

            return lists;
        }

        public Task<bool> FindExist(Favorite target)
        {
            return _context.Favorites.AnyAsync(item => item.AccountId == target.AccountId && item.ProductId == target.ProductId);
        }

        public Task<IEnumerable<Favorite>> GetAll(int? limit)
        {
            IEnumerable<Favorite> lists = _context.Favorites
                                            .OrderBy(user => user.AccountId).ToList();
            return Task.FromResult(lists);
        }

        public Task<IEnumerable<Favorite>> GetByID(int id)
        {
            IEnumerable<Favorite> lists = _context.Favorites
                                            .OrderByDescending(item => item.CreatedAt)
                                            .Include(item => item.Product)
                                            .Where(item => item.AccountId == id)
                                            .Select(
                                                f => new Favorite{
                                                    AccountId = f.AccountId,
                                                    ProductId = f.ProductId,
                                                    Product = new Product{
                                                        ProductId = f.Product.ProductId,
                                                        ProductName = f.Product.ProductName,
                                                        Image = f.Product.Image,
                                                        UnitPrice = f.Product.UnitPrice,
                                                        Description = f.Product.Description
                                                    }
                                                }
                                            )
                                            .ToList();
            return Task.FromResult(lists);
        }

        public Task<Favorite> GetByID(int firstId, int secondId)
        {
            Favorite item = _context.Favorites
                                .Where(item => item.AccountId == firstId && item.ProductId == secondId).FirstOrDefault();
            return Task.FromResult(item);
        }

        public Task<Favorite> GetByID(int id, ICollection<int> groupIds)
        {
            throw new NotSupportedException("This table is not supported by this method");
        }

        public Task<PaginationRepo<Favorite>> GetPagination(int id, PaginationModel paging)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsAbleToInsert(Favorite target)
        {
            // throw new NotSupportedException("This table is not supported by this method");
            return Task.FromResult(true);
        }

        public Task<bool> IsAvailable(int id, EntityEnum entity)
        {
            switch (entity)
            {
                case EntityEnum.Account:
                    return _context.Accounts.AnyAsync(item => item.AccountId == id);
                case EntityEnum.Product:
                    return _context.Products.AnyAsync(item => item.ProductId == id);
                default:
                    return Task.FromResult(false);
            }
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateOld(Favorite target)
        {
            
        }
    }
}