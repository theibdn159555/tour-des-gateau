using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using WebUI.Data;
using System.Linq;
using WebUI.CreateByHung.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlIngredientRepo : IRelationshipRepo<Ingredient>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlIngredientRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<int> CreateNew(Ingredient target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Category));
            }

            int flag = 0;

            if(await IsAvailable(target.MaterialId, EntityEnum.Material))
            {
                if(await IsAvailable(target.ProductId, EntityEnum.Product))
                {
                    flag = 1;
                }
            }

            if(flag == 0)
            {
                throw new ObjectNotFoundException("Account or Product is not found");
            }

            if(await FindExist(target))
            {
                throw new ObjectExistedException("Record is Existed");
            }

            if(await IsAbleToInsert(target) is false)
            {
                throw new Exception("Quantity of Ingredient is too great!");
            }

            await _context.Ingredients.AddAsync(target);

            bool save = await SaveChanges();            
            return target.ProductId;
        }

        public void DeleteOld(Ingredient target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Ingredient));
            }             
            _context.Ingredients.Remove(target);
        }

        public async Task<IEnumerable<Ingredient>> Filter(
            int id, 
            bool? orderByCreatedAt, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to
        )
        {
            IEnumerable<Ingredient> lists = await _context.Ingredients
            .Where(item => item.ProductId == id)
                                                    .Include(item => item.Material)
                                                    .Include(item => item.Product).ToListAsync();
            if(orderByCreatedAt != null)
                if(orderByCreatedAt is true)
                    lists = lists.OrderBy(item => item.CreatedAt);
                else
                    lists = lists.OrderByDescending(item => item.CreatedAt);

            if(from != null && to != null)
            {
                lists = lists.Where(item => item.CreatedAt >= from && item.CreatedAt <= to);
                return lists;
            }
            if(year != null)
            {
                lists = lists.Where(item => ((DateTime)item.CreatedAt).Year == year);
                if(month != null)
                {
                    lists = lists.Where(item => ((DateTime)item.CreatedAt).Month == month);
                    if(day != null)
                    {
                        lists = lists.Where(item => ((DateTime)item.CreatedAt).Day == day);
                    }
                }
            }

            return lists;
        }

        public async Task<bool> FindExist(Ingredient target)
        {
            return await _context.Ingredients.AnyAsync(item => item.Equals(target));
        }

        public Task<IEnumerable<Ingredient>> GetAll(int? limit)
        {
            IEnumerable<Ingredient> ingredients = _context.Ingredients.OrderBy(item => item.ProductId).ToList();
            return Task.FromResult(ingredients);
        }

        public Task<IEnumerable<Ingredient>> GetByID(int id)
        {
            IEnumerable<Ingredient> ingredients = _context.Ingredients
                                                    .OrderBy(item => item.ProductId)
                                                    .Where(item => item.ProductId == id)
                                                    .ToList();
            return Task.FromResult(ingredients);
        }

        public Task<Ingredient> GetByID(int firstId, int secondId)
        {
            Ingredient ingredient = _context.Ingredients.OrderBy(item => item.ProductId)
                                        .Where(item => item.ProductId == firstId && item.MaterialId == secondId)
                                        .Include(m => m.Material)
                                        .Include(p => p.Product).FirstOrDefault();
            return Task.FromResult(ingredient);
        }

        public Task<Ingredient> GetByID(int id, ICollection<int> groupIds)
        {
            throw new NotSupportException("This table is not supported by this method");
        }

        public Task<PaginationRepo<Ingredient>> GetPagination(int id, PaginationModel paging)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsAbleToInsert(Ingredient target)
        {
            if(target.Quantity > 1)
            {
                return false;
            }
            var sameProductList = await GetByID(target.ProductId);
            double existQuantity = 0;
            foreach (var item in sameProductList)
            {
                existQuantity += (double) item.Quantity;
            }
            if(existQuantity == 1.0)
            {
                return false;
            }
            if((1 - existQuantity) < target.Quantity)
            {
                return false;
            }
            return true;
        }

        public Task<bool> IsAvailable(int id, EntityEnum entity)
        {
            switch (entity)
            {
                case EntityEnum.Material:
                    return _context.Materials.AnyAsync(item => item.MaterialId == id);
                case EntityEnum.Product:
                    return _context.Products.AnyAsync(item => item.ProductId == id);
                default:
                    return Task.FromResult(false);
            }
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateOld(Ingredient target)
        {
            
        }
    }
}