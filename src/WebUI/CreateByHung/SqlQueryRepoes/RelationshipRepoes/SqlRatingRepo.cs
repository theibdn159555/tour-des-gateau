using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.DTOs.RatingDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes
{
    public class SqlRatingRepo : IRelationshipRepo<Rating>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlRatingRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(Rating target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Rating));
            }

            int flag = 0;
            if (await IsAvailable(target.UserId, EntityEnum.Account))
            {
                if (await IsAvailable(target.ProductId, EntityEnum.Product))
                {
                    flag = 1;
                }
            }

            if (flag == 0)
            {
                throw new ObjectNotFoundException("Account or Product is not found");
            }

            if (await FindExist(target))
            {
                throw new ObjectExistedException("Record is Existed");
            }

            if (await IsAbleToInsert(target) is false)
            {
                throw new Exception("The Rating Star is not support or You have not bought product yet");
            }
            int expired = await SqlViolationRepo.IsViolationExpired(target.UserId);
            if (expired < 0)
            {
                throw new Exception("This account is banned");
            }

            await _context.Ratings.AddAsync(target);

            bool save = await SaveChanges();
            return target.ProductId;
        }

        public void DeleteOld(Rating target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Rating));
            }
            _context.Ratings.Remove(target);
        }

        public async Task<IEnumerable<Rating>> Filter(int id, bool? orderByUpdateAt, int? year, int? month, int? day, DateTime? from, DateTime? to)
        {
            IEnumerable<Rating> lists = await _context.Ratings
            .Where(item => item.ProductId == id)
            .Include(item => item.User)
            .ToListAsync();

            if (orderByUpdateAt != null)
                lists = lists.OrderBy(item => item.UpdatedAt);
            else
                lists = lists.OrderByDescending(item => item.UpdatedAt);

            if (from != null && to != null)
            {
                lists = lists.Where(item => item.UpdatedAt >= from && item.UpdatedAt <= to);
                return lists;
            }

            if (year != null)
            {
                lists = lists.Where(item => ((DateTime)item.UpdatedAt).Year == year);
                if (month != null)
                {
                    lists = lists.Where(item => ((DateTime)item.UpdatedAt).Month == month);
                    if (day != null)
                    {
                        lists = lists.Where(item => ((DateTime)item.UpdatedAt).Day == day);
                    }
                }
            }

            return lists;
        }

        public Task<bool> FindExist(Rating target)
        {
            return _context.Ratings.AnyAsync(item => item.Equals(target));
        }

        public Task<IEnumerable<Rating>> GetAll(int? limit)
        {
            IEnumerable<Rating> lists = _context.Ratings.OrderBy(item => item.ProductId).ToList();
            return Task.FromResult(lists);
        }

        public Task<IEnumerable<Rating>> GetByID(int id)
        {
            IEnumerable<Rating> lists = _context.Ratings
                .OrderByDescending(item => item.UpdatedAt)
                .Include(item => item.User)
                .Where(item => item.ProductId == id).ToList();
            return Task.FromResult(lists);
        }
        public async Task<RatingSummaryDTO> GetRatingSummaryByID(int id)
        {
            var ratings = await GetByID(id);
            if (ratings == null)
            {
                return new RatingSummaryDTO()
                {
                    ProductId = id,
                    FiveStar = 0,
                    FourStar = 0,
                    ThreeStar = 0,
                    TwoStar = 0,
                    OneStar = 0,
                    AverageStar = 0
                };
            }
            var product = await _context.Products
            .Where(p => p.ProductId == id)
            .Select(
                p => new Product
                {
                    ProductId = p.ProductId,
                    ProductName = p.ProductName,
                    Image = p.Image,
                    Description = p.Description,
                    UnitPrice = p.UnitPrice,
                    CategoryId = p.CategoryId,
                    SizeId = p.SizeId,
                    EventId = p.EventId,
                    TagId = p.TagId
                }
            ).FirstOrDefaultAsync();

            int five = 0,
            four = 0,
            three = 0,
            two = 0,
            one = 0;

            double average = 0.0;

            foreach (var item in ratings)
            {
                switch (item.StarRating)
                {
                    case 5:
                        five++;
                        break;
                    case 4:
                        four++;
                        break;
                    case 3:
                        three++;
                        break;
                    case 2:
                        two++;
                        break;
                    default:
                        one++;
                        break;
                }
                average += item.StarRating;
            }

            average /= ratings.Count();

            RatingSummaryDTO ratingSummary = new RatingSummaryDTO()
            {
                ProductId = id,
                Product = product,
                FiveStar = five,
                FourStar = four,
                ThreeStar = three,
                TwoStar = two,
                OneStar = one,
                AverageStar = average

            };

            return ratingSummary;
        }

        public Task<Rating> GetByID(int firstId, int secondId)
        {
            Rating rating = _context.Ratings.Where(item => item.UserId == firstId && item.ProductId == secondId).FirstOrDefault();
            return Task.FromResult(rating);
        }

        public Task<Rating> GetByID(int id, ICollection<int> groupIds)
        {
            throw new NotSupportedException("This table is not supported by this method");
        }

        public Task<bool> IsAbleToInsert(Rating target)
        {
            if (target.StarRating > 5.0 || target.StarRating <= 0)
            {
                return Task.FromResult(false);
            }
            var completedOrders = _context.Orders
                .Where(
                    order => order.AccountId == target.UserId && order.StatusId == 4)
                .Include(c => c.Cart)
                    .ThenInclude(acc => acc.CartDetails)
                .ToList();

            // var carts = _context.Carts
            //     .Where(
            //         c => c.AccountId == target.UserId && !(bool)c.IsOpen)
            //     .Include(
            //         c => c.CartDetails);

            //Binary Tree Search
            foreach (var item in completedOrders)
            {
                item.Cart.CartDetails = item.Cart.CartDetails.OrderBy(p => p.ProductId).ToList();
                int min = item.Cart.CartDetails.Min(dc => dc.ProductId),
                    max = item.Cart.CartDetails.Max(dc => dc.ProductId);
                double mid = item.Cart.CartDetails.Average(dc => dc.ProductId);
                int midPos = item.Cart.CartDetails.Count() / 2;

                if (target.ProductId == min || target.ProductId == max)
                {
                    // System.Console.WriteLine("At IsAbleToInsert of SqlRatingRepo said: This customer has bought this product already!");
                    return Task.FromResult(true);
                }
                else if (target.ProductId > min && target.ProductId < mid)
                {
                    for (int i = 1; i < midPos; i++)
                    {
                        if (item.Cart.CartDetails.ElementAt(i).ProductId == target.ProductId)
                        {
                            // System.Console.WriteLine("At IsAbleToInsert of SqlRatingRepo said: This customer has bought this product already!");
                            return Task.FromResult(true);
                        }
                    }
                }
                else if (target.ProductId >= mid && target.ProductId < max)
                {
                    for (int i = item.Cart.CartDetails.Count() - 1; i >= midPos; i--)
                    {
                        if (item.Cart.CartDetails.ElementAt(i).ProductId == target.ProductId)
                        {
                            // System.Console.WriteLine("At IsAbleToInsert of SqlRatingRepo said: This customer has bought this product already!");
                            return Task.FromResult(true);
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
            // System.Console.WritseLine("At IsAbleToInsert of SqlRatingRepo said: This customer has not bought this product yet!");
            return Task.FromResult(false);
        }

        public Task<bool> IsAvailable(int id, EntityEnum entity)
        {
            switch (entity)
            {
                case EntityEnum.Account:
                    return _context.Accounts.AnyAsync(item => item.AccountId == id);
                case EntityEnum.Product:
                    return _context.Products.Select(
                        p => new Product
                        {
                            ProductId = p.ProductId,
                            ProductName = p.ProductName
                        }
                    ).AnyAsync(item => item.ProductId == id);
                default:
                    return Task.FromResult(false);
            }
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public async void UpdateOld(Rating target)
        {
            int expired = await SqlViolationRepo.IsViolationExpired(target.UserId);
            if (expired < 0)
            {
                throw new Exception("This account is banned");
            }
        }

        public Task<PaginationRepo<Rating>> GetPagination(int id, PaginationModel paging)
        {
            // var ratings = _context.Ratings.OrderByDescending(r => r.CreatedAt).
            return null;
        }
        public Task<IEnumerable<Rating>> Filter(int id, bool orderByStar = true)
        {
            IEnumerable<Rating> lists = _context.Ratings
                .Include(item => item.User)
                .Where(item => item.ProductId == id).ToList();

            if (orderByStar is true) lists = lists.OrderByDescending(item => item.StarRating);
            else lists = lists.OrderByDescending(item => item.StarRating);
            return Task.FromResult(lists);
        }
        public Task<IEnumerable<Rating>> GetByStar(int? productId, bool? orderByDate, int star = 5)
        {
            IEnumerable<Rating> lists = _context.Ratings
                .Include(item => item.User)
                .Where(item => item.StarRating == star).ToList();
            if (productId != null) lists = lists.Where(item => item.ProductId == (int)productId).ToList();
            if (orderByDate != null)
            {
                if (orderByDate is false)
                {
                    lists = lists.OrderBy(item => item.UpdatedAt);
                }
                else
                {
                    lists = lists.OrderByDescending(item => item.UpdatedAt);
                }
            }
            return Task.FromResult(lists);
        }
        public async Task<IEnumerable<StatisticProductDTO>> GetTopRatingProducts(int? month, int? year)
        {
            var results = new List<StatisticProductDTO>();
            var allRatings = new List<Rating>();
            var products = await _context.Products.Include(s => s.Size).Select((item) => new
            {
                ProductId = item.ProductId,
                ProductName = item.ProductName,
                ProductPrice = item.UnitPrice,
                ProductImage = item.Image,
                ProductSize = item.Size.SizeName
            }
            ).ToListAsync();
            System.Console.WriteLine("Before into forreach");

            // int limit = 1000;

            foreach (var product in products)
            {
                var tempProduct = new StatisticProductDTO()
                {
                    ProductId = product.ProductId,
                    ProductName = product.ProductName,
                    ProductImage = product.ProductImage,
                    ProductPrice = product.ProductPrice,
                    Size = product.ProductSize
                };

                var temps = await _context.Ratings.AsQueryable().OrderBy(r => r.ProductId).Where(
                        r => (r.ProductId == product.ProductId) &&
                        (!month.HasValue || ((DateTime)r.UpdatedAt).Month == month.Value) &&
                        (!year.HasValue || ((DateTime)r.UpdatedAt).Year == year.Value)
                    ).Select((item) => item.StarRating).ToListAsync();

                if (temps.Count == 0 || temps == null)
                {
                    continue;
                }

                tempProduct.AverageStar = await GetAverageStars(temps);

                // int page = 1;
                // while (true)
                // {
                //     var temps = await _context.Ratings.AsQueryable().OrderBy(r => r.ProductId).Where(
                //         r => (r.ProductId == product.ProductId) &&
                //         (!month.HasValue || ((DateTime)r.UpdatedAt).Month == month.Value) && 
                //         (!year.HasValue || ((DateTime)r.UpdatedAt).Year == year.Value)
                //     ).Skip((page - 1) * limit).Take(limit).Select((item) => item.StarRating).ToListAsync();

                //     if(temps.Count == 0 || temps == null)
                //     {
                //         break;
                //     }

                //     var average = await GetAverageStars(temps);

                //     if(tempProduct.AverageStar == 0)
                //     {
                //         tempProduct.AverageStar = average;
                //     }
                //     else
                //     {
                //         tempProduct.AverageStar = (tempProduct.AverageStar + average) / 2;
                //     }
                //     page ++;
                // }
                tempProduct.AverageStar = Math.Round(tempProduct.AverageStar, 2);
                results.Add(tempProduct);
            }

            return results.OrderByDescending(item => item.AverageStar).Take(9);
        }
        public Task<double> GetAverageStars(IEnumerable<int> ratings)
        {
            double average = 0.0;

            foreach (var item in ratings)
            {
                average += item;
            }

            average /= ratings.Count();
            return Task.FromResult(average);
        }
    }
}