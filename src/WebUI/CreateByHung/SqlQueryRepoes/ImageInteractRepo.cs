using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebUI.CreateByHung.DTOs.ImageDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Data;
using WebUI.Models;
using Microsoft.EntityFrameworkCore;

namespace WebUI.CreateByHung.SqlQueryRepoes
{
    public class ImageInteractRepo : IImageRepo
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public ImageInteractRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;        
        }
        private async Task<byte[]> ConvertImageToBytes(IFormFile imageUpload)
        {
            if(imageUpload.Length > 0)
            {
                if(imageUpload.Length > 2000000)
                {
                    throw new System.Exception("Image is greater than 2 MB");
                }
                using (var ms = new MemoryStream())
                {
                    imageUpload.CopyTo(ms);
                    var bytes = ms.ToArray();
                    await ms.FlushAsync();
                    // return Task.FromResult(bytes);
                    return bytes;
                }
            }
            else{
                throw new System.Exception("Missing Product Image");
            }
        }
        private byte[] ConvertBytesToImage(string base64string)
        {
            byte[] bytes = null;
            if(string.IsNullOrEmpty(base64string) is false)
            {
                bytes = Convert.FromBase64String(base64string);
            }
            return bytes;
        }
        public Task<Product> GetProduct(string name)
        {
            if(name != null)
            {
                return Task.FromResult(
                    _context.Products
                        .Where(
                            p => p.ProductName.Equals(name))
                        .FirstOrDefault());
            }
            return null;
        }
        public async Task<string> SaveProductImage(ImageUploadDTO uploadDTO)
        {
            if(uploadDTO.EntityType != Others.EntityEnum.Product)
            {
                throw new System.Exception("Wrong Type of Entity");
            }
            uploadDTO.OwnerName = uploadDTO.OwnerName.Trim();
            Console.WriteLine(uploadDTO.OwnerName + DateTime.Now);
            var product = await GetProduct(uploadDTO.OwnerName);
            if(product == null)
            {
                throw new Exception("Can not find the product");
            }
            var bytes = await ConvertImageToBytes(uploadDTO.Image);
            if(product.ProductIllustration != null && product.ProductIllustration.Equals(bytes))
            {
                throw new Exception("This image is existed");
            }
            product.ProductIllustration = bytes;
            await _context.SaveChangesAsync();
            return product.ProductName;
        }
        public async Task<int> SaveAccountAvatar(ImageUploadDTO uploadDTO, string host)
        {
            if(uploadDTO.EntityType != Others.EntityEnum.Account)
            {
                throw new System.Exception("Wrong Type of Entity");
            }
            var bytes = await ConvertImageToBytes(uploadDTO.Image);
            var account = _context.Accounts
                .Where(
                    p => p.Username.Equals(uploadDTO.OwnerName))
                .FirstOrDefault();
            if(account == null)
            {
                throw new Exception("Can not find the account");
            }
            account.AccountIllustration = bytes;

            account.Avatar = "https://" + host + "/api/images/account/account=" + account.AccountId;
            await _context.SaveChangesAsync();
            return account.AccountId;
        }

        public async Task<byte[]> GetProductImage(string name)
        {
            var product = await GetProduct(name);
            if(product == null)
            {
                throw new Exception("Can not find the product");
            }
            return ConvertBytesToImage(Convert.ToBase64String(product.ProductIllustration));
        }

        public async Task<byte[]> GetAccountAvatar(int accountId)
        {
            // var product = await GetProduct(name);
            var account = await _context.Accounts.Where(u => u.AccountId == accountId).Select(u => new Account{
                AccountId = u.AccountId, 
                AccountIllustration = u.AccountIllustration
            })
            .FirstOrDefaultAsync();

            if(account == null)
            {
                throw new Exception("Can not find the product");
            }
            return ConvertBytesToImage(Convert.ToBase64String(account.AccountIllustration));
        }
    }
}