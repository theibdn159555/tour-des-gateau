using WebUI.Models;
using WebUI.CreateByHung.Interfaces;
using System.Collections.Generic;
using WebUI.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.DTOs.CategoryDTOs;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlProductRepo : IRepo<Product>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlProductRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(Product target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Product));
            }

            if(await FindExist(target.ProductName))
                throw new ObjectExistedException("Record is Existed");

            int id = 0;

            if(await FindExistCategory(new Category() {CategoryId = target.CategoryId}))
            {
                if(await FindExistSize(new Size() {SizeId = target.SizeId}))
                {
                    if(await FindExistEvent(new DiscountEvent() {EventId = (int)target.EventId}))
                    {
                        if(await FindExistTag(new Tag() {TagId = target.TagId}))
                        {
                            await _context.Products.AddAsync(target);
                            bool save = await SaveChanges();
                            if(save)
                            {
                                target = _context.Products.Where(o => o.ProductName.CompareTo(target.ProductName) == 0).FirstOrDefault();
                                id = target != null ? target.ProductId : id;
                            }
                        }                            
                        else throw new ObjectExistedException("Tag is not exist");
                    }
                    else throw new ObjectExistedException("DiscountEvent is not exist");
                }
                else throw new ObjectExistedException("Size is not exist");
            }
            else throw new ObjectExistedException("Category is not exist");
            
            return id;
        }
        public void DeleteOld(Product target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Product));
            }
            _context.Products.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.Products.AnyAsync(p => p.ProductName.CompareTo(target) == 0);
        }
        public async Task<bool> FindExist(Product target)
        {
            return await _context.Products.AnyAsync(tag => tag.ProductName.CompareTo(target.ProductName) == 0);
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            var getList = await _context.Products
            .OrderBy(
                p => p.ProductId
            )
            .Include(p => p.Category)
            .Include(p => p.Tag)
            .Include(p => p.Size)
            .Include(p => p.Event)
            .Select(p => new {
                ProductId = p.ProductId,
                ProductName = p.ProductName,
                UnitPrice = p.UnitPrice,
                Image = p.Image,
                SizeId = p.SizeId,
                Size = p.Size,
                CategoryId = p.CategoryId,
                Category = p.Category,
                EventId = p.EventId,
                Event = p.Event,
                TagId = p.TagId,
                Tag = p.Tag,
                Description = p.Description
            })
            .ToListAsync();
            var products = new List<Product>();
            foreach (var p in getList)
            {
                var temp = new Product()
                {
                    ProductId = p.ProductId,
                    ProductName = p.ProductName,
                    UnitPrice = p.UnitPrice,
                    Image = p.Image,
                    SizeId = p.SizeId,
                    Size = p.Size,
                    CategoryId = p.CategoryId,
                    Category = p.Category,
                    EventId = p.EventId,
                    Event = p.Event,
                    TagId = p.TagId,
                    Tag = p.Tag,
                    Description = p.Description
                };
                products.Add(temp);
            }
            return products;
        }

        public Task<Product> GetByID(int id)
        {
            Product product = _context.Products
                                .Include(p => p.Category)
                                .Include(p => p.Size)
                                .Include(p => p.Event)
                                .Include(p => p.Tag)
                                .FirstOrDefault(p => p.ProductId == id);
            return Task.FromResult(product);
        }

        // public Task<IEnumerable<Product>> GetProductsPerPage(int page, int limit)
        // {
        //     IEnumerable<Product> products = _context.Products.Skip((page - 1 ) * limit).Take(limit).ToList();
        //     return Task.FromResult(products);
        // }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public void UpdateOld(Product target)
        {
            
        }
        public async Task<bool> FindExistCategory(Category target){
            return await _context.Categories.AnyAsync(c => c.CategoryId == target.CategoryId);
        }
        public async Task<bool> FindExistSize(Size target){
            return await _context.Sizes.AnyAsync(c => c.SizeId == target.SizeId);
        }
        public async Task<bool> FindExistEvent(DiscountEvent target){
            return await _context.DiscountEvents.AnyAsync(c => c.EventId == target.EventId);
        }public async Task<bool> FindExistTag(Tag target){
            return await _context.Tags.AnyAsync(c => c.TagId == target.TagId);
        }

        public async Task<IEnumerable<Product>> Search(string name)
        {
            IEnumerable<Product> items = await _context.Products
                                            .Include(p => p.Category)
                                            .Include(p => p.Event)
                                            .Include(p => p.Size)
                                            .Include(p => p.Tag)
                                            .Where(p => p.ProductName.Contains(name))
                                            .Select(p => new Product(){
                                                ProductId = p.ProductId,
                                                ProductName = p.ProductName,
                                                CategoryId = p.CategoryId,
                                                Category = p.Category,
                                                Size = p.Size,
                                                SizeId = p.SizeId,
                                                Tag = p.Tag,
                                                TagId = p.TagId,
                                                UnitPrice = p.UnitPrice,
                                                Description = p.Description,
                                                Image = p.Image,
                                                Event = p.Event,
                                                EventId = p.EventId,
                                                CreatedAt = p.CreatedAt,
                                                UpdatedAt = p.UpdatedAt
                                            }).ToListAsync();
            // return Task.FromResult(items);
            return items;
        }
        public Task<IEnumerable<Product>> Filter(IEnumerable<int> categoryIds, int? sizeId, int? priceRange, bool? orderBy)
        {
            IEnumerable<Product> products = _context.Products
                                            .Include(p => p.Category)
                                            .Include(p => p.Event)
                                            .Include(p => p.Size)
                                            .Include(p => p.Tag)
                                            .Select(p => new Product(){
                                                ProductId = p.ProductId,
                                                ProductName = p.ProductName,
                                                CategoryId = p.CategoryId,
                                                Category = p.Category,
                                                Size = p.Size,
                                                SizeId = p.SizeId,
                                                Tag = p.Tag,
                                                TagId = p.TagId,
                                                UnitPrice = p.UnitPrice,
                                                Description = p.Description,
                                                Image = p.Image,
                                                Event = p.Event,
                                                EventId = p.EventId,
                                                CreatedAt = p.CreatedAt,
                                                UpdatedAt = p.UpdatedAt
                                            }).ToList();

            if(categoryIds.Count() != 0)
            {
                List<Product> results = new List<Product>();
                foreach (var item in categoryIds)
                {
                    foreach (var product in products)
                    {
                        if(product.CategoryId == item)
                        {
                            results.Add(product);
                        }
                    }
                }
                products = null;
                products = results;
            }
            if(sizeId != null)
            {
                products = products.Where(p => p.SizeId == sizeId);
            }
            if(priceRange != null)
            {
                switch ((int)priceRange)
                {
                    case 1:
                        products = products.Where(p => p.UnitPrice <= 100);
                        break;
                    case 2:
                        products = products.Where(p => p.UnitPrice > 100 && p.UnitPrice <= 500);
                        break;
                    default:
                        products = products.Where(p => p.UnitPrice > 500);
                        break;
                }
            }
            if(orderBy != null)
            {
                if((bool)orderBy)
                {
                    products = products.OrderBy(p => p.ProductName);
                }
                else
                {
                    products = products.OrderByDescending(p => p.ProductName);
                }
            }
            return Task.FromResult(products);
        }
        public Task<IEnumerable<Product>> GetProductsByTag(int tagId)
        {
            IEnumerable<Product> products = _context.Products.Where(p => p.TagId == tagId).ToList();
            return Task.FromResult(products);
        }
        
    }
}