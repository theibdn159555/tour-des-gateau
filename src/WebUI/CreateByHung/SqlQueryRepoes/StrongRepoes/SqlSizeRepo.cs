using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlSizeRepo : IRepo<Size>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlSizeRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(Size target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Size));
            }
            if(await FindExist(target.SizeName))
                throw new ObjectExistedException("Record is Existed");
            await _context.Sizes.AddAsync(target);
            
            int id = 0;
            bool save = await SaveChanges();
            if(save)
            {
                target = _context.Sizes.Where(o => o.SizeName.CompareTo(target.SizeName) == 0).FirstOrDefault();
                id = target != null ? target.SizeId : id;
            }            
            return id;
        }

        public void DeleteOld(Size target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Size));
            }
            _context.Sizes.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.Sizes.AnyAsync(s => s.SizeName.CompareTo(target) == 0);
        }

        public async Task<bool> FindExist(Size target)
        {
            return await _context.Sizes.AnyAsync(tag => tag.SizeName.CompareTo(target.SizeName) == 0);
        }

        public Task<IEnumerable<Size>> GetAll()
        {
            IEnumerable<Size> sizes = _context.Sizes.OrderBy(s => s.SizeId).ToList();
            return Task.FromResult(sizes);
        }

        public Task<Size> GetByID(int id)
        {
            var category = _context.Sizes.Include(s => s.Products).FirstOrDefault(p => p.SizeId == id);
            return Task.FromResult(category);
        }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public Task<IEnumerable<Size>> Search(string name)
        {
            IEnumerable<Size> sizes = _context.Sizes.Where(size => size.SizeName.Contains(name));
            return Task.FromResult(sizes);
        }

        public void UpdateOld(Size target)
        {
            
        }
    }
}