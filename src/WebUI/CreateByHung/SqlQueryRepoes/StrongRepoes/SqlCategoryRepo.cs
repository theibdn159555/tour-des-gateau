using WebUI.Models;
using WebUI.CreateByHung.Interfaces;
using System.Collections.Generic;
using WebUI.Data;
using System.Linq;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Exceptions;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlCategoryRepo : IRepo<Category>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlCategoryRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        public async Task<int> CreateNew(Category target)
        {
            if(target == null)
            {
                throw new ArgumentNullException(nameof(Category));
            }

            if(await FindExist(target.CategoryName))
            {
                throw new ObjectExistedException("Record is Existed");
            }

            await _context.Categories.AddAsync(target);
            
            int id = 0;
            bool save = await SaveChanges();
            if(save)
            {
                target = _context.Categories.Where(o => o.CategoryName.CompareTo(target.CategoryName) == 0).FirstOrDefault();
                id = target != null ? target.CategoryId : id;
            }            
            return id;
        }

        public void DeleteOld(Category target)
        {
            if (target == null)
            {
                throw new ArgumentNullException(nameof(Category));
            }
            _context.Categories.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.Categories.AnyAsync(t => t.CategoryName.CompareTo(target) == 0);
        }
        public async Task<bool> FindExist(Category target)
        {
            return await _context.Categories.AnyAsync(tag => tag.CategoryName.CompareTo(target.CategoryName) == 0);
        }

        public Task<IEnumerable<Category>> GetAll()
        {
            IEnumerable<Category> categories = _context.Categories.OrderBy(c => c.CategoryId).ToList();
            return Task.FromResult(categories);
        }

        public Task<Category> GetByID(int id)
        {
            Category category = _context.Categories.Include(c => c.Products).FirstOrDefault(p => p.CategoryId == id);
            return Task.FromResult(category);
        }

        public async Task<bool> SaveChanges()
        {
            return ( await _context.SaveChangesAsync() >= 0 );
        }

        public Task<IEnumerable<Category>> Search(string name)
        {
            IEnumerable<Category> categories = _context.Categories.Where(c => c.CategoryName.Contains(name));
            return Task.FromResult(categories);
        }

        public void UpdateOld(Category target)
        {
            
        }
    }
}