using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.Data;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using AutoMapper;
using System.Linq;
using WebUI.CreateByHung.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlDiscountEventRepo : IRepo<DiscountEvent>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlDiscountEventRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<int> CreateNew(DiscountEvent target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(DiscountEvent));
            }

            if(await FindExist(target.EventName))
            {
                throw new ObjectExistedException("Record is Existed");
            }
            
            await _context.DiscountEvents.AddAsync(target);
            
            int id = 0;
            bool save = await SaveChanges();
            if(save)
            {
                target = _context.DiscountEvents.Where(o => o.EventName.CompareTo(target.EventName) == 0).FirstOrDefault();
                id = target != null ? target.EventId : id;
            }            
            return id;
        }

        public void DeleteOld(DiscountEvent target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(DiscountEvent));
            }
            _context.DiscountEvents.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.DiscountEvents.AnyAsync(t => t.EventName.CompareTo(target) == 0);
        }
        public async Task<bool> FindExist(DiscountEvent target)
        {
            return await _context.DiscountEvents.AnyAsync(tag => tag.EventName.CompareTo(target.EventName) == 0);
        }

        public Task<IEnumerable<DiscountEvent>> GetAll()
        {
            IEnumerable<DiscountEvent> events = _context.DiscountEvents.OrderBy(e => e.EventId).ToList();
            return Task.FromResult(events);
        }

        public Task<DiscountEvent> GetByID(int id)
        {
            var discountEvent = _context.DiscountEvents.Include(e => e.Products).FirstOrDefault(e => e.EventId == id);
            return Task.FromResult(discountEvent);
        }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public Task<IEnumerable<DiscountEvent>> Search(string name)
        {
            IEnumerable<DiscountEvent> events = _context.DiscountEvents.Where(e => e.EventName.Contains(name));
            return Task.FromResult(events);
        }

        public void UpdateOld(DiscountEvent target)
        {
            
        }
    }
}