using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using WebUI.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Exceptions;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlMaterialRepo : IRepo<Material>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlMaterialRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<int> CreateNew(Material target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(Material));
            }
            if(await FindExist(target.MaterialName))
                throw new ObjectExistedException("Record is Existed");
            await _context.Materials.AddAsync(target);

            int id = 0;
            bool save = await SaveChanges();
            if(save)
            {
                target = _context.Materials.Where(o => o.MaterialName.CompareTo(target.MaterialName) == 0).FirstOrDefault();
                id = target != null ? target.MaterialId : id;
            }            
            return id;
        }

        public void DeleteOld(Material target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(Material));
            }
            _context.Materials.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.Materials.AnyAsync(Material => Material.MaterialName.CompareTo(target) == 0);
        }

        public async Task<bool> FindExist(Material target)
        {
            return await _context.Materials.AnyAsync(Material => Material.MaterialName.CompareTo(target.MaterialName) == 0);
        }

        public Task<IEnumerable<Material>> GetAll()
        {
            IEnumerable<Material> Materials = _context.Materials.OrderBy(Material => Material.MaterialId).ToList();
            return Task.FromResult(Materials);
        }

        public Task<Material> GetByID(int id)
        {
            var Material = _context.Materials
                            .Include(m => m.Ingredients)
                                .ThenInclude(ingredient => ingredient.Product)
                            .FirstOrDefault(p => p.MaterialId == id);
            return Task.FromResult(Material);
        }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public Task<IEnumerable<Material>> Search(string name)
        {
            IEnumerable<Material> Materials = _context.Materials.Where(Material => Material.MaterialName.Contains(name));
            return Task.FromResult(Materials);
        }

        public void UpdateOld(Material target)
        {
            
        }
    }
}