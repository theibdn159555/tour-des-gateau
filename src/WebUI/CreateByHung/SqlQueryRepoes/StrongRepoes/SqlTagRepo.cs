using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using WebUI.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.Exceptions;

namespace WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes
{
    public class SqlTagRepo : IRepo<Tag>
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlTagRepo(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<int> CreateNew(Tag target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(Tag));
            }
            if(await FindExist(target.NameTag))
                throw new ObjectExistedException("Record is Existed");
            await _context.Tags.AddAsync(target);

            int id = 0;
            bool save = await SaveChanges();
            if(save)
            {
                target = _context.Tags.Where(o => o.NameTag.CompareTo(target.NameTag) == 0).FirstOrDefault();
                id = target != null ? target.TagId : id;
            }            
            return id;
        }

        public void DeleteOld(Tag target)
        {
            if(target == null){
                throw new ArgumentNullException(nameof(Tag));
            }
            _context.Tags.Remove(target);
        }

        public async Task<bool> FindExist(string target)
        {
            return await _context.Tags.AnyAsync(tag => tag.NameTag.CompareTo(target) == 0);
        }

        public async Task<bool> FindExist(Tag target)
        {
            return await _context.Tags.AnyAsync(tag => tag.NameTag.CompareTo(target.NameTag) == 0);
        }

        public Task<IEnumerable<Tag>> GetAll()
        {
            IEnumerable<Tag> tags = _context.Tags.OrderBy(tag => tag.TagId).ToList();
            return Task.FromResult(tags);
        }

        public Task<Tag> GetByID(int id)
        {
            var tag = _context.Tags.Include(t => t.Products)
            .Select(
                t => new Tag{
                    TagId = t.TagId,
                    NameTag = t.NameTag,
                    Products = t.Products.Select(
                        p => new Product{
                            ProductId = p.ProductId,
                            ProductName = p.ProductName,
                            Image = p.Image
                        }
                    ).ToList()
                }
                
            )
            .FirstOrDefault(p => p.TagId == id);
            return Task.FromResult(tag);
        }

        public async Task<bool> SaveChanges()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public Task<IEnumerable<Tag>> Search(string name)
        {
            IEnumerable<Tag> tags = _context.Tags.Where(tag => tag.NameTag.Contains(name));
            return Task.FromResult(tags);
        }

        public void UpdateOld(Tag target)
        {
            
        }
    }
}