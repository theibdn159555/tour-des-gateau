using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;
using WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes;
using WebUI.Data;
using WebUI.Models;

namespace WebUI.CreateByHung.SqlQueryRepoes.AdminRepoes
{
    public class SqlAdminRepo : IAdminRepo
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public SqlAdminRepo(WebUI.Data.db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }
        public async Task<OrderRevenueDTO> GetRevenue(int? year = null, int? month = null)
        {
            IEnumerable<Order> completedOrders = new List<Order>();
            IEnumerable<Order> previousMonthOrders = new List<Order>();
            decimal totalRevenue = 0;

            if(year == null)
            {
                completedOrders = await _context.Orders
                .Include(c => c.Cart)
                    .ThenInclude(u => u.Account)
                .Include(c => c.Cart)
                    .ThenInclude(d => d.CartDetails)
                .Where(o => o.StatusId == 4)
                .ToListAsync();
                foreach (var item in completedOrders)
                {
                    decimal totalOfEachOrder = 0;
                    totalOfEachOrder = item.Cart.CartDetails.Sum(
                        (d) => d.Quantity * d.UnitPrice * (1 - (decimal)d.Cart.Account.Membership * (decimal)d.DiscountValue)
                    );
                    totalRevenue += totalOfEachOrder;
                }

                System.Console.WriteLine($"totalRevenue: {totalRevenue}");

                return new OrderRevenueDTO()
                {
                    TotalRevenue = totalRevenue
                };
            }

            if(year != null)
            {            
                if(month != null)
                {
                    int previousMonth = (int)month - 1;
                    int previousYear = (int)year;
                    if(month == 1)
                    {
                        previousMonth = 12;
                        previousYear -= 1;
                    }
                    completedOrders = await GetOrdersByMonth((int)month, (int)year);
                    previousMonthOrders = await GetOrdersByMonth(previousMonth, previousYear);
                    if(completedOrders.Count() == 0)
                    {
                        return new OrderRevenueDTO(){
                            TotalRevenue = 0,
                            IsGrow = null,
                            Error = "No data"
                        };
                    }
                }
            }

            totalRevenue = await CalculateRevenue(completedOrders);
            decimal totalPreviousRevenue = await CalculateRevenue(previousMonthOrders);
            
            System.Console.WriteLine($"revenueByDate: {totalRevenue}");

            return new OrderRevenueDTO()
            {
                TotalRevenue = totalRevenue,
                IsGrow = totalRevenue > totalPreviousRevenue ? true : false
            };
        }
        private Task<decimal> CalculateRevenue(IEnumerable<Order> listOrders)
        {
            decimal totalRevenue = 0;
            if(listOrders == null) return Task.FromResult(totalRevenue);
            foreach (var item in listOrders)
            {
                if(item.StatusId == 4)
                {
                    decimal totalOfEachOrder = 0;
                    totalOfEachOrder = item.Cart.CartDetails.Sum(
                        (d) => d.Quantity * d.UnitPrice * (1 - (decimal)d.Cart.Account.Membership * (decimal)d.DiscountValue)
                    );
                    totalRevenue += totalOfEachOrder;
                }
            }
            return Task.FromResult(totalRevenue);
        }
        public async Task<IEnumerable<Order>> GetOrdersByMonth(int month, int year)
        {
            return await _context.Orders
            .Include(c => c.Cart)
                .ThenInclude(d => d.CartDetails)
            .Include(c => c.Cart)
                .ThenInclude(a => a.Account)
            .Where(
                o => ((DateTime)o.UpdatedAt).Year == year 
                && ((DateTime)o.UpdatedAt).Month == month)
            .ToListAsync();
        }

        public async Task<StatisticProductDTO> GetQuantitiesPerMonth(int productId, int year)
        {
            var quantitiesDTOs = new StatisticProductDTO();
            quantitiesDTOs.ProductId = productId;
            quantitiesDTOs.QuantityPerMonth = new List<StatisticProductDTO.PerMonth>();
            quantitiesDTOs.Year = year;
            var orders = await _context.Orders
                .Where(o => o.StatusId == 4 && ((DateTime)o.UpdatedAt).Year == year)
                .Include(c => c.Cart)
                .ThenInclude(d => d.CartDetails)
                .ToListAsync();
            for (int i = 1; i < 13; i++)
            {
                int sumQuantity = 0;
                orders.ForEach(                    
                    (element) => {
                        element.Cart.CartDetails
                        .Where(
                            d => d.ProductId == productId && 
                            ((DateTime)d.UpdatedAt).Month == i)
                        .ToList()
                        .ForEach((e) => sumQuantity += e.Quantity);
                    }
                );
                if(sumQuantity != 0)
                    quantitiesDTOs.QuantityPerMonth.Add(
                        new StatisticProductDTO.PerMonth()
                        {
                            Quantity = sumQuantity,
                                Month = i
                        }
                    );
            }
            return quantitiesDTOs;
        }

        public async Task<StatisticProductDTO> GetTotalSoldQuantitiesOfProduct(int productId)
        {
            var quantitiesDTOs = new StatisticProductDTO();
            quantitiesDTOs.ProductId = productId;
            var orders = await _context.Orders
                .Where(o => o.StatusId == 4)
                .Include(c => c.Cart)
                .ThenInclude(d => d.CartDetails)
                .ToListAsync();
            int sumQuantity = 0;
            orders.ForEach(                    
                (element) => {
                    element.Cart.CartDetails
                    .Where(d => d.ProductId == productId)
                    .ToList()
                    .ForEach((e) => sumQuantity += e.Quantity);
                }
            );
            quantitiesDTOs.SoldQuantites = sumQuantity;
            return quantitiesDTOs;
        }
        public Task<int> GetProductTotalFollowers(int productId)
        {
            int followers = _context.Favorites.Where(f => f.ProductId == productId).Count();
            return Task.FromResult(followers);
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() >= 0;
        }
        public async void ApplyEventAll(int id)
        {
            await _context.Products.ForEachAsync(
                (item) => {
                    if(item.EventId == id)
                    {
                        throw new ObjectExistedException("This event is applied");
                    }
                    item.EventId = id;
                }
            );
        }
        public async Task<IEnumerable<DiscountEvent>> GetNearestEvents()
        {
            // DateTime milestone = DateTime.Now;
            var milestone = await CalculateNextMileStone(2, DateTime.Now);
            IEnumerable<DiscountEvent> discountEvent = await _context.DiscountEvents
            .Where(
                e => 
                    ((DateTime)e.DateStart).Date >= DateTime.Now.Date && 
                    ((DateTime)e.DateStart).Date <= milestone.Date
            )
            .ToListAsync();
            // return Task.FromResult(discountEvent);
            return discountEvent;
        }
        public Task<DateTime> CalculateNextMileStone(int range, DateTime time)
        {
            var milestone = time;
            milestone = milestone.AddMonths(range);    
            System.Console.WriteLine(milestone);
            int day = DateTime.DaysInMonth(milestone.Year, milestone.Month);
            milestone = milestone.AddDays(day - milestone.Day);
            return Task.FromResult(milestone);
        }
        private async Task<OrderStatus> IsStatusAble(string status)
        {
            status = status.Trim();
            if(status.Contains("+")) status = status.Replace("+", " ");
            if(status.Contains("-")) status = status.Replace("-", " ");
            if(status.Equals(null))
            {
                throw new Exception("status name is not null");
            }
            Regex regex = new Regex(@"^\d$");
            if(regex.IsMatch(status))
            {
                throw new Exception("status name must be alphabet characters");
            }
            foreach (var item in _context.OrderStatuses.ToList())
            {
                // System.Console.WriteLine("item name to lower: " + item.StatusName.ToLower());
                if(item.StatusName.ToLower().Equals(status.ToLower()))
                {
                    System.Console.WriteLine("Found");
                    return item;
                }
            }
            var statusEntity = await _context.OrderStatuses
            .Where(s => s.StatusName.ToLower().Equals(status.ToLower())).FirstOrDefaultAsync();

            if(statusEntity is null)
            {
                throw new Exception("status name is not existed");
            }
            return statusEntity;
        }
        public async Task<PaginationRepo<Order>> StatusPagination(string status, PaginationModel paging, bool orderByOlder = true)
        {
            var target = await IsStatusAble(status);
            int total = _context.Orders.Where(order => order.StatusId == target.StatusId).Count();
            List<Order> orders = new List<Order>();
            if(orderByOlder == false)
            {
                orders = _context.Orders.OrderByDescending(order => order.CreatedAt)
                    .Where(
                            order => order.StatusId == target.StatusId)
                    .Skip((paging.Page - 1 ) * paging.Limit)
                    .Take(paging.Limit).ToList();
            }
            else 
            {
                orders = _context.Orders.OrderBy(order => order.CreatedAt)
                    .Where(
                            order => order.StatusId == target.StatusId)
                    .Skip((paging.Page - 1 ) * paging.Limit)
                    .Take(paging.Limit).ToList();
            }

            var paginationMetadata = new PaginationMetadata(paging.Page, total, paging.Limit);
            // if(paginationMetadata.CurrentPage > paginationMetadata.TotalPages)
            // {
            //     throw new Exception("Out of list");
            // }
            return new PaginationRepo<Order>(
                paginationMetadata,
                orders
            );
        }
        public async Task<List<StatisticProductDTO>> GetTopSoldProductsByMonth(int year, int month, int top = 3)
        {
            if(ValidationInputData.IsDatetTimeValid(year, month) == false) return null;

            var statisticList = new List<StatisticProductDTO>();

            int page = 1, limit = 1000;

            bool searching = true;

            while (searching)
            {                 
                var orderItems = await (from dc in _context.CartDetails
                join c in _context.Carts on dc.Cart equals c
                join o in _context.Orders on c equals o.Cart
                join p in _context.Products on dc.ProductId equals p.ProductId
                where ((DateTime)dc.UpdatedAt).Year == year 
                    && ((DateTime)dc.UpdatedAt).Month == month
                    && o != null
                    && o.StatusId == 4
                select new {
                    dc.ProductId,
                    dc.Quantity
                }).OrderBy(i => i.ProductId).Skip((page - 1) * limit).Take(limit).ToListAsync();

                if(orderItems.Count() == 0){
                    System.Console.WriteLine("Nothing");
                    break;
                }

                System.Console.WriteLine("Get 1000");
                // orderItems.LogOutList();

                var results = orderItems.GroupBy(i => i.ProductId).Select(
                    (item) => new {
                        item.Key,
                        Sum = item.Sum(p => p.Quantity)
                    }
                ).OrderByDescending(i => i.Sum);
                
                var products = await _context.Products.Select(
                    (item) => new {
                        item.ProductId,
                        item.ProductName,
                        item.Image
                    }
                )
                .ToListAsync();

                // results.LogOutList();

                foreach (var item in results)
                {
                    var existStatisticItem = statisticList.Where(sItem => sItem.ProductId == item.Key).FirstOrDefault();
                    var statisticPos = statisticList.IndexOf(existStatisticItem);
                    if(statisticPos >= 0)
                    {
                        statisticList[statisticPos].SoldQuantites += item.Sum;
                    }
                    else
                    {
                        var temp = products.Where(p => p.ProductId == item.Key).FirstOrDefault();
                        var newStatisticItem = new StatisticProductDTO(){
                            ProductId = item.Key,
                            SoldQuantites = item.Sum,
                            Product = new Product(){
                                ProductId = temp.ProductId,
                                ProductName = temp.ProductName,
                                Image = temp.Image
                            },
                            Year = year
                        };
                        statisticList.Add(newStatisticItem);                        
                    }                        
                }
                System.Console.WriteLine("assign: done => go next page");
                GC.Collect();
                GC.WaitForPendingFinalizers();
                page ++;
            }
            statisticList = statisticList.OrderByDescending(item => item.SoldQuantites).Take(top).ToList();
            return statisticList;
        }
        public async Task<OrderRevenueDTO> GetStatisticOrdersByMonth(int year, int month, string status)
        {
            var orderStatus = await IsStatusAble(status);
            var totalOrders = await _context.Orders
            .Where(
                o => o.StatusId == orderStatus.StatusId
                && ((DateTime)o.UpdatedAt).Year == year
                && ((DateTime)o.UpdatedAt).Month == month
            )
            .ToListAsync();
            if(month == 1)
            {
                year --;
                month = 12;
            }
            else
            {
                month --;
            }
            var totalPreviousOrders = await _context.Orders
            .Where(
                o => o.StatusId == orderStatus.StatusId
                && ((DateTime)o.UpdatedAt).Year == year
                && ((DateTime)o.UpdatedAt).Month == month
            )
            .ToListAsync();
            var statistic = new OrderRevenueDTO()
            {
                TotalRevenue = totalOrders.Count
            };
            System.Console.WriteLine($"{nameof(totalPreviousOrders)} {totalPreviousOrders.Count}");
            System.Console.WriteLine($"{nameof(totalOrders)} {totalOrders.Count}");
            if(totalPreviousOrders.Count == 0 || totalPreviousOrders == null)
                statistic.IsGrow = true;
            else
            {
                if(totalOrders.Count >= totalPreviousOrders.Count)
                {
                    statistic.IsGrow = true;
                }
                else
                {
                    statistic.IsGrow = false;
                }
            }
            return statistic;
        }
        
    }
}