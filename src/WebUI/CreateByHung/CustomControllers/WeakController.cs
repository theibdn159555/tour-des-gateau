using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.Interfaces;

namespace WebUI.CreateByHung.CustomControllers
{
    public class WeakController<M, R, W, U> : ControllerBase
    where M : class
    where R : class
    where W : class
    where U : class
    {
        protected readonly IWeakRepo<M> _repository;
        protected readonly IMapper _mapper;

        public WeakController(IWeakRepo<M> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        
    }
}