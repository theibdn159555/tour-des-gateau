using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;

namespace WebUI.CreateByHung.CustomControllers
{
    ///<summary>
    ///<c>A Controller</c> inherit from ControllerBase
    ///<para>
    ///<see cref="GenericType{M} is Domain Model"/>
    ///</para>
    ///<para>
    ///<see cref="GenericType{R} is ReadDTO"/>
    ///</para>
    ///<para>
    ///<see cref="GenericType{W} is WriteDTO"/>
    ///</para>
    ///</summary>
    public class StrongController<M, R, W, U> : ControllerBase 
    where M : class
    where R : class
    where W : class
    where U : class
    {
        /*
        M - Domain Model
        R - Read DTO
        W - Write DTO
        */
        protected readonly IRepo<M> _repository;
        protected readonly IMapper _mapper;

        public StrongController()
        {
            
        }

        public StrongController(IRepo<M> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("all")]
        public virtual async Task<ActionResult<IEnumerable<R>>> GetAll()
        {
            var lists = await _repository.GetAll();
            if(lists == null) return NotFound();
            return Ok(_mapper.Map<IEnumerable<R>>(lists));
        }

        public virtual async Task<ActionResult<R>> GetItemByID(int id)
        {
            var item = await _repository.GetByID(id);
            if(item == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<R>(item));
        }

        protected ActionResult<IEnumerable<R>> GetPerPage(int page, IEnumerable<M> items, int? limit)
        {
            PaginationResultSet<R, M> paginationResultSet = new PaginationResultSet<R, M>(page, items, _mapper);
            
            try
            {
                int limited = limit == null ? 6 : (int)limit;
                paginationResultSet.GetPagination(limited);
            }
            catch (ObjectNotFoundException)
            {
                return NotFound();
            }

            return Ok(
                new {
                    paginationResultSet.Metadata,
                    paginationResultSet.MappingResults
                }
            );
        }

        [HttpGet]
        [HttpGet("page-{page}")]
        public virtual async Task<ActionResult<IEnumerable<R>>> GetAllPerPage(int page, [FromQuery]int? limit)
        {
            var items = await _repository.GetAll();
            return this.GetPerPage(page, items, limit);
        }

        [HttpGet("search={search}")]
        [HttpGet("search={search}/page-{page}")]
        public virtual async Task<ActionResult<IEnumerable<R>>> SearchByName(string search, int page, [FromQuery] int? limit)
        {
            search = search.Trim();
            if(search.Contains("+"))
                search = search.Replace("+", " ");
            
            var items = await _repository.Search(search);
            if(items.Count() == 0){
                return NotFound();
            }

            return GetPerPage(page, items, limit);            
        }
        public virtual async Task<ActionResult<W>> CreateRecord(W writeDto, string routeName)
        {
            var model = _mapper.Map<M>(writeDto);               
            // System.Console.WriteLine(ID);       
            try
            {
                int ID = await _repository.CreateNew(model);
                if(ID == 0)
                {
                    throw new Exception("Can not create the record");
                }

                var readDto = _mapper.Map<R>(model);

                return CreatedAtRoute(
                    routeName, 
                    new {ID = ID}, 
                    readDto
                );
            }
            catch (Exception e)
            {
                return BadRequest(new {e.Message});
            }
        }

        [HttpPut("{id}")]
        public virtual async Task<ActionResult> UpdateRecord(int id, W update)
        {
            string message = "";
            var model = await _repository.GetByID(id);
            if(model == null)
            {
                message = "Can not find the object";
                return NotFound(new {message});
            }

            M temp = _mapper.Map<M>(update);
            bool isExist = await _repository.FindExist(temp);
            if (!isExist)
            {
                message = "The item is not exist";
                return BadRequest(new {message});
            }

            _mapper.Map(update, model);
            try
            {
                _repository.UpdateOld(model);
                await _repository.SaveChanges();
            }
            catch (SqlException e)
            {
                return BadRequest(e.Message);
            }
            message = "Record is updated";
            return Ok(new {message});
        }
        [HttpPatch("{id}")]
        public async Task<ActionResult> PartialUpdate(int id, JsonPatchDocument<U> patch)
        {
            var model = await _repository.GetByID(id);

            if(model == null)
            {
                return NotFound();
            }

            var patchModel = _mapper.Map<U>(model);
            patch.ApplyTo(patchModel, ModelState);

            if(!TryValidateModel(patchModel))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(patchModel, model);

            try
            {
                _repository.UpdateOld(model);
                await _repository.SaveChanges();
            }
            catch (SqlException e)
            {
                string message = "";
                switch (e.Number)
                {
                    case 2601:
                        message = "Duplicate value";
                        break;
                    default:
                        break;
                }
                return BadRequest(message);
            }

            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCommand(int id)
        {
            var model = await _repository.GetByID(id);

            if(model == null)
            {
                return NotFound();
            }

            _repository.DeleteOld(model);

            await _repository.SaveChanges();

            return Ok();
        }
    }
}