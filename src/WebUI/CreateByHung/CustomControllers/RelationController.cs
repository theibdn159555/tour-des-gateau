using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Paginations;

namespace WebUI.CreateByHung.CustomControllers
{
    public class RelationController<M, R, W, U> : ControllerBase
    where M : class
    where R : class
    where W : class
    where U : class
    {
        protected readonly IRelationshipRepo<M> _repository;
        protected readonly IMapper _mapper;

        public RelationController(IRelationshipRepo<M> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        protected ActionResult<IEnumerable<R>> FindOrNot(int page, IEnumerable<M> lists, int limit)
        {
            if(lists.Count() == 0)
            {
                return NotFound();
            }
            return GetPerPage(page, lists, limit);
        }
        protected ActionResult<R> FindOrNot(M item)
        {
            if(item == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<R>(item));
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<R>>> GetAll()
        {
            var readLists = await _repository.GetAll(6);
            if(readLists.Count() == 0) return NotFound();
            return Ok(_mapper.Map<IEnumerable<R>>(readLists));
        }
        protected ActionResult<IEnumerable<R>> GetPerPage(int page, IEnumerable<M> items, int limit = 6)
        {
            PaginationResultSet<R, M> paginationResultSet = new PaginationResultSet<R, M>(page, items, _mapper);
            
            try
            {
                paginationResultSet.GetPagination(limit);
            }
            catch (ObjectNotFoundException)
            {
                return NotFound();
            }

            return Ok(
                new {
                    paginationResultSet.Metadata,
                    paginationResultSet.MappingResults
                }
            );
        }
        [HttpGet]
        [HttpGet("page-{page}")]
        public virtual async Task<ActionResult<IEnumerable<R>>> GetAllPerPage(int page)
        {
            var items = await _repository.GetAll(6);
            return this.GetPerPage(page, items);
        }
        protected virtual async Task<ActionResult<IEnumerable<R>>> GetItemByID(int id, int page, int limit)
        {
            var lists = await _repository.GetByID(id);
            return FindOrNot(page, lists, limit);
        }
        protected virtual async Task<ActionResult<R>> GetItemByID(int firstId, int secondId)
        {
            var item = await _repository.GetByID(firstId, secondId);
            return FindOrNot(item);
        }
        // [HttpGet("filter/{search}")]
        public virtual async Task<ActionResult<IEnumerable<R>>> Filter(
            int targetId,
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to, 
            int? page,
            int? limit
        )
        {
            IEnumerable<M> lists = await _repository.Filter(targetId, orderByDate, year, month, day, from, to);
            
            if(lists.Count() == 0) return NotFound();

            int pageNumber = 1;
            if(page != null)
            {
                pageNumber = (int)page;
            }

            int limitItem = limit == null ? 6 : (int) limit;
            
            return GetPerPage(pageNumber, lists, limitItem);
        }
        protected virtual async Task<ActionResult<W>> CreateRecord(W writeDto, string routeName)
        {
            var model = _mapper.Map<M>(writeDto);
            try
            {
                int id = await _repository.CreateNew(model);
                if(id == 0)
                {
                    throw new Exception("Can not create the record");
                }
                var readDto = _mapper.Map<R>(model);
                
                // bool create = false; if(create is false) return BadRequest();

                return CreatedAtRoute(
                    routeName,
                    new {id = id},
                    readDto
                );
            }
            catch (Exception e)
            {
                return BadRequest(new {e.Message});
            }
        }
        // [HttpDelete("user={accountId}/product={productId}")]
        protected virtual async Task<ActionResult> DeleteRecord(int fId, int sId)
        {
            var model = await _repository.GetByID(fId, sId);
            if(model == null)
            {
                return NotFound();
            }
            try
            {
                _repository.DeleteOld(model);
                await _repository.SaveChanges();
            }
            catch (Exception e)
            {
                if(e is ObjectNotFoundException)
                {
                    return NotFound(e.Message);
                }
                return BadRequest(e.Message);
            }
            return Ok();
        }
        // [HttpDelete("user={accountId}/product={productId}")]
        protected virtual async Task<ActionResult> UpdateRecord(int fId, int sId, U update)
        {
            var model = await _repository.GetByID(fId, sId);
            if(model == null)
            {
                return NotFound();
            }
            if(await _repository.IsAbleToInsert(model) is false)
            {
                return BadRequest();
            }
            _mapper.Map(update, model);
            try
            {
                _repository.UpdateOld(model);
                await _repository.SaveChanges();
            }
            catch (SqlException e)
            {
                return BadRequest(e.Message);
            }
            return Ok("Record is updated");
        }
        
    }
}