namespace WebUI.CreateByHung.Paginations
{
    public class PaginationModel
    {
        public PaginationModel()
        {
            
        }
        public PaginationModel(int page, int limit = 6)
        {
            Page = page;
            Limit = limit;
        }
        private int page;
        public int Page
        {
            get { 
                if(page == 0)
                {
                    return 1;
                }
                return page;
            }
            set { page = value; }
        }
        
        private int limit = 6;
        public int Limit
        {
            get { return limit; }
            set { limit = value; }
        }
        public int TotalRows { get; set; }
        
    }
}