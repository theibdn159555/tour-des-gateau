using System.Collections.Generic;

namespace WebUI.CreateByHung.Paginations
{
    public class PaginationRepo<M>
    {
        private PaginationMetadata metadata;

        public PaginationMetadata Metadata {get{
            return metadata;
        }}
        private IEnumerable<M> list;
        public IEnumerable<M> List {get{
            return list;
        }}

        public PaginationRepo(PaginationMetadata metadata, IEnumerable<M> list)
        {
            this.metadata = metadata;
            this.list = list;
        }
    }
}