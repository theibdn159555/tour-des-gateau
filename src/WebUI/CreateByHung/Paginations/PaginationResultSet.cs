using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WebUI.CreateByHung.Exceptions;

namespace WebUI.CreateByHung.Paginations
{
    public class PaginationResultSet<T, M>
    {
        public PaginationResultSet()
        {
            
        }
        public PaginationResultSet(int page, IEnumerable<M> lists, IMapper mapper)
        {
            this.Page = page;
            this.Lists = lists;
            _mapper = mapper;
        }
        public PaginationResultSet(PaginationMetadata metadata, IEnumerable<T> mappingResults)
        {
            this.Metadata = metadata;
            this.MappingResults = mappingResults;
        }
            
        public int Page { get; set; }
        public IEnumerable<M> Lists { get; set; }

        private readonly IMapper _mapper;

        public PaginationMetadata Metadata { get; set; }
        public IEnumerable<T> MappingResults { get; set; }

        public PaginationResultSet<T, M> GetPagination(int limit = 6){
            PaginationModel paging = new PaginationModel(this.Page, limit);
            var itemsPerPage = this.Lists.Skip((paging.Page - 1 ) * paging.Limit).Take(paging.Limit).ToList();

            var paginationMetadata = new PaginationMetadata(paging.Page, this.Lists.Count(), paging.Limit);
            if(paginationMetadata.CurrentPage > paginationMetadata.TotalPages)
            {
                throw new ObjectNotFoundException();
            }

            var mapingList = _mapper.Map<IEnumerable<T>>(itemsPerPage);

            this.Metadata = paginationMetadata;
            this.MappingResults = mapingList;
            
            return new PaginationResultSet<T, M>()
            {
                Metadata = paginationMetadata,
                MappingResults = mapingList
            };
        }
    }
}