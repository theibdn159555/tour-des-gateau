using System;

namespace WebUI.CreateByHung.Exceptions
{
    public class ObjectExistedException : Exception
    {
        public ObjectExistedException() : base()
        {
            
        }
        public ObjectExistedException(string message) : base(message)
        {
            
        }
    }
}