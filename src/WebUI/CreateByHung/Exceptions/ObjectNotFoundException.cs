using System;

namespace WebUI.CreateByHung.Exceptions
{
    public class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException() : base()
        {
            
        }
        public ObjectNotFoundException(string message) : base(message)
        {
            
        }
    }
}