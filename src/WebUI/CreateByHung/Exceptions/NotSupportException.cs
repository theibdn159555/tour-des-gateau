using System;

namespace WebUI.CreateByHung.Exceptions
{
    public class NotSupportException : Exception
    {
        public NotSupportException() : base() { }
        public NotSupportException(string message) : base(message) { }
    }
}