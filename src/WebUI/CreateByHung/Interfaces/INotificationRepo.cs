using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.CreateByHung.Interfaces
{
    public interface INotificationRepo<T>
    {
        Task<bool> SaveChanges();
        Task<bool> FindExist(T target);
        Task<T> FindExist(int target);
        Task<IEnumerable<T>> Search(string name);
        Task<IEnumerable<T>> GetByID(int id);
        Task<bool> CreateNew(int userId);
        void UpdateOld(T target);
        void DeleteOld(T target);
    }
}