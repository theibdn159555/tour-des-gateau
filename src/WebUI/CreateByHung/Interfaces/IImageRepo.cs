using System.Threading.Tasks;
using WebUI.CreateByHung.DTOs.ImageDTOs;
using WebUI.Models;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IImageRepo
    {
        Task<string> SaveProductImage(ImageUploadDTO uploadDTO);
        Task<int> SaveAccountAvatar(ImageUploadDTO uploadDTO, string host);
        Task<byte[]> GetProductImage(string name);
        Task<byte[]> GetAccountAvatar(int accountId);
    }
}