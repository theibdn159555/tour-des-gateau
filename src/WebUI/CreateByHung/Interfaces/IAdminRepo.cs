using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Paginations;
using WebUI.Models;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IAdminRepo
    {
        Task<bool> SaveChanges();
        void ApplyEventAll(int id);
        Task<OrderRevenueDTO> GetRevenue(int? year, int? month);
        Task<StatisticProductDTO> GetQuantitiesPerMonth(int id, int year);
        Task<StatisticProductDTO> GetTotalSoldQuantitiesOfProduct(int productId);
        Task<IEnumerable<DiscountEvent>> GetNearestEvents();
        Task<int> GetProductTotalFollowers(int productId);
        Task<PaginationRepo<Order>> StatusPagination(string status, PaginationModel paging, bool orderByOlder = true);
        Task<List<StatisticProductDTO>> GetTopSoldProductsByMonth(int year, int month, int top = 3);
        Task<OrderRevenueDTO> GetStatisticOrdersByMonth(int year, int month, string status);
    }
}