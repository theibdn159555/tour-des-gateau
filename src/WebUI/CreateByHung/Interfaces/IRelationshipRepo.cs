using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IRelationshipRepo<T>
    {
        Task<bool> SaveChanges();
        Task<bool> IsAvailable(int id, EntityEnum entity);
        Task<bool> IsAbleToInsert(T target);
        Task<bool> FindExist(T target);
        Task<IEnumerable<T>> Filter(
            int id,
            bool? orderByCreatedAt, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to
        );
        Task<IEnumerable<T>> GetAll(int? limit);
        Task<IEnumerable<T>> GetByID(int id);
        Task<T> GetByID(int firstId, int secondId);
        Task<T> GetByID(int id, ICollection<int> groupIds);
        Task<int> CreateNew(T target);
        void UpdateOld(T target);
        void DeleteOld(T target);
        // Task<PaginationRepo<T>> GetPagination(int id, PaginationModel paging);
    }
}