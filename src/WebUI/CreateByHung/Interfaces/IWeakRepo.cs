using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IWeakRepo<T>
    {
        Task<bool> SaveChanges();
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetListByID(int ownerId);
        Task<T> GetExactlyByID(int ownerId, int target);
        Task<int> CreateNew(T target);
        void UpdateOld(T target);
        void DeleteOld(T target);
        
    }
}