using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebUI.Models;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IViolationRepo
    {
        Task<bool> SaveChanges();
        Task<bool> IsUserExist(int userId);
        Task<bool> FindExist(Violation target);
        Task<IEnumerable<Violation>> Filter(
            int? warningId,
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to
        );
        Task<IEnumerable<Violation>> GetAll();
        Task<IEnumerable<Violation>> GetByID(int userId);
        Task<Violation> GetByID(int userId, int warningId);
        Task<int> CreateNew(Violation target);
        void UpdateOld(Violation target);
        void DeleteOld(Violation target);
    }
}