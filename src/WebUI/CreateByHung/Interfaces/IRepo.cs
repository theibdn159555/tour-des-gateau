using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.Paginations;
using WebUI.Models;

namespace WebUI.CreateByHung.Interfaces
{
    public interface IRepo<T>
    {
        Task<bool> SaveChanges();
        Task<bool> FindExist(string target);
        Task<bool> FindExist(T target);
        Task<IEnumerable<T>> Search(string name);
        Task<IEnumerable<T>> GetAll();
        Task<T> GetByID(int id);
        Task<int> CreateNew(T target);
        void UpdateOld(T target);
        void DeleteOld(T target);

    }
}