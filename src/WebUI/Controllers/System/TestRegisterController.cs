using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebUI.DTO.System;
using WebUI.Data;
using WebUI.Models;

using BC = BCrypt.Net.BCrypt;

namespace WebUI.Controllers.System
{
    [Route("api/TestRegisterController")]
    [ApiController]
    public class TestRegisterController : ControllerBase
    {
        private readonly db_a7c7da_phunghung123Context _context;
        private readonly IMapper _mapper;

        public TestRegisterController(db_a7c7da_phunghung123Context context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<RegisterDTO>> Index(RegisterDTO dto)
        {
            var model = _mapper.Map<Account>(dto);

            try
            {
                //check agree
                if (!dto.IsAgree) return UnprocessableEntity(new { error = "Agree Policy is invalid" });

                //check confirm password
                if (dto.Password != dto.ConfirmPassword) return UnprocessableEntity(new { error = "Confirm password is invalid" });

                //check exist 
                if (AccountExists(dto.Username)) return UnprocessableEntity(new { error = "Username is invalid" });

                //insert
                if (!await CreateAccount(model, dto.Password)) return BadRequest(new { error = "Create Fail" });

                return Ok(new
                {
                    message = "success",
                    dto,
                });

            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private bool AccountExists(string username)
        {
            return _context.Accounts.Any(e => e.Username == username);
        }

        private async Task<bool> CreateAccount(Account account, String Password)
        {
            account.PasswordHash = BC.HashPassword(Password);
            _context.Accounts.Add(account);

            return (await _context.SaveChangesAsync() >= 0);
        }
    }
}

// var account = new Account
// {
//     Username = request.Username,
//     PasswordHash = BC.HashPassword(request.Password),
//     Email = request.Email
// };

// await _context.Accounts.AddAsync(account);
// var created = await _context.SaveChangesAsync();