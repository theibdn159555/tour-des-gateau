using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebUI.Controllers.System.Filters
{
    public class DebugResourceFilter : Attribute, IResourceFilter
    {
        public void OnResourceExecuted(ResourceExecutedContext context)
        {
            Console.WriteLine($"[Resource Filter] {context.HttpContext.Request.Path} is executed");
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            Console.WriteLine($"[Resource Filter] {context.HttpContext.Request.Path} is executing...");
        }
    }
}