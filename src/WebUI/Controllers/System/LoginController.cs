﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebUI.DTO.System;
using WebUI.Data;
using WebUI.Models;

using BC = BCrypt.Net.BCrypt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace WebUI.Controllers.System
{
    [Route("api/Login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly db_a7c7da_phunghung123Context _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public LoginController(db_a7c7da_phunghung123Context context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _config = configuration;
        }

        [HttpPost]
        public async Task<ActionResult<LoginDTO>> Index(LoginDTO dto)
        {
            var accountModel = await _context.Accounts
                .FirstOrDefaultAsync(item => item.Username == dto.Username);

            if (accountModel == null) return UnprocessableEntity(new { error = "Account is invalid" });

            if (!BC.Verify(dto.Password, accountModel.PasswordHash)) return BadRequest(new { error = "Password is invalid", result = BC.Verify(dto.Password, BC.HashPassword(dto.Password)) });

            var token = generateJwtToken(accountModel);

            var account = _mapper.Map<AccountDTO>(accountModel);

            return Ok(new
            {
                message = "success",
                token,
                account,
                expires_in = 60 * 60 * 24 * 7
            });
        }

        private string generateJwtToken(Account account)
        {
            var key = _config["Key"];
            var issuer = _config["Issuer"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("AccountId", account.AccountId.ToString()));
            permClaims.Add(new Claim("PermissionId", account.PermissionId.ToString()));

            var token = new JwtSecurityToken(
                            issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_token;
        }
    }
}

// var account = new Account
// {
//     Username = request.Username,
//     PasswordHash = BC.HashPassword(request.Password),
//     Email = request.Email
// };

// await _context.Accounts.AddAsync(account);
// var created = await _context.SaveChangesAsync();