﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebUI.DTO.System;
using WebUI.Data;
using WebUI.Models;

using BC = BCrypt.Net.BCrypt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace WebUI.Controllers.System
{
    [Route("api/ForgotController")]
    [ApiController]
    public class ForgotController : ControllerBase
    {
        private readonly db_a7c7da_phunghung123Context _context;
        private readonly IMapper _mapper;

        public ForgotController(db_a7c7da_phunghung123Context context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<ForgotDTO>> Index(ForgotDTO dto)
        {
            var model = _mapper.Map<Account>(dto);

            if (dto.Password != dto.ConfirmPassword) return UnprocessableEntity(new { error = "Confirm password is invalid" });

            if (!matchFields(model)) return UnprocessableEntity(new { error = "Username and password aren't matching" });

            if (!await UpdateAccount(model, dto.Password)) return BadRequest(new { error = "Update Fail" });

            return Ok(new
            {
                message = "success",
                dto,
            });
        }

        private bool matchFields(Account account)
        {
            return _context.Accounts.Any(e => e.Username == account.Username && e.Email == account.Email);
        }

        private async Task<bool> UpdateAccount(Account account, string Password)
        {
            account.PasswordHash = BC.HashPassword(Password);

            _context.Accounts.Update(account);
            var updated = await _context.SaveChangesAsync();

            return updated > 0;
        }

    }
}
