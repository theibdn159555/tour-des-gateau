using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.ImageDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes;

namespace WebUI.Controllers.Image
{
    [Route("api/images")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private const string ImageContentType = "image/png";
        private readonly IImageRepo _repo;

        public ImageController(IImageRepo repo)
        {
            _repo = repo;
        }
        [HttpPost("products")]
        public async Task<ActionResult> CreateProductImage([FromForm] ImageUploadDTO uploadDTO)
        {
            Console.WriteLine("int create product image" + DateTime.Now);
            await Task.Delay(2000);
            uploadDTO.EntityType = CreateByHung.Others.EntityEnum.Product;
            try
            {
                string name = await _repo.SaveProductImage(uploadDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }
  
        [HttpGet("products/product={name}", Name = "GetProductImage")]
        public async Task<ActionResult> GetProductImage(string name)
        {
            try
            {
                var image = await _repo.GetProductImage(name);
                return File(image, ImageContentType);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("accounts")]
        public async Task<ActionResult> CreateAvatar([FromForm] ImageUploadDTO uploadDTO)
        {
            Console.WriteLine("int create account image" + DateTime.Now);
            await Task.Delay(2000);
            uploadDTO.EntityType = CreateByHung.Others.EntityEnum.Account;
            try
            {
                string host = Request.Host.ToString();
                var name = await _repo.SaveAccountAvatar(uploadDTO, host);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpGet("account/account={accountId}", Name = "GetAccountImage")]
        public async Task<ActionResult> GetAccountImage(int accountId)
        {
            try
            {
                var image = await _repo.GetAccountAvatar(accountId);
                return File(image, ImageContentType);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}