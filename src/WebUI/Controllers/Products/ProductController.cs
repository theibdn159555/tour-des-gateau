﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Exceptions;

namespace WebUI.Controllers.Products
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : StrongController<Product, ProductReadDTO, ProductWriteDTO, ProductUpdateDTO>
    {
        public ProductController(IRepo<Product> repository, IMapper mapper) : base (repository, mapper)
        {
            
        }
        [HttpGet("{id}", Name = "GetProductByID")]
        public async Task<ActionResult<ProductReadDTO>> GetProductByID(int id)
        {
            return await base.GetItemByID(id);
        }
        // [HttpGet("filter/")]
        [HttpPost("filter/page={page}")]
        public async Task<ActionResult<IEnumerable<ProductReadDTO>>> FilterProducts(FilterProductDTO filterProductDTO, int page)
        {
            var items = await ((SqlProductRepo)_repository).Filter(
                filterProductDTO.Categories, 
                filterProductDTO.SizeId,
                filterProductDTO.PriceRange, 
                filterProductDTO.OrderByName
            );
            if(items == null){
                return NotFound();
            }
            
            return base.GetPerPage(page, items, null);
        }
        [HttpGet("tag={id}")]
        [HttpGet("tag={id}/page={page}")]
        public async Task<ActionResult<IEnumerable<ProductReadDTO>>> GetProductsByTag(int id, int page, [FromQuery] int? limit)
        {
            var items = await ((SqlProductRepo)_repository).GetProductsByTag(id);
            if(items == null){
                return NotFound();
            }           
            
            return base.GetPerPage(page, items, limit);
        }
        [HttpPost]
        public async Task<ActionResult<ProductWriteDTO>> CreateRecord(ProductWriteDTO writeDto)
        {
            writeDto.Image = "https://" + Request.Host + "/api/images/products/product=" + writeDto.ProductName;

            return await base.CreateRecord(writeDto, nameof(GetProductByID));
        }
    }
}
