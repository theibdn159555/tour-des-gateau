using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.TagDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;

namespace WebUI.Controllers.Products
{
    [Route("api/tags")]
    [ApiController]
    public class TagController : StrongController<Tag, TagReadDTO, TagWriteDTO, TagUpdateDTO>
    {
        public TagController(IRepo<Tag> repository, IMapper mapper) : base (repository, mapper)
        {
            
        }
        [HttpGet("{id}", Name = "GetTagByID")]
        public async Task<ActionResult<TagReadDTO>> GetTagByID(int id)
        {
            return await base.GetItemByID(id);
        }
        [HttpPost]
        public async Task<ActionResult<TagWriteDTO>> CreateRecord(TagWriteDTO writeDto)
        {
            return await base.CreateRecord(writeDto, nameof(GetTagByID));
        }
    }
}