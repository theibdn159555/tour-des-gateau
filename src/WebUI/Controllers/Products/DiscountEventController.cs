using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs;
using WebUI.CreateByHung.DTOs.DiscountEventDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes.StrongRepoes;
using WebUI.Models;

namespace WebUI.Controllers.Products
{
    [Route("api/discountevents")]
    [ApiController]
    public class DiscountEventController : StrongController<DiscountEvent, DiscountEventReadDTO, DiscountEventWriteDTO, DiscountEventUpdateDTO>
    {
        public DiscountEventController(IRepo<DiscountEvent> repository, IMapper mapper) : base(repository, mapper)
        {

        }
        [HttpGet("{id}", Name = "GetDiscountEventByID")]
        public async Task<ActionResult<DiscountEventReadDTO>> GetDiscountEventByID(int id)
        {
            return await base.GetItemByID(id);
        }
        [HttpPost]
        public async Task<ActionResult<DiscountEventWriteDTO>> CreateRecord(DiscountEventWriteDTO writeDto)
        {
            return await base.CreateRecord(writeDto, nameof(GetDiscountEventByID));
        }
        
    }
}

// api/discountevents/page-{page} ( get all )
// api/discountevents/{id}