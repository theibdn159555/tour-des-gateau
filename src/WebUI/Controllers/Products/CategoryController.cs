using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;
using AutoMapper;
using WebUI.CreateByHung.DTOs.CategoryDTOs;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.Others;
using System.Threading.Tasks;
using System;

namespace WebUI.Controllers.Products
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : StrongController<Category, CategoryReadDTO, CategoryWriteDTO, CategoryUpdateDTO>
    {
        public CategoryController(IRepo<Category> repository, IMapper mapper) : base(repository, mapper)
        {

        }
        [HttpGet("{id}", Name = "GetCategoryByID")]
        public async Task<ActionResult<CategoryReadDTO>> GetCategoryByID(int id)
        {
            string host = Request.Host.ToString();
            Console.WriteLine(host);
            return await base.GetItemByID(id);
        }
        [HttpPost]
        public async Task<ActionResult<CategoryWriteDTO>> CreateRecord(CategoryWriteDTO writeDto)
        {
            return await base.CreateRecord(writeDto, nameof(GetCategoryByID));
        }
    }
}
