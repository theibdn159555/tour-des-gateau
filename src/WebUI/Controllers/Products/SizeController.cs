using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.SizeDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.Models;

namespace WebUI.Controllers.Products
{
    [Route("api/sizes")]
    [ApiController]
    public class SizeController : StrongController<Size, SizeReadDTO, SizeWriteDTO, SizeUpdateDTO>
    {
        public SizeController(IRepo<Size> repository, IMapper mapper) : base (repository, mapper)
        {
            
        }
        [HttpGet("{id}", Name = "GetSizeByID")]
        public async Task<ActionResult<SizeReadDTO>> GetSizeByID(int id)
        {
            return await base.GetItemByID(id);
        }
        [HttpPost]
        public async Task<ActionResult<SizeWriteDTO>> CreateRecord(SizeWriteDTO writeDto)
        {
            return await base.CreateRecord(writeDto, nameof(GetSizeByID));
        }
    }
}