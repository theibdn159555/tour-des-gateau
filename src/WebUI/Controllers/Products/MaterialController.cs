using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.MaterialDTOs;
using WebUI.CreateByHung.IngredientDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;

namespace WebUI.Controllers.Products
{
    [Route("api/materials")]
    [ApiController]
    public class MaterialController : StrongController<Material, MaterialReadDTO, MaterialWriteDTO, MaterialUpdateDTO>
    {
        public MaterialController(IRepo<Material> repository, IMapper mapper) : base (repository, mapper)
        {
            
        }
        [HttpGet("{id}", Name = "GetMaterialByID")]
        public async Task<ActionResult<MaterialReadDTO>> GetMaterialByID(int id)
        {
            var model = await _repository.GetByID(id);
            if(model == null)
            {
                return NotFound();
            }
            var materialReadDTO = _mapper.Map<MaterialReadDTO>(model);
            foreach (var item in materialReadDTO.Ingredients)
            {
                if(item.Material != null)
                {
                    item.Material = null;
                }
            }
            return Ok(materialReadDTO);
        }
        [HttpPost]
        public async Task<ActionResult<MaterialWriteDTO>> CreateRecord(MaterialWriteDTO writeDto)
        {
            return await base.CreateRecord(writeDto, nameof(GetMaterialByID));
        }
    }
}