using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.IngredientDTOs;
using WebUI.CreateByHung.IngredientDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;

namespace WebUI.Controllers.Products
{
    [Route("api/products/ingredients")]
    [ApiController]
    public class IngredientController : RelationController<Ingredient, IngredientReadDTO, IngredientWriteDTO, IngredientUpdateDTO>
    {
        public IngredientController(IRelationshipRepo<Ingredient> repo, IMapper mapper) : base (repo, mapper)
        {
            
        }
        [HttpGet("product={id}", Name = "GetIngredientByProductID")]
        [HttpGet("product={id}/{search}")]
        public async Task<ActionResult<IEnumerable<IngredientReadDTO>>> GetIngredientByProductID(int id, int? page, int? limit)
        {
            int pageNumber = page == null ? 1 : (int) page;
            int limitItem = limit == null ? 6 : (int) limit;
            return await base.GetItemByID(id, pageNumber, limitItem);
        }
        [HttpGet("product={productId}/material={materialId}")]
        public async Task<ActionResult<IngredientReadDTO>> GetByDetailID(int productId, int materialId)
        {
            return await base.GetItemByID(productId, materialId);
        }
        [HttpGet("product={id}/filter/{search}")]
        public async Task<ActionResult<IEnumerable<IngredientReadDTO>>> FilterByID(
            int id, 
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to, 
            int? page,
            int? limit 
        )
        {
            return await base.Filter(id, orderByDate, year, month, day, from, to, page, limit);
        }
        [HttpPost]
        public Task<ActionResult<IngredientWriteDTO>> CreateNewRating(IngredientWriteDTO rating)
        {
            return base.CreateRecord(rating, nameof(GetIngredientByProductID));
        }
        [HttpDelete("product={productId}/material={materialId}")]
        public Task<ActionResult> DeleteIngredient(int productId, int materialId)
        {
            return base.DeleteRecord(productId, materialId);
        }
        [HttpPut("product={productId}/material={materialId}")]
        public Task<ActionResult> UpdateIngredient(int productId, int materialId, IngredientUpdateDTO updateDTO)
        {
            return base.UpdateRecord(productId, materialId, updateDTO);
        }
    }
}