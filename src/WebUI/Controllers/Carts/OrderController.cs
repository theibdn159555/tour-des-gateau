using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PayPal.Core;
using PayPal.v1.Payments;
using WebUI.CreateByHung.DTOs.CartDTOs;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Paginations;
using WebUI.CreateByHung.SqlQueryRepoes.WeakRepoes;
using BraintreeHttp;

namespace WebUI.Controllers.Carts
{
    [Route("api/users/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IWeakRepo<Models.Order> _repo;
        private readonly IMapper _mapper;
        private readonly string _clientId;
        private readonly string _secretKey;

        // private IEnumerable<OrderStatus> statuses;

        public OrderController(IWeakRepo<Models.Order> repo, IMapper mapper, IConfiguration config)
        {
            _repo = repo;
            _mapper = mapper;
            _clientId = config["PayPalSettings:ClientId"];
            _secretKey = config["PayPalSettings:SecretKey"];
        }

        class ExchangeRate
        {
            public DateTime Date { get; set; }
            public decimal Vnd { get; set; }
        }
        public static Task<decimal> GetExchangeRate()
        {
            string currencyApi = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/usd/vnd.json";
            WebClient client = new WebClient();
            string rawData = client.DownloadString(currencyApi);
            ExchangeRate rate = JsonConvert.DeserializeObject<ExchangeRate>(rawData);
            Console.WriteLine($"1 usd = {rate.Vnd} vnd");
            return Task.FromResult(rate.Vnd);
        }
        protected ActionResult<IEnumerable<OrderReadDTO>> GetPerPage(int page, IEnumerable<Models.Order> items)
        {
            PaginationResultSet<OrderReadDTO, Models.Order> paginationResultSet = new PaginationResultSet<OrderReadDTO, Models.Order>(page, items, _mapper);
            
            try
            {
                paginationResultSet.GetPagination();
            }
            catch (ObjectNotFoundException)
            {
                return NotFound();
            }

            return Ok(
                new {
                    paginationResultSet.Metadata,
                    paginationResultSet.MappingResults
                }
            );
        }
        private OrderReadDTO FormatDTO(OrderReadDTO readDTO)
        {
            var temp = readDTO.CartDto.DetailDto;
            var totalPrice = readDTO.CartDto.TotalPrice;
            readDTO.Cart = null;
            readDTO.CartDto.CartDetails = null;
            readDTO.CartDto.Account = null;
            readDTO.CartDto.DetailDto = temp;
            readDTO.CartDto.TotalPrice = totalPrice;
            return readDTO;
        }
        // -------------------------------------------------------------------------------
        [HttpGet("user={id}", Name = "GetListOrdersByOwner")]
        public async Task<ActionResult<IEnumerable<OrderReadDTO>>> GetListOrdersByOwner(int id)
        {
            var orders = await _repo.GetListByID(id);
            if(orders == null) return NotFound();
            var orderDtos = _mapper.Map<IEnumerable<OrderReadDTO>>(orders);
            foreach (var item in orderDtos)
            {
                var temp = item.CartDto.DetailDto;
                var totalPrice = item.CartDto.TotalPrice;
                item.Cart = null;
                item.CartDto.CartDetails = null;
                item.CartDto.Account = null;
                item.CartDto.DetailDto = temp;
                item.CartDto.TotalPrice = totalPrice;
            }
            return Ok(orderDtos);
        }
        [HttpGet("user={id}/last")]
        public async Task<ActionResult<OrderReadDTO>> GetLastOrder(int id)
        {
            try
            {
                var model = await ((SqlOrderRepo)_repo).GetLastOrder(id);
                if(model == null) return Ok(
                    new {
                        info = "No order before"
                    }
                );
                var readDTO = _mapper.Map<OrderReadDTO>(model);
                return Ok(readDTO);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        // --------------------------------------------------------------------------------
        [HttpPost]
        public async Task<ActionResult<OrderWriteDTO>> CreateNewOrder(OrderWriteDTO writeDTO)
        {
            var model = _mapper.Map<Models.Order>(writeDTO);
            int userId = 0; 
            try
            {
                userId = await _repo.CreateNew(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            var readDTO = _mapper.Map<OrderReadDTO>(model);

            readDTO = FormatDTO(readDTO);

            return CreatedAtRoute(
                nameof(GetListOrdersByOwner),
                new { id = userId },
                readDTO
            );
        }
        [HttpGet("detail/user={accountId}/order={cartId}")]
        public async Task<ActionResult<OrderReadDTO>> GetSpecifyUserOrder(int accountId, int cartId)
        {
            var model = await ((SqlOrderRepo)_repo).GetDetailExactlyByID(accountId, cartId);
            if(model == null) return NotFound();
            var readDTO = _mapper.Map<OrderReadDTO>(model);
            readDTO = FormatDTO(readDTO);
            return Ok(readDTO);
        }
        [HttpPut("update/user={accountId}/order={cartId}")]
        public async Task<ActionResult> UpdateOrderStatus(int accountId, int cartId, OrderUpdateDTO updateDTO)
        {
            var model = await _repo.GetExactlyByID(accountId, cartId);
            if(model == null) return NotFound();
            
            updateDTO.AccountId = accountId;
            updateDTO.CartId = cartId;

            updateDTO.Note = updateDTO.Note.Trim();
            if(model.StatusId == 4)
            {
                string message = "This order can not be changed";
                return BadRequest(message);
            }
            if(updateDTO.StatusId == 5 && updateDTO.Note.CompareTo(null) == 0)
            {
                string message = "If the STATUS of ORDER is CANCELLED, it will need the NOTE";
                return BadRequest(message);
            }
            _mapper.Map(updateDTO, model);
            try
            {
                _repo.UpdateOld(model);
                await _repo.SaveChanges();

                if(updateDTO.StatusId == 4)
                {
                    await ((SqlOrderRepo)_repo).UnlockMembership(accountId);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        
        [HttpGet("status={name}/user={id}")]
        public async Task<ActionResult<IEnumerable<OrderReadDTO>>> GetListOrdersByStatusUser(string name, int id)
        {
            IEnumerable<Models.Order> models = new List<Models.Order>();
            try
            {
                models = await ((SqlOrderRepo)_repo).GetOrdersOfSpecifyUserByStatus(id, name);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            if(models.Count() == 0) return NotFound();
            var readDTOs = _mapper.Map<IEnumerable<OrderReadDTO>>(models);
            foreach (var item in readDTOs)
            {
                FormatDTO(item);
            }
            return Ok(readDTOs);
        }
        [HttpGet("PayPal/user={id}")]
        public async Task<ActionResult> PaypalCheckout(int id)
        {
            // var model = _mapper.Map<Models.Order>(writeDTO);

            var cart = await ((SqlOrderRepo)_repo).GetCartAble(id);
            var cartDto = _mapper.Map<CartReadDTO>(cart);
            string invoiceId = $"{id}{cart.CartId}{DateTime.Now.Day}{DateTime.Now.Month}{DateTime.Now.Year}";
            var shipValue = SqlOrderRepo.GetShipFee(SqlOrderRepo.GetQuantityItems(cartDto.CartDetails));

            var environment = new SandboxEnvironment(_clientId, _secretKey);
            var client = new PayPalHttpClient(environment);

            #region Create Paypal Order
            var itemList = new ItemList()
            {
                Items = new List<Item>()
            };
            var exrate = await GetExchangeRate();
            exrate = Math.Round(exrate, 0);
            decimal totalPrice = 0;
            foreach (var item in cartDto.DetailDto)
            {
                var itemPrice = Math.Round((((decimal)item.FinalPrice) * 1000) / exrate, 2);
                itemList.Items.Add(new Item()
                {
                    Name = item.Product.ProductName,
                    Currency = "USD",
                    Price = itemPrice.ToString().Replace(",", "."),
                    Quantity = item.Quantity.ToString(),
                    Sku = "sku",
                    Tax = "0"
                });
                totalPrice += itemPrice * item.Quantity;
            }
            
            var shipFee = totalPrice * (decimal)shipValue;
            var finalTotal = totalPrice + shipFee;
            finalTotal = Math.Round((decimal)finalTotal, 2);
            var total = totalPrice.ToString().Replace(",", ".");
            #endregion


            var hostname = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}";
            var payment = new Payment()
            {
                Intent = "sale",
                Transactions = new List<Transaction>()
                {
                    new Transaction()
                    {
                        Amount = new Amount()
                        {
                            Total = finalTotal.ToString().Replace(",", "."),
                            Currency = "USD",
                            Details = new AmountDetails
                            {
                                Tax = "0",
                                Shipping = shipFee.ToString().Replace(",", "."),
                                Subtotal = total
                            }
                        },
                        ItemList = itemList,
                        Description = $"Invoice #{invoiceId}",
                        InvoiceNumber = invoiceId
                    }
                },
                RedirectUrls = new RedirectUrls()
                {
                    CancelUrl = $"{hostname}/api/users/orders/PayPal/CheckoutFail",
                    ReturnUrl = $"{hostname}/api/users/orders/PayPal/CheckoutSuccess"
                },
                Payer = new Payer()
                {
                    PaymentMethod = "paypal"
                }
            };

            return Ok(payment);

            // PaymentCreateRequest request = new PaymentCreateRequest();
            // request.RequestBody(payment);

            // try
            // {
            //     var response = await client.Execute(request);
            //     var statusCode = response.StatusCode;
            //     Payment result = response.Result<Payment>();

            //     var links = result.Links.GetEnumerator();
            //     string paypalRedirectUrl = null;
            //     while (links.MoveNext())
            //     {
            //         LinkDescriptionObject lnk = links.Current;
            //         if (lnk.Rel.ToLower().Trim().Equals("approval_url"))
            //         {
            //             //saving the payapalredirect URL to which user will be redirected for payment  
            //             paypalRedirectUrl = lnk.Href;
            //         }
            //     }
            //     //store Order in db
            //     // await _repo.CreateNew(model);

            //     //paypalRedirectUrl contains the link to the preview transaction
            //     return Ok(paypalRedirectUrl);
            //     // return Redirect(paypalRedirectUrl);
            // }
            // catch (HttpException httpException)
            // {
            //     var statusCode = httpException.StatusCode;
            //     var debugId = httpException.Headers.GetValues("PayPal-Debug-Id").FirstOrDefault();

            //     //Process when Checkout with Paypal fails
            //     //return Redirect("/api/users/orders/PayPal/CheckoutFail");
            //     return StatusCode(501, httpException.Message);
            // }
        }
    }
}

