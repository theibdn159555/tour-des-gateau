using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.CartDetailDTOs;
using WebUI.CreateByHung.DTOs.CartDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.SqlQueryRepoes.WeakRepoes;
using WebUI.Models;

namespace WebUI.Controllers.Carts
{
    [Route("api/users/carts")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IWeakRepo<Cart> _repo;
        private readonly IMapper _mapper;

        public CartController(IWeakRepo<Cart> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        protected IEnumerable<CartDetail> GetFinalPrices(IEnumerable<CartDetail> details)
        {
            foreach (var item in details)
            {
                if(item.Cart != null)
                {
                    item.Cart = null;
                }
            }
            return details;
        }
        [HttpGet("user={id}")]
        public async Task<ActionResult<IEnumerable<CartReadDTO>>> GetListCartByOwner(int id)
        {
            var carts = await _repo.GetListByID(id);
            if(carts == null)
            {
                return NotFound();
            }
            var cartDto = _mapper.Map<IEnumerable<CartReadDTO>>(carts);
            Account owner = new Account();
            foreach (var item in cartDto)
            {
                ICollection<DetailReadDTO> tempDetail = item.DetailDto;
                item.CartDetails = null;
                owner = item.Account;
                item.Account = null;
            }
            return Ok(new {
                owner,
                cartDto
            });
        }
        [HttpGet("user={id}/able")]
        public async Task<ActionResult<CartReadDTO>> GetAbleCartByOwner(int id)
        {
            try{
                var cart = await ((SqlCartRepo)_repo).GetAbleCartByID(id);

                if(cart == null)
                {
                    return NotFound();
                }

                var cartDto = _mapper.Map<CartReadDTO>(cart);

                var output = new CartReadDTO();
                output.AccountId = cartDto.AccountId;
                output.CartId = cartDto.CartId;
                output.DetailDto = cartDto.DetailDto;


                return Ok(output);

            }catch(Exception e){
                return BadRequest(e.Message);
            }
        }
    }
}