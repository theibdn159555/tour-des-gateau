using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.CartDetailDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.Models;

namespace WebUI.Controllers.Carts
{
    [Route("api/users/carts/details")]
    [ApiController]
    public class CartDetailController : ControllerBase
    {
        private readonly IRelationshipRepo<CartDetail> _repo;
        private readonly IMapper _mapper;

        public CartDetailController(IRelationshipRepo<CartDetail> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        [HttpPost]
        public async Task<ActionResult<DetailWriteDTO>> CreateRecord(DetailWriteDTO target)
        {
            try
            {
                var model = _mapper.Map<CartDetail>(target);
                int cartId = await _repo.CreateNew(model);
                model.CartId = cartId;
                var readDto = _mapper.Map<DetailReadDTO>(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        [HttpDelete("user={userId}/product={productId}")]
        public async Task<ActionResult> DeleteRecord(int userId, int productId)
        {
            ((SqlDetailCartRepo)_repo).DeleteOld(userId, productId);
            await _repo.SaveChanges();
            return Ok($"Delete item {productId} completed");
        }
        [HttpDelete("user={userId}/all")]
        public async Task<ActionResult> DeleteRecord(int userId)
        {
            try
            {
                ((SqlDetailCartRepo)_repo).DeleteAll(userId);
                await _repo.SaveChanges();                 
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok("Delete all items completed");
        }
        [HttpDelete("user={userId}/list")]
        public async Task<ActionResult> DeleteRecord(int userId, IEnumerable<DetailWriteDTO> targets)
        {
            try
            {
                if(targets == null) throw new Exception("No items to delete");
                foreach (var item in targets)
                {
                    item.AccountId = userId;
                }
                var models = _mapper.Map<IEnumerable<CartDetail>>(targets);
                ((SqlDetailCartRepo)_repo).DeleteList(userId, models);
                await _repo.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok($"Delete {targets.Count()} items completed");
        }
        [HttpPut("user={userId}")]
        public async Task<ActionResult> UpdateRecord(int userId, IEnumerable<DetailUpdateDTO> items)
        {
            var cart = ((SqlDetailCartRepo)_repo).GetOpenCart(userId);
            foreach (var item in items)
            {
                item.AccountId = userId;
                item.CartId = cart.CartId;
            }
            var models = await _repo.GetByID(userId);
            foreach (var item in items)
            {
                foreach (var item2 in models)
                {
                    if(item2.ProductId == item.ProductId)
                    {
                        _mapper.Map(item, item2);
                    }
                }
            }
            
            try
            {
                // foreach (var item in models)
                // {
                //     _repo.UpdateOld(item);
                // }
                await _repo.SaveChanges();
            }
            catch (Exception w)
            {
                return BadRequest(w.Message);
            }
            return Ok();
        }
        [HttpPut("user={userId}/single")]
        public async Task<ActionResult> UpdateRecord(int userId, DetailUpdateDTO item){
            var models = await _repo.GetByID(userId);
            foreach (var itemModel in models)
            {
                if(itemModel.ProductId == item.ProductId)
                {
                    itemModel.Quantity = item.Quantity;
                    break;
                }
            }
            try
            {
                await _repo.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
    }
}