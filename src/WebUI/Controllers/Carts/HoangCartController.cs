﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebUI.Data;


namespace WebUI.Controllers.Carts
{
    public class HoangCartController : Controller
    {
        private readonly db_a7c7da_phunghung123Context _context;

        public HoangCartController(db_a7c7da_phunghung123Context context)
        {
            _context = context;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var identity = User.Identity as ClaimsIdentity;
            // return Ok(new { identity });

            if (identity == null) return Ok(new { a = "tạch" });

            IEnumerable<Claim> claims = identity.Claims;
            var dataOfCarts = await _context.Carts.ToListAsync();

            return Ok(new { dataOfCarts });

        }

        [HttpPost]
        public async Task<IActionResult> Add()
        {
            var dataOfCarts = await _context.Carts.ToListAsync();
            return Ok(new { dataOfCarts });
        }

        [HttpPost]
        public async Task<IActionResult> Insert()
        {
            var dataOfCarts = await _context.Carts.ToListAsync();
            return Ok(new { dataOfCarts });
        }

        [HttpPut]
        public async Task<IActionResult> Update()
        {
            var dataOfCarts = await _context.Carts.ToListAsync();
            return Ok(new { dataOfCarts });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            var dataOfCarts = await _context.Carts.ToListAsync();
            return Ok(new { dataOfCarts });
        }
    }
}

// if (identity != null)
// {
//     IEnumerable<Claim> claims = identity.Claims;
//     var dataOfCarts = await _context.Carts.ToListAsync();
//     return Ok(new { dataOfCarts });
// }

// return Ok(new { a = "tạch" });
