using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.Controllers.System.Filters;
using WebUI.CreateByHung.DTOs.DiscountEventDTOs;
using WebUI.CreateByHung.DTOs.Orders;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;
using WebUI.CreateByHung.SqlQueryRepoes.AdminRepoes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using WebUI.Data;
using WebUI.CreateByHung.DTOs.AdminDTOs;
using WebUI.Models;
// using System.Security.Claims;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers.Admin
{
    [Route("api/admin")]
    [ApiController]
    [DebugResourceFilter]
    public class AdminController : ControllerBase
    {
        public class Meta
        {
            public Meta(int currentPage, int totalRows, int limit)
            {
                CurrentPage = currentPage == 0 ? 1 : currentPage;
                TotalRows = totalRows;
                Limit = limit;
                TotalPages = (int)Math.Ceiling(totalRows / (double)limit);
            }
            public int CurrentPage { get; set; }
            public int Limit { get; set; }
            public int TotalRows { get; set; }
            public int TotalPages { get; set; }
            public bool HasPrevious { get { return CurrentPage > 1; } }
            public bool HasNext { get { return CurrentPage < TotalPages; } }
        }

        public class Pagination
        {
            public Meta Meta { get; set; }
            public IEnumerable<Account> Contents { get; set; }
            public static Pagination PackingGoods(int page, int total, int limit, IEnumerable<Account> contents)
            {
                var pagination = new Meta(page, total, limit);
                return new Pagination()
                {
                    Meta = pagination,
                    Contents = contents
                };
            }
        }

        private readonly IAdminRepo _repo;
        private readonly IMapper _mapper;
        private readonly db_a7c7da_phunghung123Context _context;

        public AdminController(IAdminRepo repo, IMapper mapper, db_a7c7da_phunghung123Context context)
        {
            _repo = repo;
            _mapper = mapper;
            _context = context;
        }

        [Authorize]
        
        public int Authorization()
        {
            var identity = User.Identity as ClaimsIdentity;

            if (identity == null)  return 0;

            IEnumerable<Claim> claims = identity.Claims;
            var permission = claims.Where(p => p.Type == "PermissionId").FirstOrDefault() ? .Value;
        
            return int.Parse(permission) ;
        }

        [Authorize]
        [HttpGet("user/get")]
        public async Task<IActionResult> Get()
        {
            var identity = User.Identity as ClaimsIdentity;

            if (identity == null)  return BadRequest();

            IEnumerable<Claim> claims = identity.Claims;
            var AccountId = claims.Where(p => p.Type == "AccountId").FirstOrDefault() ? .Value;

            var accountModel = await _context.Accounts
                .FirstOrDefaultAsync(item => item.AccountId == int.Parse(AccountId));

            if (accountModel == null) return UnprocessableEntity(new { error = "Account is invalid" });

            var violationsModel = await _context.Violations
                .FirstOrDefaultAsync(item => item.AccountId == int.Parse(AccountId));

            // if (violationsModel == null) return UnprocessableEntity(new { error = "Violation - Account is invalid" });

        
            return Ok(new { accountModel, violationsModel }) ;   
        }

        [Authorize]
        [HttpGet("user/get-user")]
        public async Task<IActionResult> GetUser([FromQuery] int? id)
        {
            var identity = User.Identity as ClaimsIdentity;

            if (identity == null)  return BadRequest();

            IEnumerable<Claim> claims = identity.Claims;
            var AccountId = claims.Where(p => p.Type == "AccountId").FirstOrDefault() ? .Value;

            var accountModel = await _context.Accounts
                .FirstOrDefaultAsync(item => item.AccountId == id);

            if (accountModel == null) return UnprocessableEntity(new { error = "Account is invalid" });

            var violationsModel = await _context.Violations
                .FirstOrDefaultAsync(item => item.AccountId == id);

            // if (violationsModel == null) return UnprocessableEntity(new { error = "Violation - Account is invalid" });

        
            return Ok(new { accountModel, violationsModel }) ;
        }
 
        [Authorize]
        [HttpGet("user/all")]
        public async Task<IActionResult> GetAll([FromQuery] int? page, [FromQuery] int? limit, [FromQuery] string type )
        {
            int permission = Authorization();
            if(permission == 0) return Unauthorized(new {error = "UnAuthorized"});
            int permissionId = 0;
            if(type.Equals("user"))
            {
                permissionId = 4;
            }
            var total = await _context.Accounts
            .Where(
                item => ( permissionId == 0 && item.PermissionId != 4 ) || 
                ( permissionId == 4 && item.PermissionId == 4 )
            ).CountAsync();
            var users =  await _context.Accounts
            .OrderBy(s => s.AccountId)
            .Where(
                item => ( permissionId == 0 && item.PermissionId != 4 ) || 
                ( permissionId == 4 && item.PermissionId == 4 )
            )
            .Include(v => v.Violations)
            .Skip((page.Value - 1) * limit.Value)
            .Take(limit.Value)
            .Select(item => new Account{
                AccountId = item.AccountId, 
                PermissionId = item.PermissionId,
                Username = item.Username,
                Email = item.Email,
                Phone = item.Phone,
                Membership = item.Membership,
                Violations = item.Violations
                
            }).ToListAsync();
            var pagination = Pagination.PackingGoods(page.Value, total, limit.Value, users);
            return Ok(new { permission, pagination });
        }

        
        [Authorize]
        [HttpPatch("update/user")]
        public async Task<IActionResult> UpdateAccount(AdminUpdateDTO dto)
        {
            int permission = Authorization();
            if(permission == 0) return Ok(new {error = "UnAuthorized", permission});

            var accounts = await _context.Accounts
                .FirstOrDefaultAsync(item => item.AccountId == dto.AccountId);

            // var model = _mapper.Map<Account>(dto);

            int orderPermission = accounts.PermissionId;

            var result = -1;

            result = ( permission == 1) ? await adminAction(permission, orderPermission, dto.newPermission, accounts) : result ;
            result = ( permission == 2) ? await managerAction(permission, orderPermission, dto.newPermission, accounts) : result ;
            // result = ( permission == 3) ? await viceAction(permission, orderPermission, dto.newPermission, accounts) : result ;
            // result = ( permission == 4) ? await userAction(permission, orderPermission, dto.newPermission, accounts) : result ;
            // result = ( permission == 5) ? await ghostAction(permission, orderPermission, dto.newPermission, accounts) : result ;

            if( result == -1 ) return BadRequest("Action failed");

            return Ok(new { permission, result, accounts });
        }

        [Authorize]
        [HttpPatch("update/role")]
        public async Task<IActionResult> UpdateRole(AdminUpdateDTO dto)
        {
            int permission = Authorization();
            if(permission == 0) return Ok(new {error = "UnAuthorized", permission});

            var accounts = await _context.Accounts
                .FirstOrDefaultAsync(item => item.AccountId == dto.AccountId);

            int orderPermission = accounts.PermissionId;

            var result = -1;

            result = ( permission >= 1 || permission <= 3 ) ? await ghostAction(permission, orderPermission, dto.newPermission, accounts) : result ;

            if( result == -1 ) return BadRequest("Action failed");

            return Ok(new { message = "Success" });
        }

        public async Task<int> adminAction(int permission, int orderPermission, int newPermission, Account account){
            if( permission > orderPermission ) return -1;
            if( permission > newPermission ) return -1;
            if( newPermission >= 6 || newPermission <= 0 ) return -1;

            account.PermissionId = newPermission;

            var updated = await _context.SaveChangesAsync();

            return orderPermission;   
        }

        public async Task<int> managerAction(int permission, int orderPermission, int newPermission, Account account){
            if( permission > orderPermission ) return -1;
            if( permission > newPermission ) return -1;
            if( newPermission >= 6 || newPermission <= 3 ) return -1;

            account.PermissionId = newPermission;

            var updated = await _context.SaveChangesAsync();

            return orderPermission;
        }

        public async Task<int> ghostAction(int permission, int orderPermission, int newPermission, Account account){
            if( permission > orderPermission ) return -1;
            Console.WriteLine("không đc xóa người có quyền hơn mình");
            if( permission > newPermission ) return -1;
            Console.WriteLine("quyền mới của ngkhac ko đc lớn hơn mình");
            if( newPermission != 5 ) return -1;

            account.PermissionId = newPermission;

            var updated = await _context.SaveChangesAsync();

            return orderPermission;
            // return 5;
        }

        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            // check quyền của mình và quyền account muốn xó
            // senderId, removerId, senderPermission, removerPermission
            // quyền của mình < quyền account muốn xó => tạch
            int permission = Authorization();
            if(permission == 0) return Unauthorized(new {error = "UnAuthorized"});

            var dataOfAccounts = await _context.Accounts.ToListAsync();

            return Ok(new { permission, dataOfAccounts });
        }


        /*
        new 
        in handle
        to ship
        to receive
        completed
        cancelled
        */
        [HttpGet("statistic/revenue")]//  statistis/revenue/?year=2021&month=10
        public async Task<ActionResult<OrderRevenueDTO>> GetRevenue([FromQuery] YearMonthQuery query)
        {
            try
            {                
                Console.WriteLine($"in side order revenue: {query.Year} & {query.Month}");
                var revenue = await _repo.GetRevenue(query.Year, query.Month);
                return Ok(revenue);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("statistic/products/quantity/product={id}")]
        public async Task<ActionResult> GetStatisticProductQuantities(int id, [FromQuery] YearMonthQuery query)
        {
            try
            {
                StatisticProductDTO dto = new StatisticProductDTO();

                if(query.Year == null)
                {
                    dto = await _repo.GetTotalSoldQuantitiesOfProduct(id);
                }
                else
                {
                    dto = await _repo.GetQuantitiesPerMonth(id, (int)query.Year);                    
                }
                return Ok(dto);                 
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut("events/applyall={id}")]
        public async Task<ActionResult> ApplyEvent(int id)
        {
            try
            {
                _repo.ApplyEventAll(id);
                await _repo.SaveChanges();
                return Ok();            
            }
            catch (Exception e)
            {
                if(e is ObjectExistedException)
                    return BadRequest(e.Message);
                return StatusCode(501);
            }
        }
        [HttpGet("events/nearest")]
        public async Task<ActionResult<IEnumerable<DiscountEventReadDTO>>> GetNearestEvents()
        {
            var models = await _repo.GetNearestEvents();
            if(models == null) return NotFound();
            var readDTO = _mapper.Map<IEnumerable<DiscountEventReadDTO>>(models);
            return Ok(readDTO);
        }
        [HttpGet("statistic/products/followers/product={id}")]
        public async Task<ActionResult<StatisticProductDTO>> GetStatisticProductTotalFollowers(int id)
        {
            var totalFollower = await _repo.GetProductTotalFollowers(id);
            var dto = new StatisticProductDTO();
            dto.ProductId = id;
            dto.TotalFollowers = totalFollower;
            return Ok(dto);
        }
        [HttpGet("statistic/orders/status={name}")]
        [HttpGet("statistic/orders/status={name}/{filter}")]//api/admin/orders/status=in handle/?page=?&limit=?&orderBy=true|false?
        public async Task<ActionResult<IEnumerable<OrderReadDTO>>> GetListOrdersByStatus(string name, int? page, int? limit, bool? orderBy)
        {
            try
            {
                var models = await _repo.StatusPagination(
                    name, 
                    new PaginationModel(
                        page == null ? 1 : (int) page, 
                        limit: limit == null ? 10 : (int)limit
                    ),
                    orderBy == null ? true : (bool) orderBy
                );
                if(models.List.Count() == 0) return NotFound();

                var readDTOs = _mapper.Map<IEnumerable<OrderReadDTO>>(models.List);
                return Ok(
                    new {
                        models.Metadata,
                        readDTOs
                    }
                );
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("statistic/products/top")]
        public async Task<ActionResult<IEnumerable<StatisticProductDTO>>> GetTopProductsByMonth([FromQuery] YearMonthQuery query)
        {
            try
            {
                var result = await _repo.GetTopSoldProductsByMonth((int)query.Year, (int)query.Month);
                return Ok(result);                 
            }
            catch (Exception e)
            {
                return StatusCode(501, new {
                    e.Message
                });
            }
        }
        [HttpGet("countingorders/status={status}")] // api/admin/countingorders/status=new/?Year=2021&Month=11
        public async Task<ActionResult<OrderRevenueDTO>> GetCountingOrders(string status, [FromQuery] YearMonthQuery query)
        {
            // string status = "Completed"; int year = 2021; int month = 11;
            try
            {
                ValidationInputData.IsTargetNotNull(query);
                ValidationInputData.IsTargetNotNull(query.Year);
                ValidationInputData.IsTargetNotNull(query.Month);
                var result = await _repo.GetStatisticOrdersByMonth((int)query.Year, (int)query.Month, status);
                Console.WriteLine("request counting");
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
    }
}