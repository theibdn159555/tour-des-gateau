using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.NotificationDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Paginations;
using WebUI.Models;

namespace WebUI.Controllers.Notifications
{
    [Route("api/notifications")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationRepo<Notification> _repo;
        private readonly IMapper _mapper;

        public NotificationController(INotificationRepo<Notification> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        protected ActionResult<IEnumerable<NotificationReadDTO>> GetPerPage(int page, IEnumerable<NotificationReadDTO> items)
        {
            PaginationResultSet<NotificationReadDTO, NotificationReadDTO> paginationResultSet = 
            new PaginationResultSet<NotificationReadDTO, NotificationReadDTO>(
                page, items, _mapper
            );
            
            try
            {
                paginationResultSet.GetPagination();
            }
            catch (ObjectNotFoundException)
            {
                return NotFound();
            }

            return Ok(
                new {
                    paginationResultSet.Metadata,
                    paginationResultSet.MappingResults
                }
            );
        }
        [HttpGet("user={userId}")]
        [HttpGet("user={userId}/page={page}")]
        public async Task<ActionResult<IEnumerable<NotificationReadDTO>>> GetNotifiesById(int userId, int page)
        {
            try
            {
                bool isCreateEventNotify = await _repo.CreateNew(userId);
                IEnumerable<Notification> models = await _repo.GetByID(userId);
                if(models == null || models.Count() == 0) return NoContent();
                var dtos = _mapper.Map<IEnumerable<NotificationReadDTO>>(models);
                return GetPerPage(page, dtos);                 
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut("notice={id}")]
        public async Task<ActionResult> MarkRead(int id)
        {
            var model = await _repo.FindExist(id);
            model.IsRead = true;
            await _repo.SaveChanges();
            return Ok();
        }
    }
}