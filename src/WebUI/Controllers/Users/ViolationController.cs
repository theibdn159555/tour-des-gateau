using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.DTOs.ViolationDTOs;
using WebUI.CreateByHung.Exceptions;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.Paginations;
using WebUI.Models;
using System.Linq;
using WebUI.CreateByHung.Profiles;
// using WebUI.CreateByHung.Others; 

namespace WebUI.Controllers.Users
{
    [Route("api/users/violations")]
    [ApiController]
    public class ViolationController : ControllerBase
    {
        private readonly IViolationRepo _repo;
        private readonly IMapper _mapper;

        public ViolationController(IViolationRepo repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        public class Optional{
            public int? Limit { get; set; }
            public int? Page { get; set; }
            public int? WarningID { get; set; }
            public int? UserID { get; set; }
            public int? UserName { get; set; }
            public int? Day { get; set; }
            public int? Month { get; set; }
            public int? Year { get; set; }
            public DateTime? From { get; set; }
            public DateTime? To { get; set; }
        }
        protected ActionResult GetPerPage(int page, IEnumerable<Violation> items, int limit = 6)
        {
            var paginationResultSet = new PaginationResultSet<ViolationReadDTO, Violation>(page, items, _mapper);
            
            try
            {
                paginationResultSet.GetPagination(limit);
            }
            catch (ObjectNotFoundException)
            {
                return NotFound();
            }

            return Ok(
                new {
                    paginationResultSet.Metadata,
                    paginationResultSet.MappingResults
                }
            );
        }
        [HttpGet("all")]
        public async Task<ActionResult> GetAll([FromQuery] Optional optional)
        {
            var list = await _repo.GetAll();

            if(list == null || list.Count() == 0) 
            {
                return NoContent();
            }

            return this.GetPerPage(optional.Page.Value, list, optional.Limit.Value);
        }
        [HttpGet("detail/list")]
        public async Task<ActionResult> GetByUserID([FromQuery] Optional optional)
        {
            var user = await _repo.GetByID(optional.UserID.Value);
            if(user == null)
                return NotFound();
            return Ok(user);
        }
        [HttpGet("detail/specify")]
        public async Task<ActionResult> GetByUserIDAndWarningID([FromQuery] Optional optional)
        {
            var user = await _repo.GetByID(optional.UserID.Value, optional.WarningID.Value);
            if(user == null)
                return NotFound();
            return Ok(user);
        }
        [HttpPost]
        public async Task<ActionResult> CreateNewBan(ViolationWriteDTO writeDTO)
        {
            var model = _mapper.Map<Violation>(writeDTO);
            try
            {
                var id = await _repo.CreateNew(model);
                if(id == 0)
                {
                    throw new Exception("Can not create new ban");   
                }
                return Ok($"New ban {id} is applied for user {writeDTO.AccountId}");
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpDelete("remove")]
        public async Task<ActionResult> DeleteCommand([FromQuery] Optional optional)
        {
            var model = await _repo.GetByID(optional.UserID.Value, optional.WarningID.Value);
            // var model = new Violation(){
            //     AccountId = optional.UserID.Value,
            //     WarningId = optional.WarningID.Value
            // };

            model.LogOut();

            if(model == null)
            {
                return NotFound();
            }

            _repo.DeleteOld(model);

            // await _repo.SaveChanges();

            return Ok();
        }
    }
}