using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.FavoriteDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.Models;

namespace WebUI.Controllers.Users
{
    [Route("api/users/favorites")]
    [ApiController]
    public class FavoriteController : RelationController<Favorite, FavoriteReadDTO, FavoriteWriteDTO, FavoriteUpdateDTO>
    {
        public FavoriteController(IRelationshipRepo<Favorite> repository, IMapper mapper) : base(repository, mapper)
        {
        }
        [HttpGet("user={id}", Name = "GetFavoritesByUserID")]
        [HttpGet("user={id}/{search}")]
        public async Task<ActionResult<IEnumerable<FavoriteReadDTO>>> GetFavoritesByUserID(int id, int? page, int? limit)
        {
            int pageNumber = page == null ? 1 : (int) page;
            int limitItem = limit == null ? 6 : (int) limit;
            return await base.GetItemByID(id, pageNumber, limitItem);
        }
        [HttpPost]
        public Task<ActionResult<FavoriteWriteDTO>> CreateNewFavorite(FavoriteWriteDTO rating)
        {
            return base.CreateRecord(rating, nameof(GetFavoritesByUserID));
        }
        [HttpGet("user={userId}/product={productId}")]
        public async Task<ActionResult<FavoriteReadDTO>> GetByDetailID(int userId, int productId)
        {
            return await base.GetItemByID(userId, productId);
        }
        [HttpGet("user={id}/filter/{search}")]
        public async Task<ActionResult<IEnumerable<FavoriteReadDTO>>> FilterByID(
            int id, 
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to, 
            int? page,
            int? limit 
        )
        {
            // IEnumerable<Favorite> lists = await _repository.Filter(id, orderByDate, year, month, day, from, to);
            // if(lists.Count() == 0) return NotFound();

            // foreach (var item in lists)
            // {
            //     item.Account = null;
            // }

            // int pageNumber = page == null ? 1 : (int) page;
            // int limitItem = limit == null ? 6 : (int) limit;

            // return base.GetPerPage(pageNumber, lists, limitItem);
            return await base.Filter(id, orderByDate, year, month, day, from, to, page, limit);
        }
        [HttpDelete("user={accountId}/product={productId}")]
        public Task<ActionResult> DeleteFavorite(int accountId, int productId)
        {
            return base.DeleteRecord(accountId, productId);
        }
        [HttpPut("user={accountId}/product={productId}")]
        public Task<ActionResult> UpdateFavorite(int accountId, int productId, FavoriteUpdateDTO updateDTO)
        {
            return base.UpdateRecord(accountId, productId, updateDTO);
        }
    }
}