using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using WebUI.DTO.System;
using WebUI.Data;
using WebUI.Models;

using BC = BCrypt.Net.BCrypt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;

using WebUI.DTO.Accounts;

namespace WebUI.Controllers.System.Users
{
    [Route("api/user")]
    [ApiController]
    public class UpdateInfoController : ControllerBase
    {
        private readonly db_a7c7da_phunghung123Context _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public UpdateInfoController(db_a7c7da_phunghung123Context context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _config = configuration;
        }

        [HttpPatch]
        public async Task<ActionResult<UpdateDTO>> Index(UpdateDTO dto)
        {
            var accountModal = await _context.Accounts
                .FirstOrDefaultAsync(item => item.Username == dto.Username);

            if (accountModal == null) return UnprocessableEntity(new { error = "Account is invalid" });

            var accountMap = _mapper.Map<Account>(accountModal);

            if (!await UpdateAccount(accountMap , dto.FirstName, dto.LastName, dto.Email, dto.Phone, dto.Gender)) return BadRequest(new { error = "Update Fail" });

            return Ok(new
            {
                message = "success",
                
                accountMap,
                expires_in = 60 * 60 * 24 * 7
            });
        }

        private async Task<bool> UpdateAccount(Account account, String FirstName, String LastName, String Email, String Phone, int? Gender)
        {
            account.FirstName = FirstName;
            account.LastName = LastName;
            account.Email = Email;
            account.Phone = Phone;
            account.Gender = Gender;

            _context.Accounts.Update(account);
            var updated = await _context.SaveChangesAsync();

            return (await _context.SaveChangesAsync() >= 0);
        }
    }
}
