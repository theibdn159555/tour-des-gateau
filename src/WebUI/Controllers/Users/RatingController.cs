using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebUI.Controllers.System.Filters;
using WebUI.CreateByHung.CustomControllers;
using WebUI.CreateByHung.DTOs.ProductDTOs;
using WebUI.CreateByHung.DTOs.RatingDTOs;
using WebUI.CreateByHung.Interfaces;
using WebUI.CreateByHung.Others;
using WebUI.CreateByHung.SqlQueryRepoes.RelationshipRepoes;
using WebUI.Models;

namespace WebUI.Controllers.Users
{
    [Route("api/products/ratings")]
    [ApiController]
    [DebugResourceFilter]
    public class RatingController : RelationController<Rating, RatingReadDTO, RatingWriteDTO, RatingUpdateDTO>
    {
        public RatingController(IRelationshipRepo<Rating> repository, IMapper mapper) : base(repository, mapper)
        {
        }

        [HttpGet("product={id}", Name = "GetRatingByProductID")]
        [HttpGet("product={id}/{search}")]
        public async Task<ActionResult<IEnumerable<RatingReadDTO>>> GetRatingByProductID(int id, int? page, int? limit)
        {
            var ratingLists = await _repository.GetByID(id);
            int pageNumber = page == null ? 1 : (int) page;
            int limitItem = limit == null ? 6 : (int) limit;
            RatingSummaryDTO ratingSummary = await ((SqlRatingRepo)_repository).GetRatingSummaryByID(id);
            ActionResult<IEnumerable<RatingReadDTO>> ratingSets = GetPerPage(pageNumber, ratingLists, limitItem);
            // var ratingSets = _mapper.Map<IEnumerable<RatingReadDTO>>(ratingLists);
            // foreach (var item in ratingSets)
            // {
            //     item.Product = null;
            // }
            return Ok(new {ratingSummary, ratingSets});
        }

        [HttpGet("withousmr/product={id}")]
        public async Task<ActionResult<IEnumerable<RatingReadDTO>>> GetRatingByProductIDWithoutSummary(int id, [FromQuery]int? page, [FromQuery]int? limit)
        {
            var ratingLists = await _repository.GetByID(id);
            int pageNumber = page == null ? 1 : (int) page;
            int limitItem = limit == null ? 6 : (int) limit;
            // RatingSummaryDTO ratingSummary = await ((SqlRatingRepo)_repository).GetRatingSummaryByID(id);
                        
            return GetPerPage(pageNumber, ratingLists, limitItem);;
        }

        [HttpGet("smr/product={id}/")]
        public async Task<ActionResult<RatingSummaryDTO>> GetRatingSummaryByProductID(int id, int? page, int? limit)
        {
            return Ok(await ((SqlRatingRepo)_repository).GetRatingSummaryByID(id));
        }

        [HttpGet("product={id}/filter/{search}")]
        public async Task<ActionResult<IEnumerable<RatingReadDTO>>> FilterByID(
            int id,
            int? star,
            bool? orderByStar, 
            bool? orderByDate, 
            int? year, int? month, int? day, 
            DateTime? from, DateTime? to, 
            int? page,
            int? limit
        )
        {
            IEnumerable<Rating> models = new List<Rating>();
            if(orderByStar != null)
            {
                models = await ((SqlRatingRepo)_repository).Filter(id, (bool)orderByStar);
            }
            else if(star != null)
            {
                models = await ((SqlRatingRepo)_repository).GetByStar(id, orderByDate, (int)star);
            }
            else models = await _repository.Filter(id, orderByDate, year, month, day, from, to);
            if(models.Count() == 0)
            {
                return NotFound();
            }
            int pageNumber = page == null ? 1 : (int)page;
            int limitItem = limit == null ? 6 : (int)limit;
            
            return GetPerPage(pageNumber, models, limitItem);
        }
        [HttpPost]
        public Task<ActionResult<RatingWriteDTO>> CreateNewRating(RatingWriteDTO rating)
        {
            return base.CreateRecord(rating, nameof(GetRatingByProductID));
        }
        [HttpDelete("user={accountId}/product={productId}")]
        public Task<ActionResult> DeleteRating(int accountId, int productId)
        {
            return base.DeleteRecord(accountId, productId);
        }
        [HttpPut("user={accountId}/product={productId}")]
        public async Task<ActionResult> UpdateRating(int accountId, int productId, RatingUpdateDTO updateDTO)
        {
            int expired = await SqlViolationRepo.IsViolationExpired(updateDTO.UserId);
            if (expired < 0)
            {
                return BadRequest("This account is banned");
            }
            return await base.UpdateRecord(accountId, productId, updateDTO);
        }
        [HttpGet("user={userId}/product={productId}")]
        public async Task<ActionResult<RatingReadDTO>> GetByDetailID(int userId, int productId)
        {
            return await base.GetItemByID(userId, productId);
        }
        [HttpGet("statistic/products/topratings")]
        public async Task<ActionResult<StatisticProductDTO>> GetTopRatingsProducts([FromQuery] YearMonthQuery query)
        {
            try
            {
                if(query.Month.HasValue && !query.Year.HasValue)
                    throw new Exception("Missing Year param");
                var results = await ((SqlRatingRepo)_repository).GetTopRatingProducts(query.Month, query.Year);
                return Ok(results);
            }
            catch (Exception e)
            {
                return StatusCode(501, e.Message);
            }
        }
    }
}