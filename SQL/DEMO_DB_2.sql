--CREATE DATABASE DEMO_WEBCAKE_DB;
--USE DEMO_WEBCAKE_DB;
CREATE TABLE [Accounts] (
  [account_id] int IDENTITY(1,1) NOT NULL,
  [permission_id] int DEFAULT 4 NOT NULL,
  [username] varchar(50) NOT NULL,
  [password_hash] varchar(100) NOT NULL,
  [first_name] nvarchar(30) NULL,
  [last_name] nvarchar(30) NULL,
  [email] varchar(100) NOT NULL,
  [avatar] varchar(500) NULL,
  [dob] date NULL,
  [phone] varchar(20) NULL,
  [gender] int NULL,
  [membership] float DEFAULT 0 NULL,
  [facebook_id] varchar(255) NULL,
  [google _id] nvarchar(255) NULL,
  [ip_address] varchar(255) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_ACC] PRIMARY KEY CLUSTERED ([account_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [unique_account] UNIQUE NONCLUSTERED ([username])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [unique_email] UNIQUE NONCLUSTERED ([email])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_name_email]
ON [Accounts] (
  [username] ASC,
  [email] ASC
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_phone]
ON [Accounts] (
  [phone]
)
WHERE [phone] IS NOT NULL
GO
CREATE TRIGGER [TRG_ACC_CREATE]
ON [Accounts]
FOR INSERT
AS
BEGIN
	-- this trigger is created to use update [created_at] field
  update [dbo].[Accounts] 
	set [dbo].[Accounts].created_at = GETDATE() 
	where [dbo].[Accounts].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_ACC_UPDATE]
ON [Accounts]
FOR UPDATE
AS
BEGIN
  -- this trigger is created to use update [updated_at] field
	update [dbo].[Accounts] 
	set [dbo].[Accounts].updated_at = GETDATE() 
	where [dbo].[Accounts].account_id = (select i.account_id from inserted as i);
END
GO

CREATE TABLE [Cart_Details] (
  [account_id] int NOT NULL,
  [cart_id] int NOT NULL,
  [product_id] int NOT NULL,
  [quantity] int NOT NULL,
  [discount_value] float NOT NULL,
  [unit_price] decimal NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NOT NULL,
  CONSTRAINT [PK_DCT] PRIMARY KEY CLUSTERED ([account_id], [cart_id], [product_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_DCT_CREATE]
ON [Cart_Details]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Cart_Details] 
	set [dbo].[Cart_Details].created_at = GETDATE() 
	where [dbo].[Cart_Details].product_id = (select i.product_id from inserted as i) 
	  and [dbo].[Cart_Details].cart_id = (select i.cart_id from inserted as i)
		and [dbo].[Cart_Details].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_DCT_UPDATE]
ON [Cart_Details]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Cart_Details] 
	set [dbo].[Cart_Details].updated_at = GETDATE() 
	where [dbo].[Cart_Details].product_id = (select i.product_id from inserted as i) 
	  and [dbo].[Cart_Details].cart_id = (select i.cart_id from inserted as i)
		and [dbo].[Cart_Details].account_id = (select i.account_id from inserted as i);
END
GO

CREATE TABLE [Carts] (
  [cart_id] int NOT NULL,
  [account_id] int NOT NULL,
  [is_open] bit DEFAULT 1 NOT NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_CRT] PRIMARY KEY CLUSTERED ([cart_id], [account_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_acc]
ON [Carts] (
  [account_id]
)
GO
CREATE TRIGGER [TRG_CRT_CREATE]
ON [Carts]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Carts] 
	set [dbo].[Carts].created_at = GETDATE() 
	where [dbo].[Carts].cart_id = (select i.cart_id from inserted as i)
	AND [dbo].[Carts].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_CRT_UPDATE]
ON [Carts]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Carts] 
	set [dbo].[Carts].updated_at = GETDATE() 
	where [dbo].[Carts].cart_id = (select i.cart_id from inserted as i)
	AND [dbo].[Carts].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_CRT_VALID_ACC]
ON [Carts]
FOR INSERT
AS
BEGIN
  DECLARE @Acc_id INT;
	DECLARE @Per_id INT;
	DECLARE @Per_name VARCHAR(50);
	DECLARE @Email VARCHAR(100);
	DECLARE @Phone VARCHAR(20);
	SET @Acc_id = (SELECT i.account_id FROM inserted AS i);
	SET @Email = (SELECT a.email FROM Accounts AS a);
	SET @Phone = (SELECT a.phone FROM Accounts AS a);
	SET @Per_id = (SELECT a.permission_id FROM [dbo].[Accounts] AS a WHERE a.account_id = @Acc_id);
	SET @Per_name = (SELECT p.permission_name FROM [dbo].[Permissions] AS p WHERE p.permission_id = @Per_id);
	IF @Per_name <> 'USER'
	BEGIN
	  RAISERROR('Permission is not match', 14, 1);
	  ROLLBACK TRAN;
	END
	ELSE
	BEGIN
	  IF (@Email <> '' OR @Email <> NULL)
		BEGIN
		  RAISERROR('Email is not validation', 14, 1);
	    ROLLBACK TRAN;
		END
		ELSE
		BEGIN
		  IF (@Phone <> '' OR @Phone <> NULL)
			BEGIN
			  RAISERROR('Phone is not validation', 14, 1);
	      ROLLBACK TRAN;
			END
		END
	END
END
GO

CREATE TABLE [Categories] (
  [category_id] int IDENTITY(1,1) NOT NULL,
  [category_name] varchar(255) NOT NULL,
  [description] varchar(500) NULL,
  CONSTRAINT [PK_CAT] PRIMARY KEY CLUSTERED ([category_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Discount_Events] (
  [event_id] int IDENTITY(1,1) NOT NULL,
  [event_name] nvarchar(255) NULL,
  [date_start] date NULL,
  [date_end] date NULL,
  [discount_value] float NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_EVT] PRIMARY KEY CLUSTERED ([event_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_event]
ON [Discount_Events] (
  [event_name]
)
GO
CREATE TRIGGER [TRG_EVT_CREATE]
ON [Discount_Events]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Discount_Events]
	set [dbo].[Discount_Events].created_at = GETDATE() 
	where [dbo].[Discount_Events].event_id = (select i.event_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_EVT_UPDATE]
ON [Discount_Events]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Discount_Events] 
	set [dbo].[Discount_Events].updated_at = GETDATE() 
	where [dbo].[Discount_Events].event_id = (select i.event_id from inserted as i);
END
GO

CREATE TABLE [Favorites] (
  [account_id] int NOT NULL,
  [product_id] int NOT NULL,
  [created_at] datetime NOT NULL,
  CONSTRAINT [PK_FAV] PRIMARY KEY CLUSTERED ([account_id], [product_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_FAV_CREATE]
ON [Favorites]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Favorites] 
	set [dbo].[Favorites].created_at = GETDATE() 
  where [dbo].[Favorites].product_id = (select i.product_id from inserted as i)
	  and [dbo].[Favorites].account_id = (select i.account_id from inserted as i);
END
GO

CREATE TABLE [Ingredients] (
  [product_id] int NOT NULL,
  [material_id] int NOT NULL,
  [quantity] float NULL,
  [main] bit DEFAULT 0 NOT NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_ING] PRIMARY KEY CLUSTERED ([product_id], [material_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_ingredient]
ON [Ingredients] (
  [main]
)
GO
CREATE TRIGGER [TRG_ING_CREATE]
ON [Ingredients]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Ingredients] 
	set [dbo].[Ingredients].created_at = GETDATE() 
	where [dbo].[Ingredients].product_id = (select i.product_id from inserted as i)
	  and [dbo].[Ingredients].material_id = (select i.material_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_ING_UPDATE]
ON [Ingredients]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Ingredients]
	set [dbo].[Ingredients].updated_at = GETDATE() 
	where [dbo].[Ingredients].product_id = (select i.product_id from inserted as i)
	  and [dbo].[Ingredients].material_id = (select i.material_id from inserted as i);
END
GO

CREATE TABLE [Materials] (
  [material_id] int IDENTITY(1,1) NOT NULL,
  [material_name] varchar(255) NOT NULL,
  CONSTRAINT [PK_MAT] PRIMARY KEY CLUSTERED ([material_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [UNIQUE_MAT_NAME] UNIQUE NONCLUSTERED ([material_name])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Notification_Templates] (
  [template_id] int IDENTITY(1,1) NOT NULL,
  [content_template] varchar(255) NULL,
  [type_id] int NULL,
  CONSTRAINT [PK_NTP] PRIMARY KEY CLUSTERED ([template_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Notification_Types] (
  [notice_type_id] int IDENTITY(1,1) NOT NULL,
  [type_name] varchar(255) NULL,
  CONSTRAINT [PK_NTY] PRIMARY KEY CLUSTERED ([notice_type_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Notifications] (
  [notification_id] int NOT NULL,
  [template_id] int NULL,
  [content] varchar(255) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_NOT] PRIMARY KEY CLUSTERED ([notification_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_NEV_CREATE]
ON [Notifications]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Notifications] 
	set [dbo].[Notifications].created_at = GETDATE() 
	where [dbo].[Notifications].notification_id = (select i.notification_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_NEV_UPDATE]
ON [Notifications]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Notifications] 
	set [dbo].[Notifications].updated_at = GETDATE() 
	where [dbo].[Notifications].notification_id = (select i.notification_id from inserted as i);
END
GO

CREATE TABLE [Notifies] (
  [account_id] int NOT NULL,
  [notice_event_id] int NOT NULL,
  [is_read] bit DEFAULT 0 NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_NTF] PRIMARY KEY CLUSTERED ([account_id], [notice_event_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_NTF_CREATE]
ON [Notifies]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Notifies] 
	set [dbo].[Notifies].created_at = GETDATE() 
	where [dbo].[Notifies].notice_event_id = (select i.notice_event_id from inserted as i)
	  and [dbo].[Notifies].account_id = (select i.account_id from inserted as i);	
END
GO
CREATE TRIGGER [TRG_NTF_UPDATE]
ON [Notifies]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Notifies] 
	set [dbo].[Notifies].updated_at = GETDATE() 
	where [dbo].[Notifies].notice_event_id = (select i.notice_event_id from inserted as i)
	  and [dbo].[Notifies].account_id = (select i.account_id from inserted as i);	
END
GO

CREATE TABLE [Order_Status] (
  [status_id] int IDENTITY(1,1) NOT NULL,
  [status_name] varchar(100) NOT NULL,
  [description] varchar(255) NULL,
  CONSTRAINT [PK_STT] PRIMARY KEY CLUSTERED ([status_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [UNIQUE_STT_NAME] UNIQUE NONCLUSTERED ([status_name])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Orders] (
  [cart_id] int NOT NULL,
  [account_id] int NOT NULL,
  [status_id] int NOT NULL,
  [payment_method] int NOT NULL,
  [bank_name] varchar(255) NULL,
  [ship_fee] float DEFAULT 0 NULL,
  [note] varchar(255) NULL,
  [ship_address] varchar(255) NOT NULL,
  [ship_ward] varchar(100) NOT NULL,
  [ship_district] varchar(100) NOT NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_ORD] PRIMARY KEY CLUSTERED ([cart_id], [account_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_crt]
ON [Orders] (
  [cart_id]
)
GO
CREATE TRIGGER [TRG_ORD_CREATE]
ON [Orders]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Orders] 
	set [dbo].[Orders].created_at = GETDATE() 
	where [dbo].[Orders].cart_id = (select i.cart_id from inserted as i) 
	AND [dbo].[Orders].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_ORD_UDPATE]
ON [Orders]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Orders] 
	set [dbo].[Orders].updated_at = GETDATE() 
	where [dbo].[Orders].cart_id = (select i.cart_id from inserted as i) 
	AND [dbo].[Orders].account_id = (select i.account_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_ORD_PAY]
ON [Orders]
FOR INSERT
AS
BEGIN
  UPDATE [dbo].[Carts] 
	SET [dbo].[Carts].[is_open] = 0 
	WHERE [dbo].[Carts].[cart_id] = (SELECT i.cart_id FROM inserted AS i)
	AND [dbo].[Carts].[account_id] = (SELECT i.account_id FROM inserted AS i);
END
GO
CREATE TRIGGER [TRG_ORD_PAY_2]
ON [Orders]
FOR INSERT
AS
BEGIN
  DECLARE @new_cart INT;
	DECLARE @owner INT;
	SET @new_cart = (SELECT i.[cart_id] FROM inserted as i);
	SET @new_cart = @new_cart + 1;
	SET @owner = (SELECT i.[account_id] FROM inserted as i);
  INSERT INTO [dbo].[Carts]([cart_id],[account_id]) VALUES (@new_cart, @owner);
END
GO

CREATE TABLE [Permissions] (
  [permission_id] int IDENTITY(1,1) NOT NULL,
  [permission_name] varchar(50) NOT NULL,
  [description] varchar(500) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_PER] PRIMARY KEY CLUSTERED ([permission_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [UNIQUE_PER_NAME] UNIQUE NONCLUSTERED ([permission_name])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_PER_CREATE]
ON [Permissions]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Permissions] 
	set [dbo].[Permissions].created_at = GETDATE() 
	where [dbo].[Permissions].permission_id = (select i.permission_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_PER_UPDATE]
ON [Permissions]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Permissions] 
	set [dbo].[Permissions].updated_at = GETDATE() 
	where [dbo].[Permissions].permission_id = (select i.permission_id from inserted as i);
END
GO

CREATE TABLE [Products] (
  [product_id] int IDENTITY(1,1) NOT NULL,
  [category_id] int NOT NULL,
  [size_id] int NOT NULL,
  [tag_id] int NOT NULL,
  [event_id] int DEFAULT 1 NULL,
  [product_name] varchar(255) NOT NULL,
  [unit_price] decimal NOT NULL,
  [image] varchar(500) NULL,
  [description] varchar(500) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_PRD] PRIMARY KEY CLUSTERED ([product_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [UNIQUE_PROD_NAME] UNIQUE NONCLUSTERED ([product_name])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_prd]
ON [Products] (
  [product_name]
)
GO
CREATE TRIGGER [TRG_PRD_CREATE]
ON [Products]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Products]
	set [dbo].[Products].created_at = GETDATE() 
	where [dbo].[Products].product_id = (select i.product_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_PRD_UPDATE]
ON [Products]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Products] 
	set [dbo].[Products].updated_at = GETDATE() 
	where [dbo].[Products].product_id = (select i.product_id from inserted as i);
END
GO

CREATE TABLE [Rating] (
  [product_id] int NOT NULL,
  [user_id] int NOT NULL,
  [star_rating] int NOT NULL,
  [comment] varchar(1000) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_RAT] PRIMARY KEY CLUSTERED ([product_id], [user_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE NONCLUSTERED INDEX [idx_sta]
ON [Rating] (
  [star_rating]
)
GO
CREATE TRIGGER [TRG_RAT_CREATE]
ON [Rating]
FOR INSERT
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Rating] 
	set [dbo].[Rating].created_at = GETDATE() 
	where [dbo].[Rating].product_id = (select i.product_id from inserted as i) 
	  and [dbo].[Rating].user_id = (select i.user_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_RAT_UPDATE]
ON [Rating]
FOR UPDATE
AS
BEGIN
  -- Type the SQL Here.
	update [dbo].[Rating] 
	set [dbo].[Rating].updated_at = GETDATE() 
	where [dbo].[Rating].product_id = (select i.product_id from inserted as i) 
	  and [dbo].[Rating].user_id = (select i.user_id from inserted as i);
END
GO

CREATE TABLE [Sizes] (
  [size_id] int IDENTITY(1,1) NOT NULL,
  [size_name] varchar(40) NOT NULL,
  [description] varchar(255) NULL,
  CONSTRAINT [PK_SIZ] PRIMARY KEY CLUSTERED ([size_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
  CONSTRAINT [UNIQUE_SIZ_NAME] UNIQUE NONCLUSTERED ([size_name])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

CREATE TABLE [Tags] (
  [tag_id] int IDENTITY(1,1) NOT NULL,
  [name_tag] varchar(255) NOT NULL,
  CONSTRAINT [PK_TAG] PRIMARY KEY CLUSTERED ([tag_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_name_tag]
ON [Tags] (
  [name_tag]
)
GO

CREATE TABLE [Violations] (
  [account_id] int NOT NULL,
  [warning_id] int NOT NULL,
  [reason] varchar(500) NULL,
  [created_at] datetime NULL,
  [updated_at] datetime NULL,
  CONSTRAINT [PK_VIO] PRIMARY KEY CLUSTERED ([account_id], [warning_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
CREATE TRIGGER [TRG_VIO_CREATE]
ON [Violations]
FOR INSERT
AS
BEGIN
	-- this trigger is created to use update [created_at] field
  update [dbo].[Violations] 
	set [dbo].[Violations].created_at = GETDATE() 
	where [dbo].[Violations].account_id = (select i.account_id from inserted as i)
	  and [dbo].[Violations].warning_id = (select i.warning_id from inserted as i);
END
GO
CREATE TRIGGER [TRG_VIO_UPDATE]
ON [Violations]
FOR UPDATE
AS
BEGIN
	-- this trigger is created to use update [created_at] field
  update [dbo].[Violations] 
	set [dbo].[Violations].updated_at = GETDATE() 
	where [dbo].[Violations].account_id = (select i.account_id from inserted as i)
	  and [dbo].[Violations].warning_id = (select i.warning_id from inserted as i);
END
GO

CREATE TABLE [Warnings] (
  [warning_id] int IDENTITY(1,1) NOT NULL,
  [content] varchar(255) NOT NULL,
  [description] varchar(500) NULL,
  CONSTRAINT [PK_WAR] PRIMARY KEY CLUSTERED ([warning_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [Accounts] ADD CONSTRAINT [FK_ACC_PER] FOREIGN KEY ([permission_id]) REFERENCES [Permissions] ([permission_id])
GO
ALTER TABLE [Cart_Details] ADD CONSTRAINT [FK_DCT_CRT] FOREIGN KEY ([cart_id], [account_id]) REFERENCES [Carts] ([cart_id], [account_id])
GO
ALTER TABLE [Cart_Details] ADD CONSTRAINT [FK_DCT_PRD] FOREIGN KEY ([product_id]) REFERENCES [Products] ([product_id])
GO
ALTER TABLE [Carts] ADD CONSTRAINT [FK_CRT_ACC] FOREIGN KEY ([account_id]) REFERENCES [Accounts] ([account_id])
GO
ALTER TABLE [Favorites] ADD CONSTRAINT [FK_FAV_ACC] FOREIGN KEY ([account_id]) REFERENCES [Accounts] ([account_id])
GO
ALTER TABLE [Favorites] ADD CONSTRAINT [FK_FAV_PRD] FOREIGN KEY ([product_id]) REFERENCES [Products] ([product_id])
GO
ALTER TABLE [Ingredients] ADD CONSTRAINT [FK_IGD_PRD] FOREIGN KEY ([product_id]) REFERENCES [Products] ([product_id])
GO
ALTER TABLE [Ingredients] ADD CONSTRAINT [FK_IGD_MAT] FOREIGN KEY ([material_id]) REFERENCES [Materials] ([material_id])
GO
ALTER TABLE [Notification_Templates] ADD CONSTRAINT [FK_NTP_NTY] FOREIGN KEY ([type_id]) REFERENCES [Notification_Types] ([notice_type_id])
GO
ALTER TABLE [Notifications] ADD CONSTRAINT [FK_NEV_NTE] FOREIGN KEY ([template_id]) REFERENCES [Notification_Templates] ([template_id])
GO
ALTER TABLE [Notifies] ADD CONSTRAINT [FK_NTF_ACC] FOREIGN KEY ([account_id]) REFERENCES [Accounts] ([account_id])
GO
ALTER TABLE [Notifies] ADD CONSTRAINT [FK_NTF_NOT] FOREIGN KEY ([notice_event_id]) REFERENCES [Notifications] ([notification_id])
GO
ALTER TABLE [Orders] ADD CONSTRAINT [FK_ORD_CRT] FOREIGN KEY ([cart_id], [account_id]) REFERENCES [Carts] ([cart_id], [account_id])
GO
ALTER TABLE [Orders] ADD CONSTRAINT [FK_ORD_STT] FOREIGN KEY ([status_id]) REFERENCES [Order_Status] ([status_id])
GO
ALTER TABLE [Products] ADD CONSTRAINT [FK_PRO_CAT] FOREIGN KEY ([category_id]) REFERENCES [Categories] ([category_id])
GO
ALTER TABLE [Products] ADD CONSTRAINT [FK_PRD_SIZ] FOREIGN KEY ([size_id]) REFERENCES [Sizes] ([size_id])
GO
ALTER TABLE [Products] ADD CONSTRAINT [FK_PRD_TAG] FOREIGN KEY ([tag_id]) REFERENCES [Tags] ([tag_id])
GO
ALTER TABLE [Products] ADD CONSTRAINT [FK_PRD_EVT] FOREIGN KEY ([event_id]) REFERENCES [Discount_Events] ([event_id])
GO
ALTER TABLE [Rating] ADD CONSTRAINT [FK_RAT_PRD] FOREIGN KEY ([product_id]) REFERENCES [Products] ([product_id])
GO
ALTER TABLE [Rating] ADD CONSTRAINT [FK_RAT_ACC] FOREIGN KEY ([user_id]) REFERENCES [Accounts] ([account_id])
GO
ALTER TABLE [Violations] ADD CONSTRAINT [FK_VIO_ACC] FOREIGN KEY ([account_id]) REFERENCES [Accounts] ([account_id])
GO
ALTER TABLE [Violations] ADD CONSTRAINT [FK_VIO_WAR] FOREIGN KEY ([warning_id]) REFERENCES [Warnings] ([warning_id])
GO

